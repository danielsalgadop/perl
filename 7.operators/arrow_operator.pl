#!/usr/bin/perl -w
use strict;
use Data::Dumper;

print "X"x30,"\n";
### USUAL way
my %tradicional_hash;
$tradicional_hash{tradicional_key} = "tradicional_value";
print Dumper %tradicional_hash;

print "X"x30,"\n";
### NEW way using references (as seen in NSIM)
my $arrow_operator_str->{arrow_operator_key}="arrow_operator_value"; # 
print Dumper $arrow_operator_str;

print "X"x30,"\n";
### From NEW->USUAL
my %similar_to_tradicional = %$arrow_operator_str;
print Dumper %similar_to_tradicional;

print "X"x30,"\n";
### From USUAL->NEW
print Dumper \%tradicional_hash;
print "X"x30,"\n";


#### MY NOTES
# array operator is used to derefence