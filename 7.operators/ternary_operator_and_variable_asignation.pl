#!/usr/bin/perl -w
use strict;
my $var; # se rellena en funcion del resultado de la subrutina

sub subrutinaTrue(){return 1}
sub subrutinaFalse(){return undef}

subrutinaTrue
 ? print "TRUE\n" #ok
 : print "FALSE\n";

subrutinaTrue
 ? $var = "TRUE"
 : $var = "FALSE";    # <<<<<<<<<<<<<<<<< ?????????????? por que entra por aqui
print $var."\n";

subrutinaFalse
 ? print "TRUE\n"
 : print "FALSE\n"; #ok

subrutinaFalse
 ? $var = "TRUE"
 : $var = "FALSE"; #ok
print $var."\n";

$var = subrutinaTrue
	? "TRUE" # ok
	: "FALSE";
	print $var."\n";

$var = subrutinaFalse
	? "TRUE"
	: "FALSE"; # ok
	print $var."\n";

# SOLUCION propuesta por Adrian Lo mas sencillo es poner paréntesis a todo, y te olvidas: (...) ? (...) : (...)
(subrutinaTrue)
 ? ($var = "TRUE")        # ahora sí OK
 : ($var = "FALSE");
print $var."\n";


print "="x30,"2 VARIABLE ASIGNATION\n";
my $var2;
subrutinaTrue
 ? do{$var = "TRUE";$var2="TRUE"}
 : do{$var = "FALSE";$var2="FALSE"};
print $var."-".$var2."\n";

my $var2;
(subrutinaFalse)
 ? do{$var = "TRUE";$var2="TRUE"}
 : do{$var = "FALSE";$var2="FALSE"};
print $var."-".$var2."\n";