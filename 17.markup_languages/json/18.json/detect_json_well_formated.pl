#!/usr/bin/perl
# detect a well formated json
use strict;
use warnings;
use JSON;
use Data::Dumper;
use Test::More 'no_plan';

my $json_ok = '{"borrable":"adios me piro","titulo":"tiutlo chulo","contenido":"contenido","correo":"cuenta@bt.com","tipo":"petcion","estado":"inicial","epoch_inicial":{"borrable":"aqui si","aniomesdia":"20090916","hora":"18","minuto":"45"}}';
my $json_no_ok = '{::""borrable":"adios me piro","titulo":"tiutlo chulo","contenido":"contenido","correo":"cuenta@bt.com","tipo":"petcion","estado":"inicial","epoch_inicial":{"borrable":"aqui si","aniomesdia":"20090916","hora":"18","minuto":"45"}}';

my %r_isItWellFormatedJson;
#### a well formated json
%r_isItWellFormatedJson =  &isItWellFormatedJson($json_ok);
ok( $r_isItWellFormatedJson{status} eq "OK" , "un json BIEN formado");
# print Dumper(%r_isItWellFormatedJson);

#### bad formatted json
%r_isItWellFormatedJson =  &isItWellFormatedJson($json_no_ok);
ok( $r_isItWellFormatedJson{status} eq "ERROR" , "un json BIEN MAL formado");
isnt( $r_isItWellFormatedJson{message} , "" , "el message no esta vacio");
# print Dumper(%r_isItWellFormatedJson);

###################################################
# Function detects a well formated json  (string)
# Receives: a 'literal json'
#
###################################################
sub isItWellFormatedJson($){
    my $is_json = shift;
    print $is_json;
    eval {
        decode_json($is_json);
    };
    if($@){     # PROBLEMS
        chomp($@);
        return(status=>"ERROR",message=>$@);
    }
    return(status=>"OK");
}