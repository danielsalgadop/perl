#!/usr/bin/perl -w
# it also has Tests
use strict;
use Data::Dumper;
use Test::More 'no_plan';
use lib '.';
use miJSON;
my %r_CodificaJson;

######################################## OKs
diag("return{status} == OK");
# TEST -
%r_CodificaJson = CodificaJson({uno=>"1",dos=>"2"});
ok($r_CodificaJson{status} eq "OK","JSON constructed from hash - directly in arguments");

# TEST -
my %params = (uno=>1,dos=>2);
%r_CodificaJson = CodificaJson(\%params);
ok($r_CodificaJson{status} eq "OK","JSON constructed from hash - ref to hash");

# TEST -
%r_CodificaJson = CodificaJson(["uno","1","dos","2"]);
ok($r_CodificaJson{status} eq "OK","JSON constructed from array - directly in arguments");

# TEST -
my @params = qw (uno 1 dos 2);
%r_CodificaJson = CodificaJson(\@params);
ok($r_CodificaJson{status} eq "OK","JSON constructed from array - ref to array");

######################################## ERRORS
diag("return{status} == ERROR");
# TEST - empty JSON
%r_CodificaJson = CodificaJson("");
ok($r_CodificaJson{status} eq "ERROR","empty JSON");
ok($r_CodificaJson{message} eq  "Emtpy Json", "Correct message for empty JSON");

# TEST - JSON = 0
%r_CodificaJson = CodificaJson(0);
ok($r_CodificaJson{status} eq "ERROR","JSON = 0");
ok($r_CodificaJson{message} eq  "Emtpy Json", "Correct message for empty JSON = 0");


# TEST -
%r_CodificaJson = CodificaJson("not a ref to a hash or an array");
ok($r_CodificaJson{status} eq "ERROR","string as parameter");
# TEST - Message is not empty
isnt($r_CodificaJson{message}, "", "Mesage of error NOT EMPTY [".$r_CodificaJson{message}."]");

# TEST -
%r_CodificaJson = CodificaJson(\"not a ref to a hash or an array");
ok($r_CodificaJson{status} eq "ERROR","ref_string as parameter");
# TEST - Message is not empty
isnt($r_CodificaJson{message}, "", "Mesage of error NOT EMPTY [".$r_CodificaJson{message}."]");

# TEST -
%r_CodificaJson = CodificaJson(%params);
ok($r_CodificaJson{status} eq "ERROR","hash not a ref_hash as parameter");
# TEST - Message is not empty
isnt($r_CodificaJson{message}, "", "Mesage of error NOT EMPTY [".$r_CodificaJson{message}."]");

# TEST -
%r_CodificaJson = CodificaJson(@params);
ok($r_CodificaJson{status} eq "ERROR","array not a ref_hash as parameter");
# TEST - Message is not empty
isnt($r_CodificaJson{message}, "", "Mesage of error NOT EMPTY [".$r_CodificaJson{message}."]");


# print Dumper(%r_CodificaJson);