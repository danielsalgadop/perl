#!/usr/bin/perl -w
# it also has Tests
use strict;
use Data::Dumper;
use Test::More 'no_plan';
use lib '.';
use cargadorUniversalJson;

my $path_jsones = "jsones";

my %r_universalJsonReader;

# if you want to skip tests include them with this block
SKIP: {
        skip "not now";
}  # SKIP blosc

# TODO -test when file dosent exists or yo dont have read permissions

#######################
# file_json is a very simple type {"key","value"}
# Also works for:
#{
#	"key":"value",
#	"key2":"value2"
#}
# just1 line just 1 json object
#######################
diag("TEST para jsones tipo1");
# TEST - json_tipo1.json Loaded OK
%r_universalJsonReader = universalJsonReader($path_jsones."/json_tipo1.json");
ok($r_universalJsonReader{status} eq "OK" ,"json_tipo1.json Loaded OK");

# TEST - HASH with keys/values Correctly loaded
my %hash_tipo1 = %{$r_universalJsonReader{ref_json_hash}};

ok(($hash_tipo1{1}{uno} == 1 and $hash_tipo1{1}{dos} == 2 and $hash_tipo1{1}{tres} == 3),"json_tipo1.json HASH with keys/values Correctly loaded");

# TEST - BAD formatted json DETECTED
%r_universalJsonReader = universalJsonReader($path_jsones."/json_tipo1_error.json");
ok($r_universalJsonReader{status} eq "ERROR" ,"json_tipo1_error.json BAD formatted json DETECTED");

# TEST - el MESSAGE ERROR no esta vacio
isnt($r_universalJsonReader{message} , "" , "el MESSAGE ERROR no esta vacio");

#######################
# file_json is an agregation, one per line (without comas at end of line)
#{"key","value"}
#{"key","value"}
#{"key","value"}
#######################

diag("TEST para jsones tipo2");
# TEST - json_tipo2.json Loaded OK
%r_universalJsonReader = universalJsonReader($path_jsones."/json_tipo2.json");
ok($r_universalJsonReader{status} eq "OK" ,"json_tipo2.json Loaded OK");
my %hash_tipo2 = %{$r_universalJsonReader{ref_json_hash}};

# TEST - json_tipo2.json HASH with keys/values Correctly loaded
ok(($hash_tipo2{1}{uno1} == 11 and $hash_tipo2{1}{dos1} == 21 and $hash_tipo2{1}{tres1} == 31 and $hash_tipo2{2}{uno2} == 12 and $hash_tipo2{2}{dos2} == 22 and $hash_tipo2{2}{tres2} == 32 and $hash_tipo2{3}{uno3} == 13 and $hash_tipo2{3}{dos3} == 23 and $hash_tipo2{3}{tres3} == 33 and $hash_tipo2{4}{uno4} == 14 and $hash_tipo2{4}{dos4} == 24 and $hash_tipo2{4}{tres4} == 34 and $hash_tipo2{5}{uno5} == 15 and $hash_tipo2{5}{dos5} == 25 and $hash_tipo2{5}{tres5} == 35),"json_tipo2.json HASH with keys/values Correctly loaded");
# TODO detect that key ref_json_hash_errors DOSENT exist

%r_universalJsonReader = universalJsonReader($path_jsones."/json_tipo2_error.json");

# TEST - json_tipo2_error.json ERROR status detected
ok($r_universalJsonReader{status} eq "ERROR" ,"json_tipo2_error.json ERROR status detected");
# TODO detect that key ref_json_hash DOSENT exist

# TEST - json_tipo2_error.json WARNING status detected
%r_universalJsonReader = universalJsonReader($path_jsones."/json_tipo2_warning.json");
ok($r_universalJsonReader{status} eq "WARNING" ,"json_tipo2_error.json WARNING status detected");

#######################
# Repeated keys are LOST. You loose 1 of them
#{
#	"key":"value",
#	"key":"value2"
#}
#######################
%r_universalJsonReader = universalJsonReader($path_jsones."/json_tipo1_repeated_keys.json");
ok((scalar keys $r_universalJsonReader{ref_json_hash}{1}) == 2 ,"one of the keys 'uno' is LOST");

#######################
# Problemas cargando algo que deberica funcionar
# Funciona
#######################
%r_universalJsonReader = universalJsonReader($path_jsones."/json_produccion_con_problemas.json");
ok($r_universalJsonReader{status} eq "OK" ,"json que daba problemas esta ok");
# print Dumper(%r_universalJsonReader);