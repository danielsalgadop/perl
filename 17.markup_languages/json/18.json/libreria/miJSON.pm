#!/usr/bin/perl -w

use strict;
use JSON;

# -------------------------------------------------------------------------------------------------------
#  Nombre: miJSON.pm                               Version: 1.0
# -------------------------------------------------------------------------------------------------------
# funciones comunes hechas con JSON

my $debug=0;

#   Function: BuscarEnArrayJson

#       Recibe un ref@Array que es el resultado de @Array = <FICH>. siendo el FICH jsones linea por linea
#       Recibe un ref%patrones
#       Devuelve numero de lineas que tienen TODOS los parametros en una misma linea
#       Si hay error $return{status}="la descripcion del error"; sino $return{status}="OK";
#       Sustituye al anterior &BuscarenArray
# TODO mete el modo (#      Recibe: MODO (l- numero de veces que aparece el patron), (i- devuelve ref_array con Array sin las lineas))
sub BuscarEnArrayJson($$)
{
    my $refArray = shift;          # array de jsones concatenados
    my $refHash  = shift;          # hash con key => values que devemos encontrar
    my @Array    = @{$refArray};
    my %patrones = %{$refHash};
    my %return;
    $return{status} = "OK";
    # NO han llegado los Argumentos necesarios
    if (!@Array or !%patrones) {
        $return{status} =
            "ERROR en ["
          . (caller(0))[3]
          . "] Falta de Argumentos";    # con (caller(0))[3] saco el nombre de la subrutina
        return (%return);
    }
    ### variables locales
    my $json = new JSON;

    my @lineasSINpatron;
    my @lineasCONpatron;

    # recorro el array de jsones
    foreach my $unalinea (@Array) {
        my $ref_hash_decoded = $json->decode($unalinea);
        my %decoded          = %{$ref_hash_decoded};
        my $tiene_todos =
          1;    # flag que determina si se han encontrado todos los  valores en la linea
        foreach my $keybuscada (keys(%patrones)) {
            unless ($decoded{$keybuscada} == $patrones{$keybuscada})
            { # con que exista un solo keybuscada que no este en decoded o que no coincida con el valor, no nos vale
                $tiene_todos = 0;
                last;
            }
        }
        if ($tiene_todos) {
            push(@lineasCONpatron, $unalinea);
        }
        else {
            push(@lineasSINpatron, $unalinea);
        }
    }

    $return{array_jsones_lineas_con_patron} = \@lineasCONpatron;
    $return{array_jsones_lineas_sin_patron} = \@lineasSINpatron;
    $return{num_lineas_con_patron}          = @lineasCONpatron;
    $return{num_lineas_sin_patron}          = @lineasSINpatron;
    return (%return);
}

# Recibe un json y lo concatena a un file
#
#
sub ConcatJson2File($$)
{
    my $json = shift;
    my $file = shift;
    my %return;
    $return{status} = "OK";
    $return{value}  = "";
    if ($json =~ /\{\s*\}/ or !$file) {
        $return{status} = "ConcatJson2File, no ha recibido los 2 argumentos necesarios";
        $return{status} .= " FALTA Json " if $json =~ /\{\s*\}/;
        $return{status} .= " FALTA File " unless $file;
    }
    else {
            # json y file recibidios por funcion
            print "json=[".$json."] file=[".$file."]<br>" if $debug;
        if (open(FILE, ">> " . $file)) {
            print FILE $json . "\n";
            close(FILE);
        }
        else {
            $return{status} = "no he podido abrir fichero =["
              . $file . "]";
        }
    }
    return (%return);
}

# --- Lee una linea de archivo JSON. --- #
# Receives: path to file
# For coherence with fileJSON2HashAgregado and use inside universalJsonReader it will always return hash{1}=
# BE AWARE -  if a json has repeated keys it will construct only 1 of them! you will loose the information of the other
sub fileJSON2Hash($){

  # my $archivo=shift;
  my $file_in_string;
  # --- DATOS GENERICOS. --- #
  {
    # We change $/ all file into a uniq string, so we can charge files
    # simple {"key":"value"}
    # and
    # extended
    # {
    #  "key":"value"
    # }
    local $/=undef;
    open(FILE,shift);
    $file_in_string = <FILE>;
    close FILE;
  }

  my $jsoncfg = new JSON;
  my $ref_perlhash;
  eval {
      $ref_perlhash = $jsoncfg->decode($file_in_string);
  };
  if($@){     # PROBLEMS
        chomp($@);
        return(status=>"ERROR",message=>$@);
  }
  my %hash_json;
  $hash_json{1} = $ref_perlhash;   # line number 1, hardcoded
  return(status=>"OK",ref_json_hash=>\%hash_json);
}#Cierra sub fileJSON2Hash($)

# --- Decodifica JSON. --- #
# Receives: a string with json (literal json)
# bad formated json DETECTED
sub DecodificaJson($){

  # my $dato=shift;
  my $json= new JSON;
  my $ref_perlhash;

  eval {
      $ref_perlhash = $json->decode(shift);
  };
  if($@){     # PROBLEMS
    chomp($@);
    return(status=>"ERROR",message=>$@);
  }
  # my $enperl = $json->decode($dato);
  # my %Hash = %$enperl;

  return(status=>"OK",ref_json_hash=>$ref_perlhash);

}#Cierra sub DecodificaJson($)

# --- Codifica JSON. --- #
sub CodificaJson($){

  # --- Recibe una referencia a un hash. --- #
  my $dato=shift;
  return(status=>"ERROR",message=>"Emtpy Json") if (!$dato or $dato=~/^\s*$/);
  my $json=new JSON;
  my $en_json;
  eval {
    $en_json=$json->encode($dato);
  };
  if($@){
    chomp($@);
    return(status=>"ERROR",message=>$@);
  }

  return(status=>"OK",json=>$en_json);

}#Cierra sub CodificaJson($)



# Function: fileJSON2HashAgregado similar to FileJSON2Hash but the origin file is an agregation of jsons
# Receives a path to file. These file must be an agregation of jsons
# Returns a %return{status} = (OK|ERROR|WARNING)
#     OK      - all json are OK
#       %return{ref_json_hash} = ref to a hash
#     ERRROR  - none jsons are OK.
#       %return{ref_json_hash_errors} = a ref to an array
#     WARNING - some lines are OK, some are ERRORS
#       %return{ref_json_hash} = ref to a hash
#       %return{ref_json_hash_errors} = a ref to an array
#
# Returns a return{hash} = Content of file (json) as hash. The keys are the number of line of the origin file
sub fileJSON2HashAgregado($){
  my $path_to_file = shift;

  my %hash_with_jsons;

  open (FILEJSON, $path_to_file);
  my @file = <FILEJSON>;
  close FILEJSON;

  my $cont = 0;
  my %hash_with_badly_formated_lines;

  foreach my $unalinea(@file){
    # print "\$unalinea".$unalinea."<br>";
    $cont++;

    my %r_DecodificaJson = DecodificaJson($unalinea);

    if ($r_DecodificaJson{status} eq "OK"){
      $hash_with_jsons{$cont} = $r_DecodificaJson{ref_json_hash};
    }
    else{ # Bad formatted in a line
      $hash_with_badly_formated_lines{$cont} = "BADLY formated json error [".$r_DecodificaJson{message}."]";
    }
  }
  # construct %return
  my %return;
  if(scalar keys %hash_with_badly_formated_lines == 0){ # OK
    $return{status} = "OK";
    $return{ref_json_hash}= \%hash_with_jsons;
  }
  elsif(scalar keys %hash_with_jsons == 0){ # ERROR
    $return{status} = "ERROR";
    $return{ref_json_hash_errors} = \%hash_with_badly_formated_lines;
  }
  else{
    $return{status} = "WARNING";
    $return{ref_json_hash_errors} = \%hash_with_badly_formated_lines;
    $return{ref_json_hash}= \%hash_with_jsons;

  }
  return(%return);
}
1;