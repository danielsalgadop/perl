#!/usr/bin/perl -w
# it also has Tests
use strict;
use Data::Dumper;
use Test::More 'no_plan';
use lib '.';
use miJSON;

my %r_DecodificaJson;
%r_DecodificaJson = DecodificaJson('{"uno":"1","dos":"2"}');
ok($r_DecodificaJson{status} eq "OK","Well formated JSON detected");
# print Dumper(%r_DecodificaJson);

%r_DecodificaJson = DecodificaJson('{"uno":"1","dos"::::"2"}');  # Bad formated
ok($r_DecodificaJson{status} eq "ERROR","Bad formated JSON detected");

### Message is not empty
isnt($r_DecodificaJson{message}, "", "Mesage of error [".$r_DecodificaJson{message}."]");
