#!/usr/bin/perl -w
use strict;
use JSON;
use lib '.';
use miJSON;


# Function universalJsonReader
#
# Receives:
#	$path_al_json
#
# Returns:
# 	hash{status} = ERROR and hash{message} = descritption of failure
# or 
#  	returns form fileJSON2Hash and fileJSON2Hash
#
# What does it do?
# Comprueba que $path_al_json existe y que es leible
# lanza fileJSON2Hash and fileJSON2HashAgregado
sub universalJsonReader($){
	my $path_al_json = shift;

	# file must exist
	return(status=>"ERROR",message=>"File $path_al_json must exist") unless (-e $path_al_json);
	# file must have read permissions
	return(status=>"ERROR",message=>"File $path_al_json must be readable") unless (-r $path_al_json);

	# executes fileJSON2Hash if !OK executes fileJSON2HashAgregado
	my %return =  fileJSON2Hash($path_al_json);
	return %return if $return{status} eq "OK";   # OJO you never get the return form ERROR in fileJSON2Hash
	fileJSON2HashAgregado($path_al_json);
}
1;