#!/usr/bin/perl
# use lib '/home/dan/cosas_hechas/ejemplos_mios/perl/18.json/JSON-2.15/lib';
use JSON;
use Data::Dumper;
use strict;
use warnings;
# datos
my %hash = ( 'key1', 'value1', 'key2', 'value2', 'key3', 'value3' );
my $string_con_json = '{"borrable":"adios me piro","titulo":"tiutlo chulo","contenido":"contenido","correo":"cuenta@bt.com","tipo":"petcion","estado":"inicial","epoch_inicial":{"borrable":"aqui si","aniomesdia":"20090916","hora":"18","minuto":"45"}}';
my $json = new JSON;
#encode_jso  y $json->encode son "functrionally identical"
# no pueden existir dos keys iguales, es decir que si en json existe esto {"algo":"se va a sobrescibir","algo":"esto prevalecera, y se borrara la anterior"}, acabara siendo asi $hash( algo, esto prevalecera....);

# ------------------------------------------------------------------------- #
# CODIFICAR JSON
my $hash_ref  = \%hash;
my $json_text   = $json->encode($hash_ref);
#print $json_text."\n";


# DECODIFICA JSON
my $enperl = $json->decode($string_con_json);
%hash = %$enperl;
#foreach $a(keys(%hash)){
#	print "key ".$a." value ".$hash{$a}."\n";
#}
#pretty hace q el output sea es distintas lineas en vez de una
#$pretty_text = $json->pretty->encode($hash_ref);
#print "pretty text->".$pretty_text; 
#print "fin pretty text \n";


# aniadir un obj json
my %epoch_nueva = ("minuto","3","hora","34","aniomesdia","20088284");
$hash{'epoch_nueva'}= \%epoch_nueva;
# aniadir/modificar un valor nivel1 (igual que aniadir pares de elementos)
$hash{'aniadido'} = "modificado desde perl";
# borrar un par nivel1 nombre:valor
delete $hash{'borrable'};
# borrar un par nivel2 nombre:valor
delete $hash{'epoch_inicial'}->{'borrable'};
# meter letras raras en contenido (se auto-escapa el simbolo " y el simbolo \)
$hash{'contenido'} = 'asldfjksSD/&/(/))\"http://www.google.com:="·$%"·%·"·%modificado desde perl';
#$hash{'contenido2'} = 'asldfjksSD/&/(/))\"http://www.google.com:="·$%"·%·"·%modificado desde perl';
# modificar un valor en segundo nivel (cambiar la hora de epoch_inicial)
$hash{'epoch_inicial'}->{'hora'}="22";
# recoger valor a nivel 1
my $estado = $hash{'estado'};
print "\n estado ===> ".$hash{'estado'}."\n";
# recoger valor a nivel 2
my $hora_epoch_inicial = $hash{'epoch_inicial'}->{'hora'};
print " hora epoch inicial ===> ".$hora_epoch_inicial."\n";

# comprobar si existe/definido/verdadero un valor o un key nivel1 , la verdad que no entinedo muy bien la diferencia entere estas tres
if (exists $hash{'titulo'}){
	print "titulo EXISTE aunque puede tener valor indefinido\n";
}
if (defined $hash{'titulo'}){
	print "titulo esta DEFINIDO\n";
}
if ($hash{'titulo'}){
	print "key titulo VERDADERO\n";
}

# comprobar si existe/definido/verdadero un valor nivel1
if (exists $hash{'epoch_nueva'}->{'minuto'}){
	print "epoch->minuto EXISTE aunque puede tener valor indefinido\n";
}
if (defined $hash{'epoch_nueva'}->{'minuto'}){
	print "epoch_nueva -> minuto esta DEFINIDO\n";
}
if ($hash{'epoch_nueva'}->{'minuto'}){
	print "key epoch_nueva -> minuto VERDADERO\n";
}
print "\n";

#$json_text   = $json->pretty->encode($hash_ref);
$json_text   = encode_json $hash_ref;
print $json_text;
print "\n";
print Dumper(%hash);
# ------------------------------------------------------------------------- #

#print "\n\n\n ahora con el tipo borde de XS\n";
# he tenido que aniadir todo el contenido de JSON-XS-2-25 dentro de JSON-2.15 para que funcionase
#use lib '/home/dan/cosas_hechas/ejemplos_mios/perl/json/JSON-XS-2.25';
#use JSON::XS;
#$string_con_json_xs = encode_json $hash_ref;
#$enperl_xs  = decode_json $string_con_json;

#print $string_con_json_xs."\n";
#print Dumper($enperl_xs);

print "=================================== CODIFICAR UN JSON complejo ===================================\n";

# devuelvo la respuesta %return
my %hash_en_perl = ("e323232rror"=> "uno", "error2"=>"dos");
my $hash_en_json = &CodificaJson(\%hash_en_perl);
print "hash_en_json\n".$hash_en_json."\n";

# my %return2 = {"result"=> "toe", "errores" => \@errores3};
# my $json = &CodificaJson(\%return2);

my @array_en_perl = ("chuchuchu","chachacha");
my $array_en_json = &CodificaJson(\@array_en_perl);
print "array_en_json\n".$array_en_json."\n";

my %hash_interno = (
		"key1" => "valor key1",
		"key2" => "valor key2",
	);
my %hash_de_hashes = (
    "un string" => "valor de un string",
    "hash"      => \%hash_interno,
   );
my $hash_de_hashes_en_json = &CodificaJson(\%hash_de_hashes);
print "hash_de_hash\n".$hash_de_hashes_en_json."\n";

# se define directamente el hash como referencia a un hash
my $hash_referencia_a_hash_anonimo = { "key_raro1" => "value raro1"};  # fijate que se declara con un $
my $hash_referencia_a_hash_anonimo_json = &CodificaJson($hash_referencia_a_hash_anonimo);
print "hash_referencia_a_hash_anonimo_json\n".$hash_referencia_a_hash_anonimo_json."\n";

# para esto estoy haciendo estas pruebas
my $ref_errores_array_anonimo = ["valor1","valor2"];
my @array_tal_cual = qw(sdaf asdf);
my %hash_con_string_y_array = (
	"string" => "valor de string",
	"ref" => $ref_errores_array_anonimo,
	"array tal cual" => \@array_tal_cual
	);
my $hash_con_string_y_array_json = &CodificaJson(\%hash_con_string_y_array);
print "hash_con_string_y_array_json\n".$hash_con_string_y_array_json."\n";






################## FUNCIONES

# --- Codifica JSON. --- #
sub CodificaJson($){

  # --- Recibe una referencia a un hash. --- #
  my $dato=shift;

  my $json=new JSON;

  # $json = $json->allow_nonref(1);
  my $informacion=$json->encode($dato);

  return($informacion);

}#Cierra sub CodificaJson($)
