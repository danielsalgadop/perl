#!/usr/bin/perl -w
use strict;
use diagnostics;

my $foo = "My bonnie lies over the ocean";

print "Enter a pattern:\n";
while (<STDIN>) {
   my $pattern = $_;
   if (not ($pattern =~ /^\/.*\/[a-z]?$/)) {
      print "Invalid pattern\n";
   } else {
      my $x = eval "if (\$foo =~ $pattern) { return 1; } else { return 0; }";
      if ($x == 1) {
         print "Pattern match\n";
      } else {
         print "Not a pattern match\n";
      }
   }
   print "Enter a pattern:\n"
}

