#!/usr/bin/perl
use warnings;
use strict;
# quotemeta
# he tenido problemas detectando un sting con asterisco con un regexp
# me ocurrio en proyecto 77.2.modificacions_SAPs_7750_via_WEB
my $string_con_asterisco="toetoe*";

# CURIOSAMENTE con asterisco no se reconoce a si mismo !!!
# si quito asterisco sí se reconoce
if($string_con_asterisco=~/^$string_con_asterisco$/){  # <<<<<<<<<< !!!!!!!!!!!!!!!!!
	print "detectado\n";
}
else{
	print "NO detectado\n";
}

print "string_con_asterisco [".$string_con_asterisco."]\n";
# $string_con_asterisco=~s!\*!\\*!;   # escapar asterisco
# $string_con_asterisco = quotemeta($string_con_asterisco);
print "string_con_asterisco [".$string_con_asterisco."]\n";
if($string_con_asterisco=~m/^\Q$string_con_asterisco\E$/){   # <<<< !!!!! se soluciona con \Q y con \E
	print "detectado\n";
}
else{
	print "NO detectado\n";
}
