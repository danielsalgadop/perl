#! /usr/local/bin/perl

# ============================================================================

# $Id: example1.pl,v 4.0 2001/10/15 13:13:17 dtown Rel $

# Copyright (c) 2000-2001 David M. Town <dtown@cpan.org>
# All rights reserved.

# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.

# ============================================================================

use strict;
use lib '/tmp/Net-SNMP-5.2.0/lib/';
print "\@INC is @INC\n";

use Net::SNMP;

