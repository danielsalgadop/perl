#!/usr/bin/perl
# este lib usa variables globales dentro de funciones q script_que_no_usa_todas_funciones.pl no usa

use warnings;
use strict;
use feature 'say';
say "SE HA LLAMADO A lib.pm";

sub funcion_ok(){
	our $variable_super_global;
	say "valor de variable_super_global [$variable_super_global]";
}

sub funcion_especifica_de_completo(){
	# que usa su propia variable_super_global
	our $variable_super_global_especifica;
	say "valor de variable_super_global_especifica [$variable_super_global_especifica]";
}
1;