#!/usr/bin/perl -w
use strict;
use Data::Dumper;

use lib '.';
use libreria_maestra;  # es esta la que apunta al resto de librerias_hijas


# a esta libreria se accede desde libreria_maestra.pm => libreria_hija1
funcionEnLibreriaHija1();

# esta funcion se accede desde libreria_maestra.pm =>libreria_hija1.pm=>libreria_hija2.pm
calcularPi();

# funcion escondida tras funcionEscondida que se llega desde index.pl => libreria_maestra.pm => libreria_escondida 1=>2=>3=>4
funcionEscondida();  # esta funcion esta al final de unas librerias en serie q se llaman unas a otras