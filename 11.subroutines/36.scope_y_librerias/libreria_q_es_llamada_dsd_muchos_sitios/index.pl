#!/usr/bin/perl -w
use strict;
use Data::Dumper;

use lib '.';
use libreria_maestra;  # es esta la que apunta al resto de librerias_hijas
use libreria_llamada_muchas_veces;

# ¿que ocurreo cuando hay una libreria llamada varias veces?
# no entiendo muy bien, pues en el output parece que solo fuese llamada 1 sola vez la libreria_llamada_muchas_veces, por que su contenido solo aparece una vez


# CONCLUSION:
# perl se las apaña para ejecutar SOLO UNA VEZ la libreria_llamada_muchas_veces. He probado a quitar solo de index, o solo de libreria_hija1 y libreria_hija_2, haga lo que haga se ejectuara solo una vez. NO DA NI ERROR NI WARNING
