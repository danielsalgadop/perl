#!/usr/bin/perl -w
#  https://kb.wisc.edu/middleware/page.php?id=4309
use strict;
use Data::Dumper;
use lib '.';

print "libreria_llamada_muchas_veces LLAMADA\n";
print "caller=[".(caller(0))[3]."]\n";     # Me 
print "caller=[".(caller(1))[3]."]\n";     # Parent
print "caller=[".(caller(2))[3]."]\n";
1;
