#!/usr/bin/perl -w
use strict;

# problema surgido haciendo el proycto hatteras Version2
# parece que dentro de una subrutina no puedo acceder al valor del programa principal
my $valor;
my @array = ( "asd", "q234", "rtyurt" );

foreach (@array) {

   # la clave esta en la linea de abajo. Esto no funciona foreach $valor(@array)
    $valor = $_;
    print "desde main " . $valor . "\n";
    &rutina();
}

sub rutina {
    print "desde subrutina " . $valor . "\n";
}

