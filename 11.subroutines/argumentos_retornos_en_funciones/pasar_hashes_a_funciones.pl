#!/usr/bin/perl -w
use strict;
use Data::Dumper;
# Study of passing Hash by reference and by value
# I CANT CHANGE origianal data structure by refernce !!!!! I dont understand

#######################################
##### como recibe una funcion un hash #
#######################################
our %hash;
$hash{1} = "valor uno";
$hash{2} = "valor dos";
$hash{3} = "valor tres";

# paso por referencia
sub recibeHashPorReferencia($){
	print "=== dentro de ".(caller(0))[3]."===\n";
	my $ref_hash = shift;
	my %hash = %{$ref_hash};
	print Dumper(%hash)."==============\n";
}

# paso por referencia y Modifica DeferencingInsideSub
# Derefencing inside subroutine
sub recibeHashPorReferenciaYModificaDeferencingInsideSub($){
	# print "=== dentro de ".(caller(0))[3]."===\n";
	my $ref_hash = shift;
	%hash = %{$ref_hash};
	$hash{keysAnyadidoPorReferenciaDeferencingInsideSub}="valor anyadido por ".(caller(0))[3];
	# print "==============\n";
}

# paso por referencia y Modifica OpcionUsingHashDirectly
# using the hash directly
sub recibeHashPorReferenciaYModificaOpcionUsingHashDirectly($){
	# print "=== dentro de ".(caller(0))[3]."===\n";
	$_[0]{keysAnyadidoPorReferenciaOpcionUsingHashDirectly}="valor anyadido por referencia ".(caller(0))[3];
	# print Dumper($_[0])."==============\n";
}

# Este es una mezcla de
# recibeHashPorReferenciaYModificaDeferencingInsideSub
# recibeHashPorReferenciaYModificaOpcionUsingHashDirectly
sub recibeHashPorReferenciaYModificaCombinacionOpcionesFuncionaConCiertoOrden($){
	# this line (using hash directy) ONLY works if is invoked PREVIOSLY to the oder way (dereferencing inside subroutine)
	$_[0]{keysAnyadidoPorReferenciaOpcionUsingHashDirectly_testing_order}="valor anyadido por referencia ".(caller(0))[3];

	# Dereferencing 
	my $ref_hash = shift;
	%hash = %{$ref_hash};
	$hash{keysAnyadidoPorReferenciaDeferencingInsideSub_testing_order}="valor anyadido por ".(caller(0))[3];

	# Somehow derefencings (empties $_)
	# This line DOES NOTHING. The hash is not changed
	$_[0]{NOT_EXISTS_testing_order}="valor anyadido por referencia ".(caller(0))[3];
}


####################################
# paso por valor
####################################

# tambien valen estas
#sub recibeHash(@)
#sub recibeHash(%)
sub recibeHash{
	print "=== dentro de ".(caller(0))[3]."===\n";
	my (%hash) = @_;
	print Dumper(%hash)."==============\n";
}

# tambien valen estas
#sub recibeHash(@)
#sub recibeHash(%)
sub recibeHashYModifica{
	print "=== dentro de ".(caller(0))[3]."===\n";
	my (%hash) = @_; # internal COPY of hash
	# This line DOES NOTHING. The hash is not changed
	$hash{NOT_EXISTS_recibeHashYModifica}="valor anyadido por referencia";
	print Dumper(%hash)."==============\n";  # <<< NOT_EXISTS_recibeHashYModifica is only inside the copy of %hash
}


########
# mail #
########
# recibeHashPorReferencia(\%hash);
# Yoy cannot do both in the same subroutine
recibeHashPorReferenciaYModificaDeferencingInsideSub(\%hash);
recibeHashPorReferenciaYModificaOpcionUsingHashDirectly(\%hash);
recibeHashPorReferenciaYModificaCombinacionOpcionesFuncionaConCiertoOrden(\%hash);
recibeHash(%hash);
recibeHashYModifica(%hash);

print "\nVALOR FINAL DE HASH\n";
print Dumper(%hash);