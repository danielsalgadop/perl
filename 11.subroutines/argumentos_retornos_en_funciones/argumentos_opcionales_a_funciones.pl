#!/usr/bin/perl
use warnings;
use strict;
use Data::Dumper;

sub funcion{
	my ($arg1,$arg2) = @_;
	my $this_function = (caller(0))[3];
	print "this_function_name = ".$this_function."\nel dumper de lo recibido[".Dumper(@_)."]\n";
	# my $arg1;
	# my $arg2;

	unless ($arg1){
		 $arg1 = "valor_1_por_defecto" ;
	}
	unless ($arg2){
		 $arg2 = "valor_2_por_defecto" ;
	}

	print "valores Dentro de funcion arg1=[".$arg1."] arg2=[".$arg2."]\n";
}

sub funcionNoFunciona{
	# Curiosamente este codigo siempre me daba 
	# "Use of uninitialized value $arg1 in concatenation (.)"
	my @arg_funcion = @_;
	print "el dumper de lo recibido[".Dumper(@arg_funcion)."]\n";
	my $arg1;
	my $arg2;
	unless ($arg_funcion[0]){
		 $arg1 = "valor_1_por_defecto" ;
	}
	unless ($arg_funcion[1]){
		 $arg2 = "valor_2_por_defecto" ;
	}
	print "valores Dentro de funcion arg1=[".$arg1."] arg2=[".$arg2."]\n";
}

# visto aqui
# http://stackoverflow.com/questions/8124138/how-to-pass-optional-parameters-to-a-perl-function
# LA CLAVE SON LOS PUNTOS Y COMAS
sub funcionExplicitamenteDeclarandoArgumentosOpcionales($$;$){
	my ($obligatorio1,$obligatorio2,$opcional) = @_;
	print "obligatorio1 [".$obligatorio1."] obligatorio2 [".$obligatorio2."]";
	print " opcional [".$opcional."]" if $opcional;
	print "\n";
}


sub funcionPuedeRecibirNadaORef2Hash{
	my %hash;
	if(@_){
		my $ref_hash = shift;
		%hash = %{$ref_hash};
		
	}
	if(%hash){
		print "Recibo hash\n".Dumper(%hash);
	}
	else{
		print "NO RECIBO HASH\n";
	}
}



# # llamada sin argumentos
# funcion;

# # llamada con 1 argumento
# funcion("arg1");

# # llamada con 2 argumentos
# funcion("arg2.1","arg2.2");

# # llamada con 2 argumentos obligatorios
# funcionExplicitamenteDeclarandoArgumentosOpcionales("VALOR_obligatorio1","VALOR_obligatorio2");
# # llamada con 2 argumentos obligatorios y 1 opcional
# funcionExplicitamenteDeclarandoArgumentosOpcionales("VALOR_obligatorio1.1","VALOR_obligatorio2.1","opcional");

#
funcion('uno', 'dos');
funcionPuedeRecibirNadaORef2Hash();
funcionPuedeRecibirNadaORef2Hash({keys=>"valor"});
