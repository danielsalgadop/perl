#!/usr/bin/perl -w
use strict;
use Data::Dumper;
# perl script.pl -enTerminal 
# si pones GetOptions( 'enTerminalalgo' =>\$variable_algo);
# if(@ARGV)  # estara vacia
# si buscas @ARGV lineas __antes__ de GetOptions estara llena
# # 
# 
# http://perldoc.perl.org/Getopt/Long.html

# Por definición una opcion es opcional, es decir el script hara algo sin las opciones
# acepta opciones cortas    -c
# acepta opciones largas    --larga
# acepta tb valores         --valor 44
# acepta tb valores         --valor=44
#
# BUNDLING. (not enabled by default) cuando varias opciones se "arrejuntan" asi -a -b -c   se puede poner como -abc

# si una opcione es abcdef y pones -abcd asume que hablas de abcdef (digamos q "rellena")
# si dos opciones comparten las primeras lineas y no puede distingir te da: "Option abcd is ambiguous (abcdefg, abcd)"

use Getopt::Long;

# disparadores de tests
my $simple_options                 = 0;
my $opcion_negada                  = 0;
my $opcion_sumada                  = 0;    # da fallo
my $mezclando_optciones            = 0;
my $opciones_con_valores           = 1;
my $opciones_con_multiples_valores = 0;

if ($simple_options) {

    # espera --verbose y espera --all
    my $verbose = '';    # option variable with default value (false)
    my $all     = '';    # option variable with default value (false)
    GetOptions( 'verbose' => \$verbose, 'all' => \$all );

    print "verbose=" . $verbose . "\n";
    print "all=" . $all . "\n";
}

if ($opcion_negada) {
    my $verbose = 'default'
        ; # option variable with default value (false) .A negatable option is specified with an exclamation mark ! after the option name
    GetOptions( 'verbose!' => \$verbose );    # fijate en la exclamacion !
    print "verbose=" 
        . $verbose . "\n"
        ; # esta variable tiene tres posibles valores, default, verbose (1) o noverbose (0)
}

if ($opcion_sumada) {

# OJO , me da este fallo "Argument "" isn't numeric in addition (+) at /usr/share/perl/5.14/Getopt/Long.pm line 532."
# cuenta el numero de veces que se ha incluido la opción
    my $verbose = '';    # option variable with default value (false)
    GetOptions( 'verbose+' => \$verbose );    # fíjate en la suma +
    print "verbose=" . $verbose
        . "\n";    # esta variable suma las veces que aparezca en la entradaa
}

if ($mezclando_optciones) {

# To stop Getopt::Long from processing further arguments, insert a double dash -- on the command line:  (como en GIT)
# en esta instancia (perl script --option -- -all) all no sera tratada por GetOptions sino que entrara en @ARG (as usual)
    my $verbose = '';    # option variable with default value (false)
    my $all     = '';    # option variable with default value (false)
    GetOptions( 'verbose' => \$verbose, 'all' => \$all );
    print "verbose=" . $verbose . "\n";
    print "all=" . $all
        . "\n";    # si pones -- --all (all despues de --) all no se rellenara

}
if ($opciones_con_valores) {

# Three kinds of values are supported: integer numbers (=i), floating point(=f) numbers, and strings (=s).
# Vale tanto --tag_string=algo como --tag_string algo o --tag_string="algo", las 3 rellenan $tag_string con algo
# The equals sign indicates that this option requires a value. The s indicates string
    my $tag_string      = 'default';
    my $tag_integer     = '69';
    my $tag_float       = '45,4';
    my $optional_string = 'default_optional_sting';
    GetOptions(
        'tag_string=s'      => \$tag_string,
        'tag_integer=i'     => \$tag_integer,
        'tag_float=f'       => \$tag_integer,
        'optional_string:s' => \$optional_string
    );
    print "tag_string=" . $tag_string . "\n";
    print "tag_integer=" . $tag_integer . "\n";
    print "tag_float=" . $tag_float . "\n";
    print "optional_string=" . $optional_string . "\n";

# if no suitable value is supplied, string valued options get an empty string '' assigned, while numeric options are set to 0

    # No veo mucha diferencia entre optional string o poner un default value
}

if ($opciones_con_multiples_valores) {

    # Simply specify an array reference as the destination for the option

    my @nombres             = qw(por defecto muchos nombres);
    my @separados_por_comas = qw(valores iniciales);

#NOMBRES= si instancias asi perl script.pl -nombres uno -nombres dos -nombres tres. Concatena estos nombres a @nombres
#SEPARADOS POR COMAS =  si instancias perl script.pl --separados_por_comas nuevo,valor,separado. Concatena a separados_por_comas

    GetOptions(
        'nombres=s'             => \@nombres,
        'separados_por_comas=s' => \@separados_por_comas
    );
    print "nombres " . Dumper(@nombres) . "\n";
    @separados_por_comas = split( /,/, join( ',', @separados_por_comas ) );
    print "separados_por_comas " . Dumper(@separados_por_comas) . "\n";

    # EXPERIMENTAL, Options can take multiple values at once
}



# Me he quedado en Options with hash values
