#!/usr/bin/perl -w
use strict;
use LWP::UserAgent;
my $browser = LWP::UserAgent->new();
$browser->env_proxy();    # if we're behind a firewall
my $url      = 'http://www.guardian.co.uk/';
my $response = $browser->get($url);
die "Hmm, error \"", $response->status_line(), "\" when getting $url"
  unless $response->is_success();
my $content_type = $response->content_type();
die "Hm, unexpected content type $content_type from $url"
  unless $content_type eq 'text/html';
my $content = $response->content();
die "Odd, the content from $url is awfully short!"
  if length($content) < 3000;

if ($content =~ m/UK|Madonna|Arkansas/i) {
    print "<!-- The news today is IMPORTANT -->\n", $content;
}
else {
    print "$url has no news of ANY CONCEIVABLE IMPORTANCE!\n";
}
