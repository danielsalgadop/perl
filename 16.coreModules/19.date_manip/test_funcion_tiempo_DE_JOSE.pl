#!/usr/local/bin/perl
use lib '/home/dan/cosas_hechas/ejemplos_mios/perl/19.date_manip/Date-Manip-5.54/lib';
use Date::Manip;

#Saco la fecha normal (No Unix)
print "Imprime la fecha actual en formato normal";
print "\n";
print scalar localtime(time);
print "\n";

#Saco la fecha en normal desde el formato Unix (epoch) 
print "Imprime la fecha actual en formato normal desde el formato Unix";
print "\n";
print scalar localtime(1021363245);
print scalar localtime(1021363245);
print "\n";

#Saco la fecha de manhana
$hora_actual=time; #time, es la fecha actual
$manhana=$hora_actual+86400; #86400, un dia entero en segundos (24horas *60minutos *60segundos)
print "Imprime la fecha de manhana desde el formato de hora normal";
print "\n";
print scalar localtime($manhana); #Imprime la hora en formato CET 
print "\n";

#Devuelve el valor numerico del dato que se esta solicitando 
($seconds,$minutes,$hour,
 $monthday,$month,$year,
 $weekday,$yearday,$dst_flag)=localtime(time);
print "Devuelve el valor numerico del dato que se esta solicitando";
print "\n";
print $hour;
print "\n";
print $monthday;
print "\n";
print $weekday;
print "\n";


#Para imprimir un dia de la semana por su nombre y no con numeros como antes 
$dia=(Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday)[(localtime(time))[4]];
print "Imprime un dia de la semana por su nombre y no con numero como antes";
print "\n";
print $dia; #Imprimo el Jueves, lo mismo se puede hacer para los meses del anho
print "\n";

#Otra forma de hacer lo anterior
#use Time::localtime;
#$tomorrow=time+86400;
#$timeVar=localtime($tomorrow); #Asigno localtime a una variable escalar
#$day=(Sunday,Monday,Tuesday,
#      Wednesday,Thursday,Friday,Saturday)[$timeVar->wday]; #Asigno la variable escalar al metodo wday
#print "Otra forma del punto anterior";
#print "\n";
#print $day; #Imprimo el Jueves
#print "\n";

print "Imprime la fecha actual en formato Unix, se hace una conversion pasando la fecha en formato normal";
print "\n";
use Date::Manip;
#time zone info (if necessary)
&Date_Init("TZ=WET");
$someday=localtime(time);
print $someday;
print "\n";
$parsed=&ParseDate($someday);
print $parsed;
print "\n";
$epoch=&UnixDate($parsed,"%s");
print $epoch;
print "\n";
print scalar localtime($epoch);
print "\n";

#Otra forma de poner lo del punto de arriba
print "Otro metodo para imprimir lo del punto de arriba\n";
print scalar localtime(&UnixDate(&ParseDate(localtime(time)),"%s"));
print "\n";

#Para anhadir 12 horas a la hora actual
print "Para anhadir 12 horas a la hora actual";
print "\n"; 
$time=&DateCalc("today","+12 hours");
print scalar localtime(&UnixDate($time,"%s"));
print "\n";

#Para sustraer 12 horas a la hora actual
print "Para sustraer 12 horas a la hora actual";
print "\n";
$time=&DateCalc("today","-12 hours");
print scalar localtime(&UnixDate($time,"%s"));
print "\n";

#Para anhadir un periodo de tiempo aleatorio
print "Para anhadir un periodo de tiempo aleatorio";
print "\n";
$time=&DateCalc("today","+3 days 12 hours 30 minutes");
print scalar localtime(&UnixDate($time,"%s"));
print "\n";

#Para anhadir horario de oficina
print "Para anhadir horario de oficina";
print "\n";
$tiempo_oficina=&DateCalc("tomorrow","+3 business days");
print scalar localtime(&UnixDate($tiempo_oficina,"%s"));
print "\n";

#Para averiguar cuanto tiempo ha pasado entre dos fechas
print "Para averiguar cuanto tiempo ha pasado entre dos fechas";
print "\n";
print "El formato es: Anho, Mes, Semana, Dia, hora, minuto, segundo";
print "\n";
use Date::Manip;
#time zone info (if necessary)
&Date_Init("TZ=WET");
$parsed=&ParseDate($someday);
$tiempo_medio=&DateCalc(&ParseDate("12/17/07 11:29:07"),&ParseDate("12/18/07 14:29:07"),\$err,1);
print $tiempo_medio;
print "\n";
