#!/usr/bin/perl -w
#use strict; #(esto no lo puedo poner por que no se que hacer con \$err, parecen referencias a subrutinas, pero no lo he conseguido)
use lib '/home/dan/cosas_hechas/ejemplos_mios/perl/19.date_manip/Date-Manip-5.54/lib';
use Date::Manip;
my $version = DateManipVersion;
#print "Version ".$version."\n";
#my $date_actual=&DateCalc("today","+0 hours");
#my $date_pasada="2008071901:25:00";
#print "epoch actual: $date_actual\n";
#my $flag = Date_Cmp($date_actual,$date_pasada);
#print "\nflag $flag\n";
#my $eodate1 = ParseDate($date_actual);
#my $eodate2 = ParseDate($date_pasada);
## en la modificacion de lista de peticiones, me faltaba el flag 1 para que me lo diera en el formato que me interesaba
#my $delta = DateCalc($eodate2, $eodate1, \$err1,1);
#print "delta $delta\n";
## con el falg 2 , se salta los fines de semana y fiestas.
#my $delta2 = DateCalc($eodate2, $eodate1, \$err2,2);
#print "delta $delta2\n";

###################################################
# date manip # usado en el gestor de licencias
# para la parte de fecha_inicio e intervalo, tenia que calcular la fecha_final
#  las fechas tienen este formato aaaa-mm-dd

print '# para la parte de fecha_inicio e intervalo, tenia que calcular la fecha_final'."\n";
my $date = ParseDate("2009-13-10");
print "date que no funciona por el 13 $date\n";
my $date = ParseDate("2008-02-30");
print "date que no funciona por que no existe el 30 de febrero $date\n";
my $date = ParseDate("2008-02-29");
print "funciona por que en 2008 si que existio el dia 29 de febrero $date\n";
my $date = ParseDate("2009-02-29");
print "NO funciona por que en 2009 no existe el 2930 de febrero $date\n";
my $date = ParseDate("2009-10-20");
print "date que si funciona $date\n";

my $startDate = ParseDate("2009-02-1");
# asi se puede saber si la fecha es correcta o incorrecta
if ($startDate){
	print "FECHA CORRECTA\n";
}
else{
	print "FECHA INCORRECTA\n";
}
my $timeSpent = "2";
# %Q es yyyymmdd
my @format = ("%Q");
my $pre_fecha_fin = UnixDate(DateCalc($startDate, "+$timeSpent days"), @format);
print "start Date $startDate intervalo $timeSpent  futureDate $pre_fecha_fin\n";
my $anio = substr $pre_fecha_fin, 0, 4;
my $mes = substr $pre_fecha_fin, 4,2;
my $dia = substr $pre_fecha_fin, 6,2;
my $fecha_final = $anio."-".$mes."-".$dia;

print "fecha_final $fecha_final\n";

#my $date = ParseDate("in 3 weeks"); # esto hace el calculo desde hoy, no es lo que yo quiero
#print $date;
