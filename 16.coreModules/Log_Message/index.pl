#!/usr/bin/perl
use strict;
use diagnostics;
use warnings;
use Data::Dumper;

# BEWARE BEWARE BEWARE BEWARE BEWARE BEWARE BEWARE BEWARE BEWARE BEWARE BEWARE 
# Log::Message will be removed from the Perl core distribution in the next major release. Please install the separate liblog-message-perl package.
#

use Log::Message private => 0, config => 'config';
my $log = Log::Message->new
( 
	private => 1,
	level => 'log',
	# config => '/tmp/log_file_cf_file',  # other way to say where is config
);
$log->store
(
	'this is my first message'
);
$log->store
( 
	message => 'message #2',
	tag => 'MY_TAG',
	level => 'carp',
	extra => ['this is an argument to the handler'],
);
$log->store
( 
	message => 'message #3',
	tag => 'MY_TAG',
	level => 'carp',
	extra => ['this is an argument to the handler'],
);
my @last_five_items = $log->retrieve(5);
my @items = $log->retrieve
( 
	tag => qr/my_tag/i,
	message => qr/\d/,
	remove => 1,
);
@items = $log->final( level => qr/carp/, amount => 2 );
my $first_error = $log->first();
# croak with the last error on the stack
# $log->final->croak;
# empty the stack
$log->flush();