#!/usr/bin/perl
use strict;
use warnings;
use Storable qw (freeze retrieve store thaw);
use Data::Dumper;

my %hash1 = (
    "toe" => "valorToe",
    "eee" => "valoreee"
);

#################################
# Escribiendo data a un fichero #
#################################
store \%hash1, 'files_with_data_storable/hash1';

######################
# Recogiendo la info #
######################
my $hashref         = retrieve('files_with_data_storable/hash1');
my %hash1_retrieved = %{$hashref};

print Dumper(%hash1_retrieved);

##########################
# Serializing (marshall) #
##########################
#  to memory he tenido que llamar explicitamente a freeze un la linea 'use Storable' para que funcionase
my $serialized = freeze \%hash1;

# print Dumper($serialized);
# ahora $serialized contiene una copia del hash "en modo string"

# lo contrario a freeze es thaw (unmarshall)
my %hash1_thaw = %{ thaw($serialized) };
print Dumper(%hash1_thaw);

##################################
# TODO: deep (recursive) cloning #
##################################
# $cloneref = dclone($ref);
