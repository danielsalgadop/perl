#!/usr/bin/perl
# Does an md5sum hexdigest to ervery foto or video inside the path
use warnings;
use strict;
use File::Find;
use Data::Dumper;
use Digest::MD5;
 use IO::File;
&argControl();
# at this point, all @ARGV exist

my @paths;
my %md5s;
my @no_permissions;
my @file_handle_problems;

find({wanted => \&only_media,preprocess=>\&haspermissions},@ARGV);
# find({wanted => \&only_media},@ARGV);
# print Dumper
my $total_files = scalar(@paths);

&analizarMd5sum();
&output();

# Function analizarMd5sum
# Fills %md5s
# or @file_handle_problems
sub analizarMd5sum(){
    my $ctx = Digest::MD5->new;
    foreach (@paths){
        # print $_."\n";
        my $fh = IO::File->new();
        my $md5;
        eval{
            $fh->open("< ".$_);
            $ctx->addfile($fh);
            push(@{$md5s{$ctx->hexdigest}},$_);
        };
        if($@){
            push(@file_handle_problems,$_);
        }
        close $fh;
    }

}
sub output(){
	print "final output\n";
    print "total_files ".$total_files."\n";
    if(@no_permissions){
        print "no_permissions\n";
        print Dumper(@no_permissions);
    }
    if(@file_handle_problems){
        print "file_handle_problems\n";
        print Dumper(@file_handle_problems);
    }
    my $flag_repeated_files=0;
    foreach (keys %md5s){
        if (scalar @{$md5s{$_}} > 1){
            for (my $var = 1; $var < scalar @{$md5s{$_}} ; $var++) {
                my $path_sin_espacio = @{$md5s{$_}}[$var];
                $path_sin_espacio =~s/\s+/\\ /g;
                if ($var == 1){
                    print "============== ESTA es igual a las de abajo ====".$path_sin_espacio."\n";   # Keep the first copy of repeated file
                    next;
                }
                $flag_repeated_files=1;
                unlink $path_sin_espacio;
                print "removed ".$path_sin_espacio."\n"; # print path
            }
        }
        # else{
            # print "NO MEDIA FOUND\n";
        # }
    }
    unless($flag_repeated_files){
        print "NO REPEATED FILES FOUND\n";
    }
	# mostrar ficheros repes ( y los comandos propuestos para borrarlos )
	# mostrar WARNING ficheros no_permissions
	# if(@no_permissions){
    	# print "no_permissions============\n";
    	# print Dumper(@no_permissions);
    	# print "========================\n";
	# }
    # print Dumper(%md5s);
    # if(scalar %md5s == 0){
        # "no md5 calculated\n";
    # }
}

# preprocess to only have with permissions
sub haspermissions(){
    grep {
        if ( -d $_ and !-r _ ) {
            push @no_permissions, "$File::Find::dir/$_";
            0;    # don't pass on no_permissions dir
        } else {
            1;
        }
    } @_;
}

sub only_media(){
# type of files analized (case insensitive): png, jpg, .mov, .avi, 3gp, jpeg, mp4
# dudas .tif?
 	unless ($_ eq "." or $_ eq ".."){
 		# if(-r $_){
        if ($_=~/(avi|png|jpg|jpeg|mov|3gp|mp4)$/i){
            push (@paths,$File::Find::name);
        }
	}

}

sub argControl() {

    # receive path(s)
    unless (@ARGV) {
        print "need paths to search\n";
        exit;
    }
    foreach (@ARGV) {
        unless ( -d $_ ) {
            print "this path " . $_ . " does not exist\n";
            exit;
        }
    }
}
