#!/usr/bin/perl -w
# ejemplo muy sencillo


use strict;
use v5.18;
# use Test::More tests => 1;

my $end			= 0;
my $rec_level 	= 0;
sub function
{
	$rec_level++;
	say "Inicio - rec_level[$rec_level] end[$end]";
	return "SALIDA-1" if $end;
	say "Medio - rec_level[$rec_level] end[$end]";
	$end++;
	say function();   # <<<<<<<<<< Recursividad
	say "Final - rec_level[$rec_level] end[$end]"; # aqui llega al salir en primera recursion
	return "SALIDA-2";
}
say function();