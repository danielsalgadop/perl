#!/usr/bin/perl -w
# array with unknown deep. All values are numbers
# TODO: not using recursion

use strict;
use Data::Dumper;
use v5.22;


my $array=[
	1,
	[
		4,
		[
			10,
			11,
		],
		5,
		[
			8,
			9,
		],
		6,
	],
	2,
	[
		7,
		[
			12
		]
	],
	3,
];

# print Dumper $array;

foreach my $x (@$array) {

	if(ref($x) eq "ARRAY")
	{
		foreach my $y (@$x) 
		{

			if(ref($y) eq "ARRAY")
			{
				foreach my $z (@$y) 
				{
					say $z;
				}
			}
			else
			{
				say $y;
				
			}
		}
	}
	else
	{
		say $x;
	}
	# body...
}
