#!/usr/bin/perl -w
use strict;
use lib '.';



my $msg="escribiendo desde dos.pl";
    use Fcntl qw(:flock SEEK_END); # import LOCK_* and SEEK_END constants
    sub lock {
	    my ($fh) = @_;
	    flock($fh, LOCK_EX) or die "Cannot lock mailbox - $!\n";
	    # and, in case someone appended while we were waiting...
	    seek($fh, 0, SEEK_END) or die "Cannot seek - $!\n";
    }
    sub unlock {
	    my ($fh) = @_;
	    flock($fh, LOCK_UN) or die "Cannot unlock mailbox - $!\n";
    }
    open(my $mbox, ">>", "/tmp/probandoLockEnPerl")
    or die "Can't open mailbox: $!";
    lock($mbox);
    print $mbox $msg,"\n\n";
    unlock($mbox);
