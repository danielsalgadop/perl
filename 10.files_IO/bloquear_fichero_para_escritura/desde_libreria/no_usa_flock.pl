#!/usr/bin/perl
# script que debe fallar en por que exista un lock anterior (que debe ser lanzado a mano)
use warnings;
use strict;
use Data::Dumper;
use lib '.';
use write2FileWithBlocker qw(miWrite);
use variables_globales;
our $path_fichero_dnd_probar_lock;

open(FH,">>".$path_fichero_dnd_probar_lock) or die "algo falla ".$!;
print FH "desde ".$0."\n";
close FH;