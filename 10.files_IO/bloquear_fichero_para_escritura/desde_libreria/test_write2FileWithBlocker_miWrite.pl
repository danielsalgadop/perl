#!/usr/lib/perl
# Test for miWrite
use warnings;
use strict;
use Data::Dumper;
use lib '.';
use write2FileWithBlocker qw(miWrite miWriteWITHSLEEP);
# use variables_globales;
my  $path_fichero_dnd_probar_lock  = "/tmp/".$0.".txt";

# si no sabes/quieres indicar explicitamente el numero de tests
use Test::More 'no_plan';

my %r_miWrite; # place to store miWrite response
my @just_created; # place to store content of $path_fichero_dnd_probar_lock


# me aseguro de que $path_fichero_dnd_probar_lock existe
unless(-e $path_fichero_dnd_probar_lock){
	`touch $path_fichero_dnd_probar_lock`;
}
# me aseguro de que $path_fichero_dnd_probar_lock tengo permisos escritura
unless(-w $path_fichero_dnd_probar_lock){
	`chmod a+w $path_fichero_dnd_probar_lock`;
}

# TEST - Wrong open modes
%r_miWrite = miWrite($path_fichero_dnd_probar_lock,"desde ".$0."\n","errror");
ok($r_miWrite{status} eq "ERROR", "Wrong open modes");
ok($r_miWrite{message} eq "Wrong open modes", "Wrong open modes message correcto");

# TEST - se concatena bien al fichero
%r_miWrite = miWrite($path_fichero_dnd_probar_lock,"concat - desde 1".$0."\n",">>");
%r_miWrite = miWrite($path_fichero_dnd_probar_lock,"concat - desde 2".$0."\n",">>");
open (FICH,$path_fichero_dnd_probar_lock);
@just_created = <FICH>;
close (FICH);
ok( ($r_miWrite{status} eq "OK" and ($just_created[0] eq "concat - desde 1".$0."\n" and $just_created[1] eq "concat - desde 2".$0."\n") ), "concatenado CORRECTAMENTE");

# TEST - el fichero se sobreescribe
`echo "esto debe desaparecer" >> $path_fichero_dnd_probar_lock`;
%r_miWrite = miWrite($path_fichero_dnd_probar_lock,"sobre escribir ".$0."\n",">");
open (FICH,$path_fichero_dnd_probar_lock);
@just_created = <FICH>;
close (FICH);
ok( ($r_miWrite{status} eq "OK" and $just_created[0] eq "sobre escribir ".$0."\n"), "sobreescribir CORRECTAMENTE" );

# TEST - no existe fichero
%r_miWrite = miWrite("path_ficcticio_q_no_existe","desde ".$0."\n");
ok($r_miWrite{status} eq "ERROR", "path_ficcticio_q_no_existe detectado");
ok($r_miWrite{message} =~/File must exist/, "path_ficcticio_q_no_existe message correcto");

# TEST - sin permisos de escritura
`chmod a-w $path_fichero_dnd_probar_lock`;
%r_miWrite = miWrite($path_fichero_dnd_probar_lock,"desde ".$0."\n");
ok($r_miWrite{status} eq "ERROR", "sin permisos escritura");
ok($r_miWrite{message} =~/Write permissions needed/, "sin permisos escritura message correcto");
# print Dumper(%r_miWrite);



# print Dumper(%r_miWrite);


&cleanTestTrazas();
sub cleanTestTrazas(){
	`chmod 777 $path_fichero_dnd_probar_lock`;
	`rm $path_fichero_dnd_probar_lock`;
}
# cleanTestTrazas;