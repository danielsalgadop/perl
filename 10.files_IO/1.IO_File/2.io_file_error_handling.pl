#!/usr/bin/perl -w
use strict;
use IO::File;
# if the open fails, the object is destroyed

my $path_file = "/tmp/exmaple_".$0.".txt";
# to ensurance $path_file does not exist
unlink($path_file) if -e $path_file;


# Reading file
my $fh = IO::File->new();
if ($fh->open("< ".$path_file))
{
    print <$fh>;
    $fh->close;
}
else{
    print "ERROR handling1: trying to read unexisting file\n";
}
print "going on\n";
# Condensed Reading file
$fh = IO::File->new($path_file, "r");
if (defined $fh)
{
    print <$fh>;
    undef $fh; # automatically closes the file
}
else{
    print "ERROR handling2: trying to read unexisting file\n";
}
# # Writing file
# $fh = IO::File->new("> ".$path_file);
# if (defined $fh) {
#     print $fh "bar\n";
#     $fh->close;
# }

# # modes:
# # O_WRONLY (write-only) for opening the file in write-only mode
# # O_APPEND for appending to file
# $fh = IO::File->new($path_file, O_WRONLY|O_APPEND);
# if (defined $fh) {
#     print $fh "corge\n";
#     my $pos = $fh->getpos;
#     $fh->setpos($pos);
#     undef $fh; # automatically closes the file
# }
autoflush STDOUT 1;