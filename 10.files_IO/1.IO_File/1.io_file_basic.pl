#!/usr/bin/perl -w
# IO::File provides an object-oriented interface for opening files.
# IO::File inherits from IO::Handle and IO::Seekable . It extends these classes with methods that are specific to file handles.
use strict;
use IO::File;

my $fh = IO::File->new();

my $path_file = "/tmp/exmaple_".$0.".txt";

# Reading file
if ($fh->open("< ".$path_file)) {
    print <$fh>;
    $fh->close;
}
# Writing file
$fh = IO::File->new("> ".$path_file);
if (defined $fh) {
    print $fh "bar\n";
    $fh->close;
}
# Condensed Reading file
$fh = IO::File->new($path_file, "r");
if (defined $fh) {
    print <$fh>;
    undef $fh; # automatically closes the file
}

# modes:
# O_WRONLY (write-only) for opening the file in write-only mode
# O_APPEND for appending to file
$fh = IO::File->new($path_file, O_WRONLY|O_APPEND);
if (defined $fh) {
    print $fh "corge\n";
    my $pos = $fh->getpos;
    $fh->setpos($pos);
    undef $fh; # automatically closes the file
}
autoflush STDOUT 1;