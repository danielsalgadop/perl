#!/usr/bin/perl
use warnings;
use strict;
use Data::Dumper;

# Array constants
use constant ARRAYCONSTATNT => [ "10.0.170.46",2 ];

# way to walk-through an array constant
foreach (@{+ARRAYCONSTATNT})
{
	print $_."\n";
}