#!/usr/bin/perl
# Este script asume que los tipos de datos ya estan validados (no asume eso por que acepta dsd HERMES)
use strict;
use warnings;
use Data::Dumper;
my %valores_no_raros_de_keys;

# valores standard
# 
$valores_no_raros_de_keys{'uno'} = "valor k3ml3km2";
$valores_no_raros_de_keys{'con comillas'} = "valor k3ml3km2";
$valores_no_raros_de_keys{'1/1::23/()'} = "SII QUE VALE vale o no vale Necesitado al usarlo para saps como keys  012ue01e2";


###############################
# valores mas raros para keys #
###############################
my %valores_raros_de_keys;



# en este link NO recomiendan para nada usar estos keys sin comillas.
# http://stackoverflow.com/questions/401556/are-quotes-around-hash-keys-a-good-practice-in-perl
# 
# When specifying constant string hash keys, you should always use (single) quotes. E.g., $hash{'key'}
#  This is the best choice because it obviates the need to think about this issue and results in consistent
#  formatting
$valores_raros_de_keys{sin_comillas_sin_espacios} = "valor 34mn32de";




# calcular el numero de keys


my %calcular_el_numero_de_keys = (
			uno => 'ed32',
			dos => 'ed32',
	);
	%calcular_el_numero_de_keys = ();   # numero = 0
my $numero_de_keys = keys(%calcular_el_numero_de_keys);
print "numero_de_keys=[".$numero_de_keys."]\n";




print Dumper(%valores_raros_de_keys);
print Dumper(%valores_no_raros_de_keys);
