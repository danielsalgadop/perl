#!/usr/bin/perl
use warnings;
use strict;
# diference between concatenating empty value or undef value

my %hash=(
	empty=>'',   #  value '' will not exist, but will not raise warning
);

##########################  Option 1 explicitly calling required key
if($hash{emtpy})
{
	print "[empty] EXISTE\n";
}
else
{
	print "NO EXISTE [empty]\n";
}
print "concatenated value [".$hash{empty}."]\n";


########################## Option 2 dinamically calling required key
my @sap_keys = 
	qw(
		no_existe 
		empty
	);
# key 'no_existe' will raises 'Use of uninitialized value 
foreach (@sap_keys)
{
	if($hash{$_})
	{
		print "[".$_."] EXISTE\n";
	}
	else
	{
		print "NO EXISTE [".$_."]\n";
	}
	print "concatenated value [".$hash{$_}."]\n";
}