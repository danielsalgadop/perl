#! /usr/bin/perl
use strict;
use warnings;
use Data::Dumper;

#splice (ARRAY, OFFSET, LENGHT,LIST)
my @aa          = qw(CX_asdf CS_vvrer CS_borrar CS_lashfs CS_osdifohf);
my $para_borrar = "CS_borrar";
print Dumper(@aa);
my $i;
foreach (@aa) {

    #print $_;
    if ( $_ eq $para_borrar ) {
        splice( @aa, $i, 1 );

        #       print "$_ <=> $para_borrar";
        #       print "SOLO INA VEZ. no?\n";
    }
    $i++;
}
print "esto es matriz aa\n";
print Dumper(@aa);


print "======================== RECORRER ARRAY\n";
print "method, foreach\n";
foreach (@aa)
{
    print $_,"\n";
}

print "method, for(1)\n";
for(@aa)
{
    print $_,"\n";
}

print "method, for(2)\n";
for my $i(0 .. $#aa)
{
    print $aa[$i],"\n";
}

print "method, for(3) - classic\n";
for (my $i=0;$i<scalar(@aa);$i++)
{
    print $aa[$i],"\n";
}


my @aa2 = @aa;
print "method, while(1) this empties array\n";
while (my $e = shift(@aa2))
{
    print $e,"\n";
}

@aa2 = @aa;
print "method, while(2) this empties array\n";
while(scalar(@aa2) != 0)
{
    my $e = shift(@aa2);
    print $e."\n";
}

print "method, map\n";
map {print $_."\n"} @aa;

# añadir un array a otro y asignarlo todo al array que ha sido empujado
my @ab = qw(estoesab1 ab2 ab3 ab4 ab5 ab6);
push( @aa, @ab );
@ab = @aa;
print "esto es concateniacion de matriz aa y ab\n";
print Dumper(@ab);
my @ac;
push( @aa, @ac );
print "esto es concateniacion de matriz aa y ab y ac (vacia)\n";
print Dumper(@aa);

######################################### comprobar como se imprime un array
my @imp = qw (esto con espacios y sin ellos);

#sin ellos. Si pones print @imp."\n"; imprime como si fuera numérico
print @imp;
print "\n";

#con ellos
print "val=@imp\n";

# modificando la variable $"
$" = ' <-espacio molon-> ';
print "val=@imp\n";
$" = ' ';

######################################### comparacion de arrays

# comparando arrays con smart match
#http://perldoc.perl.org/perlop.html#Smartmatch-Operator
# creo (no estoy seguro) que solo vale para versiones de perl superiores a 5.10
use strict;
print "\n======== comparando arrays ====================\n";
print "\ncomparando con ~~\n";
my @a = qw (1 2 3);
my @b = qw (1 2 3);
if ( @a ~~ @b ) {
    print "son iguales";
}
else {
    print "no lo son";
}

print "\ncomparando con eq esto OJO esto NO FUNCIONA!!!!!\n";   # <<<< esto NO FUNCIONA!!!!!
if ( @a eq @b ) {
    print "Son iguales";
}
else {
    print "NO lo son";
}

print "\ncomparando con sub arrayCompare\n";
print "result".arrayCompare(\@a,\@b)."\n";
if(arrayCompare(\@a,\@b)){
    print "Son iguales\n";
}
else{
    print "NO lo son";
}
# Function: arrayCompare
# Done this sub because Smartmatch (comparing with ~~) is experimental
# returns 1 if are equal
# returns 0 if there is some diference
sub arrayCompare($$){
    my $ref_array1 = shift;
    my $ref_array2 = shift;
    my @array1 = @{$ref_array1};
    my @array2 = @{$ref_array2};
    # print "scalar 1 [".scalar(@array1)."] scalar 2[".scalar(@array2)."]\n";
    # compare sizes
    return 0 if scalar(@array1) != scalar(@array2);
    my $cont = 0;
    foreach (@array1){
        return 0 if $array2[$cont] ne $_;
        $cont++;
    }
    return 1;
}





#print "a y b son iguales" (@a eq @eq)?"si lo son":"no lo son";

print "\n====== existe elemento dentro de array ======================\n";

# ¿existe un elementeo dentro de un array
my $elemento = "toe";
my @array    = qw(klsadjlas jlkas flaksjfa sl toe);
if ( $elemento ~~ @array ) {

    #if ("toe" ~~ @array){
    print "el elemento=[" . $elemento . "] esta dentro de array\n";
}
else {
    print "el elemento=[" . $elemento . "] NOOOOOO esta dentro de array\n";
}


print "\n======== existe INDICE dentro de array =====================\n";
    print "Existe el indice 44? =>";
if ($array[44]){
    print "SI";
}
else{
    print "NO";
}
print "\n";




print
    "\n====== TAMAÑO ARRAY diferencias entre scalar(array), \$\#array y array en string context ======================\n";
print "OJO existen diferencias entre contar scalar y contar con \$#\n";
print "1) scalar es mas humano array vacio es 0 es length 0\n";
print "2) con \$# es -1\n";
print "3) array en string en context se comporta como scalar\n";

my @array_vacio       = ();
my @array_1_elemento  = ("unSoloValor");
my @array_2_elementos = ( "primerValor", "segundoUltimoValor" );

print "\@array_vacio Scalar=[" . scalar(@array_vacio) . "]\n";
print "\@array_1_elemento Scalar=[" . scalar(@array_1_elemento) . "]\n";
print "\@array_2_elementos Scalar=[" . scalar(@array_2_elementos) . "]\n";

print "\@array_vacio \$#=[" . $#array_vacio . "]\n";
print "\@array_1_elemento \$#=[" . $#array_1_elemento . "]\n";
print "\@array_2_elementos \$#=[" . $#array_2_elementos . "]\n";

print "\@array_vacio \$#=[" . @array_vacio . "]\n";
print "\@array_1_elemento \$#=[" . @array_1_elemento . "]\n";
print "\@array_2_elementos \$#=[" . @array_2_elementos . "]\n";




use Test::More;
print "======== uniq array dosent work, only elimiates 1 duplicated==================\n";
# given array return one with uniq array_2_elementosmy %seen = ();
my @sample_with_repeats = qw (123qwe 123qwe qweasd 456rty fghvbn 123qwe 123qwe);
print "ORIGINAL (@sample_with_repeats)\n";
my %seen = ();
my @only_repeat = map { 1==$seen{$_}++ ? () : $_ } @sample_with_repeats;
my @uniq_elements = map { 1==$seen{$_}++ ? $_ : () } @sample_with_repeats;
is_deeply(\@uniq_elements,[qw(qweasd 456rty fghvbn)],"elements NOT_REPEATED");
print "not NOT_REPEATED (@uniq_elements)\n";
print "only_repeat\n".Dumper(@only_repeat);
is_deeply(\@only_repeat,[qw(123qwe)]);

# print "=== using List::MoreUtils";
# use List::MoreUtils;
# @uniq_elements = uniq(@sample_with_repeats);
# print "uniq_elements\n".Dumper(@uniq_elements);

@uniq_elements = array_unique(@sample_with_repeats);
print "My way to get uniq elements( i think these works )\n".Dumper(@uniq_elements);
is_deeply(\@uniq_elements,[qw(123qwe qweasd 456rty fghvbn)],"My way to get uniq elements");
sub array_unique(@){   # php nomenclature
    my %seen;
    my @cleaned;
    foreach(@_){
        push (@cleaned,$_) unless($seen{$_});
        $seen{$_}++;
    }
    return(@cleaned);
}
# TODO cambiar nombre, pesado por q esta mas de 1 vez
sub array_elementos_pesados
{
    my %seen;
    my @return;
    foreach (@_)
    {
        $seen{$_}++;
        push (@return,$_) if $seen{$_} == 2;
    }
    return @return;
}
my @elementos_pesados = array_elementos_pesados(@sample_with_repeats);
print Dumper @elementos_pesados;
is_deeply(\@elementos_pesados,[qw(123qwe)],"elementos_pesados");
my @aux = @sample_with_repeats;
push(@aux,"otroNuevo","otroNuevo");
@elementos_pesados = array_elementos_pesados(@aux);
is_deeply(\@elementos_pesados,[qw(123qwe otroNuevo)],"elementos_pesados con otroNuevo");




print "============= given array1(sample) and array2(eraser), return array3 with elements in array1 NOT in array1\n";
my @sample = (1..10,"toe",22,"toe1");
my @eraser= qw (1 3 5 22 toe toe2 7);

# Function: filter_array_with_array
# Receives:
#    1- (ref_to_array) Used as sample
#    2- (ref_to_array) Used as filter
# Returns:
#   1- (array) Contaning elemnts in sample NOT seen in filter
sub filter_array_with_array($$){
    my ($ref_sample,$ref_eraser) = @_;
    my @return;
    foreach my $un_sample(@$ref_sample){
        unless (grep { $_ eq $un_sample } @$ref_eraser){
            push @return, $un_sample;
        }
    }
    return @return;
}

print "sample antes   @sample\n";
@sample =filter_array_with_array(\@sample,\@eraser);
is_deeply(\@sample,[qw(2 4 6 8 9 10 toe1)],"sample ok");
print "sample despues @sample\n";

print "sample antes   @sample - Using a single vale\n";
@sample =filter_array_with_array(\@sample,["toe1"]);
is_deeply(\@sample,[qw(2 4 6 8 9 10)],"sample ok");
print "sample despues @sample\n";
done_testing();
