#!/usr/bin/perl -w
use strict;
# http://search.cpan.org/~kasei/Class-Accessor-0.34/lib/Class/Accessor.pm
package Foo;
use base qw(Class::Accessor);
Foo->follow_best_practice;
Foo->mk_accessors(qw(name role salary));
use Data::Dumper;

# or if you prefer a Moose-like interface...

# package Foo;
# use Class::Accessor "antlers";
# has name => ( is => "rw", isa => "Str" );
# has role => ( is => "rw", isa => "Str" );
# has salary => ( is => "rw", isa => "Num" );

# Meanwhile, in a nearby piece of code!
# Class::Accessor provides new(). Dont have to write it!
my $mp = Foo->new({ name => "Marty", role => "JAPH" });
# print Dumper($mp);

# CANT MAKE this two (getter and setter) WORK!! #<<<<<<<<<<<<<<<
# my $job = $mp->role;  # gets $mp->{role}
# $mp->salary(400000);  # sets $mp->{salary} = 400000 # I wish

# # like my @info = @{$mp}{qw(name role)}
# print Dumper(@{$mp}{qw(name role)});
my @info = $mp->get(qw(name role));  # this getter work
print Dumper(@info);

# # $mp->{salary} = 400000
$mp->set('salary', 400000);  # this setter works
print Dumper($mp);