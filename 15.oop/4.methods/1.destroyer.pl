#!/usr/bin/perl -w
# simple and useless class and object
use strict;
package Object;

# Constructor
# Receives needed params
# Returns blessed object
sub new
{
	# receives the name of the object that it will create
	my $class = shift; # this is mandatory
	my $self = {}; # reference to a hash

	bless $self, $class;
	return $self;
}
sub DESTROY
{
	my $self = shift;
	"Destroy method called.\n";
}
package main;
my $obj = Object->new;
print "First object created ".ref($obj)."\n";
undef $obj;  # TODO not working
print "after destroying";