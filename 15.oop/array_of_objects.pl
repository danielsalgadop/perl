#!/usr/bin/perl -w
# fill an array wiht objects


use strict;
use Data::Dumper;
use v5.18;

sub generadorObj($)
{
	return bless {name=>shift}, 'Class';;
}

# create an array of objetct
my @array_of_objects;
for (1..5){
	my $obj = generadorObj($_);
	push @array_of_objects,$obj;
}

print Dumper @array_of_objects;

