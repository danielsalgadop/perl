#!/usr/bin/perl;
# esto lo he construido en DIGI mockeando la respuesta con metodo ->fault de SOAP::Lite
# quiereo meter un atributo en constructor y leerlo desde otro método (no el constructor)

use strict;
use warnings FATAL => 'all';
use Test::Simple;

use Data::Dumper;
my $methodName = 'estoEstabaDefinidoEnMain';

package ResponseOnFault;
{
    sub new {
        my $class= shift;
        my $self = {
            # soap => shift,
            res => shift,
        };
        bless $self, $class;
        return $self;
    }
    sub thisMethodNeedsToAccessObjectAttibutes {
        my $self = shift;        # la "clave" estaba en ESTA línea
        # my $self = $_;
        use Data::Dumper;
        print Dumper($self);
        my $faultStringMessage = 'ERROR: ';
        my $res = $self->{'res'};
        # my $res = 'esta puta mierda quiero pillarla del constructor';
        $faultStringMessage .= 'methodName ['.$methodName.'] '.$res;
        # $faultStringMessage .= $res ? $res->faultstring : $soap->transport->status
        return $faultStringMessage;
    }
}
# print Dumper($res);
# my $soap = qw(fake  'soap_response');
my $responseOnFault = new ResponseOnFault('HC -res');
use Data::Dumper;
print Dumper($responseOnFault->thisMethodNeedsToAccessObjectAttibutes);
