#!/usr/bin/perl -w
use strict;
use warnings;
use Data::Dumper;

##########################################
print "##############Using class atributes\n";
##########################################
package Class;
use strict;
use warnings;

my $class_attribute = 0; # had to do a getterClassAttribute because this is lexically scoped as 'my'
our $public_class_attribute = 10;
sub new
{
	my $class 	= shift;
	$class_attribute++; # <<<< changin $class_attribute
	$public_class_attribute++; # <<<< changin $class_attribute
	return bless {}, $class;
}
# class method
sub getterClassAttribute
{
	# my $class = ref($_[0]) || $_[0]; # this line is in http://gwolf.org/files/poo_perl.pdf
	# I dont understand it
	return $class_attribute;
}

package main;
my $obj = Class->new();
Class->new();
Class->new();
print "Using a class_attribute (via getter) to count number of classes created [".$obj->getterClassAttribute()."]\n";
print "Using Class method (withour object) [".&Class::getterClassAttribute()."]\n";
print "Public class attribute [".$Class::public_class_attribute."]\n";