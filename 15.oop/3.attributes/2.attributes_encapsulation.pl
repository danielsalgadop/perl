#!/usr/bin/perl -w
use strict;
use warnings;
use Data::Dumper;

##########################################
print "##############Setting and printing VARIABLE in an Object attribute\n";
##########################################
package ClassWithVARIABLEAttribute;
use strict;
use warnings;
sub new
{
	my $class 	= shift; # $_[0] contains the class name
	my $name 	= shift; # $_[1] contains the value of our number
            # it is given by the user as an argument

    # instance (object) attributes
	my $self = {};
	bless $self, $class;
	$self->nameSetter($name);
	return $self;
}
sub nameSetter($)
{
	my $self 			= shift;
	$self->{name} 	= shift;
}
sub nameGetter($)
{
	my $self 		= shift;
	return $self->{name};
}

package main;
my $obj = ClassWithVARIABLEAttribute->new("Paul");
print $obj->nameGetter."\n";

# print $obj->{name}."\n"; # accesing atribute. But violating object data encaupsulation

# ##########################################
# print "##############Setting and printing ARRAY in an Object attribute\n";
# ##########################################
# package ClassWithARRAYAttribute;
# use strict;
# use warnings;
# sub new
# {
# 	my $class 		= shift;
# 	my @coordinates = @_;

# 	my $self = {coordinates=>\@coordinates};
#            # the data in our class is a hash reference. These could be done AFTER blessing doing # $self->{coordinates} = \@coordinates;

# 	bless $self, $class;
# 	return $self;
# }

# package main;
# my @coordinates = ("1","2","3");
# my $obj2 = ClassWithARRAYAttribute->new(@coordinates);
# print Dumper (@{$obj2->{coordinates}});
# foreach (@{$obj2->{coordinates}})
# {
# 	print $_."\n";
# }

# ##########################################
# print "##############Setting and printing HASH in an Object attribute\n";
# ##########################################
# package ClassWithHASHAttribute;
# use strict;
# use warnings;
# sub new
# {
# 	my $class 				= shift;
# 	my $href_person_data 	= shift;
# 	my %person_data 		= %{$href_person_data};
# 	my $self = {person_data=>\%person_data};

# 	bless $self, $class;
# 	return $self;
# }

# package main;
# my %person_data = (name=>"Paul",second_name=>"Mac Flurry",telephone=>"123456789");
# my $obj3 = ClassWithHASHAttribute->new(\%person_data);
# print Dumper (%{$obj3->{person_data}});
# foreach (keys %{$obj3->{person_data}})
# {
# 	print $_."\t - ".$obj3->{person_data}{$_}."\n";
# }

# ##########################################
# print "##############Multi-type attributes\n";
# ##########################################
# package MultiTypeAttributes;
# use strict;
# use warnings;
# sub new
# {
# 	# my $class 				= shift;
# 	# my $variable 				= shift;
# 	# my $aref_array 			= shift;
# 	# my $href_hash 			= shift;

# 	my ($class,$variable,$aref_array,$href_hash) = @_;
# 	my $self = {variable=>$variable,array=>$aref_array,hash=>$href_hash};

# 	bless $self, $class;
# 	return $self;
# }

# package main;
# my $obj4 = MultiTypeAttributes->new("variable_value",["array1","array2"],{key1=>"hash1",key2=>"value2"});
# print $obj4->{variable}."\n";
# print Dumper(@{$obj4->{array}});
# print Dumper(%{$obj4->{hash}});