#!/usr/bin/perl
# Using Methods
# For more cleaner code NO data encapsulation
use strict;
use warnings;
use Data::Dumper;
package LivingBeing;
use strict;
use warnings;
sub new {
	my $class 		= shift;
	my $specie 		= shift;
	# instance (object) attributes
	my $self = {
		specie=>$specie
	};
	
	bless $self, $class;
	return $self;
}
sub born {
	my $self 		   		= shift;
	$self->{birthday} 		= shift;
	$self->{position}      = \@_;   # creating position attribute in birth
}

package Plant;
@Plant::ISA = qw(LivingBeing); # could be declared as our @ISA = qw(LivingBeing);

package Animal;
@Animal::ISA = qw(LivingBeing);
sub move {
	my $self  				   		= shift;
	my $aref_new_position   		= shift;
	$self->{position} = $aref_new_position;
}

package main;
###########
# HORSE
###########
my $horse_obj = Animal->new("horse");
$horse_obj->born("11-11-2011",(1,23,45));  # asigning birthday and birthplace
$horse_obj->move([2,3,4]);			# moving around
print Dumper($horse_obj);

###########
# ROSE
###########
my $plant_obj = Plant->new("Rose");
$plant_obj->born("12-12-2014",(1,2,3));
# $plant_obj->move([2,3,4]); # No method defined! (Plants DONT move) 'Can't locate object method "move" via package "Plant"' 
print Dumper($plant_obj);