#!/usr/bin/perl -w
# had to install libmoose-perl
use strict;
package Point;
use Moose; # automatically turns on strict and warnings

has 'x' => (is => 'rw', isa => 'Int');
has 'y' => (is => 'rw', isa => 'Int');

sub clear {
    my $self = shift;
    $self->x(0);
    $self->y(0);
}

package Point3D;
use Moose;

extends 'Point';

has 'z' => (is => 'rw', isa => 'Int');

after 'clear' => sub {
    my $self = shift;
    $self->z(0);
};

package main;
use Data::Dumper;
my $point3d = new Point3D({x=>1,y=>2,z=>3});
print Dumper $point3d;