#!/usr/bin/perl -w
# Blessing things

use strict;
use warnings;

my $var = {key=>"value"};  # ref to hash
print 'Before it was a '.ref($var)."\n";  # HASH
# bless - This function tells the thingy referenced by REF that it is now an object in the CLASSNAME package
# bless REF,CLASSNAME
bless $var, 'Classname';
print 'Has become '.ref($var)."\n"; # ClassName



# it's possible to bless any type of data structure or referent, including scalars, globs, and subroutines