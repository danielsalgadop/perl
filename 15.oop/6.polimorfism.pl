#!/usr/bin/perl
# Polimofism in constructor (born).
# Animal have gender
# Plants dont have gender (this is not bilogical true, but due to aply polymorfism)
# Polimorfism in matterInput, it is defined in LivingBeing in Animal and Plant
# good food will cause Animal to increase 50% of matterInput (digestion taking place)
# good food will cause Plant to increase 100% of matterInput
# bad food will cause an Animal to decrease mass
# bad food will NOT cause Plant to decrease mass
# For more cleaner code NO data encapsulation
use strict;
use warnings;
use Data::Dumper;
package LivingBeing;

use strict;
use warnings;
sub new {
	my $class 		= shift;
	my $specie 		= shift;
	
	# instance (object) attributes
	my $self = {
		specie=>$specie
	};
	
	bless $self, $class;
	return $self;
}
sub born {
	my $self 		   		= shift;
	$self->{birthday} 		= shift;
	$self->{mass}			= shift;
	$self->{position}      = \@_;
	my $message = "a new LivingBeing is born [".$self->{specie}."] yor birthday is [".$self->{birthday}."] your mass is [".$self->{mass}."g] and your position is [".${$self->{position}}[0].",".${$self->{position}}[1].",".${$self->{position}}[2]."]";
	$message .= " gender [".$self->{gender}."]" if $self->{gender};
	print $message."\n";
}

# if LivingBeing likes it return 1, else return 0
sub tasting
{
	my $self 			= shift;
	my $type_of_food 	= shift;
	if ($self->isa("Animal"))
	{
		return 1 if $type_of_food =~/herb/;
	}
	if ($self->isa("Plant"))
	{
		return 1 if $type_of_food =~/water/;
	}
	return 0;
}


sub matterInput {
	my $self 		   		= shift;
	my $quantity 			= shift;	# grams
	$self->{mass} += $quantity;
}
sub matterOutput {
	my $self 		   		= shift;
	my $quantity 			= shift;	# grams
	$self->{mass} -= $quantity;
}
###############################################################
package Plant;
@Plant::ISA = qw(LivingBeing); # could be declared as our @ISA = qw(LivingBeing);
sub matterInput {
	my $self 		   		= shift;
	my $type_of_water 		= shift;
	my $quantity 			= shift;	# grams
	if($self->tasting($type_of_water))
	{
		# this could be done Explicitly calling parent method $self->LivingBeing::matterInput
		$self->SUPER::matterInput($quantity);  # calling PARENT (SUPER) method
	}
	else
	{
		print "bands of Capary avoiding matterInput\n";
	}
}
###############################################################
package Animal;
@Animal::ISA = qw(LivingBeing);
sub born
{
	my $self 		   		= shift;
	my $birthday 			= shift;
	my $mass				= shift;
	$self->{gender}			= shift;   # this is the diference with Plant
	# my $position      = ;
	$self->SUPER::born($birthday,$mass,@_);
}
sub move {
	my $self  				   		= shift;
	my $aref_new_position   		= shift;
	print $self->{specie}." moving!\n";
	$self->{position} = $aref_new_position;
	$self->{mass}--;
}
# Some grams are not going to be swallowed
sub digestion 
{
	my $self = shift;
	my $quantity = shift;
	$quantity = $quantity/2;
	return $quantity;
}

# really really it could be called ->eat
sub matterInput {
	my $self 		   		= shift;
	my $type_of_herb 		= shift;
	my $quantity 			= shift;	# grams
	if($self->tasting($type_of_herb))
	{
		$quantity  = $self->digestion($quantity); # << digestion taking place
		# this could be done Explicitly calling parent method $self->LivingBeing::matterInput
		$self->SUPER::matterInput($quantity);    # calling PARENT (SUPER) method
	}
	else
	{
		print $self->{specie}." puking!\n";
		$self->matterOutput(50); # loosing 50 gr due to puking
	}
}
package main;
###########
# HORSE
###########
print "############### HORSE\n";
my $horse_obj = Animal->new("horse");
$horse_obj->born("11-11-2011","5000","female",(1,2,3));  # asigning birthday, birthplace and mass
$horse_obj->move([2,3,4]);			# moving around
print "Moving makes him thinner [".$horse_obj->{mass}."]\n";
print "Time to eat GOOD food\n";
$horse_obj->matterInput("mountaing_herbs","500");
print "Increased mass (fatter) [".$horse_obj->{mass}."]\n";
print "Time to eat BAD food (will make ".$horse_obj->{specie}." puke)\n";
$horse_obj->matterInput("BAD_FOOD","500");
print "Decreased mass because of puking [".$horse_obj->{mass}."]\n";

###########
# ROSE
###########
print "############### ROSE\n";
my $plant_obj = Plant->new("Rose");
$plant_obj->born("12-12-2014","100",(4,5,6));
print "Start raining so this ".$plant_obj->{specie}." drinks\n";
$plant_obj->matterInput("rain_water","10");
print "Increased mass [".$plant_obj->{mass}."]\n";
print "Afterward some detritus came, hopefully ".$plant_obj->{specie}." will not drink it nor decrease mass\n";
$plant_obj->matterInput("detritus","10");
print "Same mass [".$plant_obj->{mass}."]\n";

print "\n\nNOTICE! matterInput reacts diferently (Polimorfism) between Animals and Plants\n";