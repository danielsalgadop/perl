#!/usr/bin/perl
use warnings FATAL=>'all';
use strict;
use Net::Ping;
use Data::Dumper;

# Function: pingEquipos
# para realizar un ping (TCP) a un equipo
# Receives:
#   (string) - can be hostname or ip
# Returns:
#   (number) 1 - if ping ok
#   (number) 0 - if NO ping
sub pingEquipo($){
    my $ping=Net::Ping->new();
    return 1 if $ping->ping(shift);
    return 0;
}
print Dumper pingEquipo("BTCPEOV3");


# --- Función para realizar un ping a un equipo. --- #
# original de Jose
sub PingEquiposOriginalJose(){
    # El protocolo por defecto es TCP. Si se quiere ICMP, hay que ser root para ejecutar el script.
    # Con el protocolo TCP el ping intenta establecer una conexión con el puerto echo del equipo remoto.
    # Si la conexión se establece correctamente, el equipo remoto se considera alcanzable.
 
    # Variables que recibe.
    my ($rdestino,$rprotocolo,$rtimeout,$rtamanopaquete)=@_;
    my $ping=Net::Ping->new(${$rprotocolo},${$rtimeout},${$rtamanopaquete});
    # Se ejecuta el ping.
    my $resultping=$ping->ping("${$rdestino}");
    my %Hresultado=();
    $Hresultado{'ping'}{${$rdestino}}{'texto'}="Ping ${$rdestino}";
    $Hresultado{'ping'}{${$rdestino}}{'codigo'}=($resultping eq '1') ? "OK" :  "ERROR";

    return(\%Hresultado);
}