#!/usr/bin/perl
use warnings;
use strict;
use Net::Telnet;



my $telnet = new Net::Telnet(
    Timeout   => 20,
    Input_Log => "/tmp/path_log_net_telnet_pruebas_perl",
    Errmode   => "return",
);

my $host="TOE";
$telnet->open($host);


my $errores_encontrados = $telnet->errmsg;
if ($errores_encontrados) {    # hay algun error
	print "ERROR ".$errores_encontrados."\n";
}