#!/usr/bin/perl -w
use strict;
my $CONSTANTE = 2;
# sacado de man Test::Tutorial
# Test::More es Test::Simple ampliado, lo q funciona en simple funciona en mora
# pagina que tb tiene explicaciones interesantes http://www.shlomifish.org/lecture/Perl/Newbies/lecture5/testing/demo/test-more.html

# PARA LANZAR TEST DESDE LA TERMINA, y que te haga un pequeño resumen (en color)
#  comando "prove script.pl"   <<<<<<<<<<<<<<<<<<<<<<<<<
#  comando "prove -v script.pl"  par Verbose and see what script is outputing
# 
# use Test::More tests => 2;    # si sabes cuantos tests vas a lanzar, OJO, aun asi lanza todos
# si no sabes/quieres indicar explicitamente el numero de tests
use Test::More 'no_plan';

# diag("algo") es como a poner COMENTARIOS en los tests
diag("ZONA DE PRUEBAS PARA Test::More");

# diag sale cuando el error ha ido mal, si solo quieres que salga en ejecuccion normal, hay que usar note
note("'note' es como un diag 'optimista' solo sale cuando todo va bien");


# se puede usar ponerlo detras de un test, xej:
# como ok() or diag("poner aqui lo que sospechas");

#ok solo comprar true/false
ok( 1 );
ok( 1 + 1 == 2 );
ok( 1 + 1 == 2222 );
ok( 2 + 2 == 4, "nombre (descripcion) del test" );    # segundo argumento opcional
ok( 5 == &devuelvo5(), "probando una funcion simple" );
ok(1 and 1 and 1, "multiple comparations in one ok");

# is()  Compara lo que devuelve la funcion a testear con lo que le has pasado
# primer parametro es el codigo a probar, segundo la respuesta, 2l tercero es el nombre del test
is( 1 + 1,       2, "uno mas uno == 2" );
is( &devuelvo5(), 5, "devuelvo5 devuelve 5" );
is( &devuelvoConstante($CONSTANTE) + 1,    # fuerzo sumarle 1
    &devuelvoConstanteMas1($CONSTANTE), "devuelvoConstante y devuelvoConstanteMas1 IGUALES" );

# isint  "are these two things not equal?", lo contrario a is
isnt(0,1,"0 no es 1");
isnt( &devuelvoConstante($CONSTANTE),
    &devuelvoConstanteMas1($CONSTANTE), "devuelvoConstante y devuelvoConstanteMas1 NO IGUALES" );

# like (regular Expresions)
like("un string que empieza y acaba por u",qr/^u.*u$/,"un string que empieza y acaba por u");

# Para comparar DATA STRUCTURES
my %hash = (
	uno=>"contra",
	dos=>"contra",
);
my %hash2 = (
	uno=>"contra",
	dos=>"contra",
);
is_deeply(\%hash,\%hash2,"Comparando hashes");
is_deeply(\%hash,{uno=>"contra",dos=>"contra"},"Comparando hashes, with anonymous hash");

my @array = qw(sldkjf sadflklkejwflkjeflj klwejf lwkjljgl welkg);
my @array2 = qw(sldkjf sadflklkejwflkjeflj klwejf lwkjljgl welkg);
is_deeply(\@array,\@array2,"Comparando arrays");

is_deeply(\@array,["sldkjf","sadflklkejwflkjeflj","klwejf","lwkjljgl","welkg"],"Comparando arrays, with anonymous array");


# plan()

# SKIP If you dont want to do certain tests you have to include them in 
# SKIP: {
#         skip $why, $how_many if $condition;
# }
# Example
SKIP: {
        skip "Estas pruebas no se haran";
        ok( 1 + 1 == 2, "nunca se lanzara");
}





################ FUNCIONES

# funcion muy simple
sub devuelvo5() {
    return 5;
}


sub devuelvoConstante($) {
    my $CONSTANTE = shift;
    return $CONSTANTE;
}

sub devuelvoConstanteMas1($) {
    my $CONSTANTE = shift;
    return $CONSTANTE + 1;
}

