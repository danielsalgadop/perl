#!/usr/bin/perl -w
use strict;
use diagnostics;


print "While sprintf RETURNS the value formated. printf prints it\n";

####  Removing decimal part of a number
my $div = 7111.42243696653; # using , instead of . will raise WARNING
print "ORIGINAL[".$div."]\n";
my $result = sprintf("%d",$div);
print "printf [".$result."]\n";
printf("%d",$div);