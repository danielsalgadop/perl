#!/usr/bin/perl
# estudiando una alterantiva a usar JSON en alarmas, a ver que pasa si imprimes un hash
use warnings;
use strict;
use Data::Dumper;
my %hash;
$hash{"uno"} = "valor1";
$hash{"dos"} = "valor2";
$hash{"tres"} = "valor3";
$hash{"cuatro"} = "valor4";
print Dumper(%hash);
open(FILE,"> output.txt");

print FILE %hash;
close(FILE);

