#!/usr/bin/perl -w
# place to play with require
use strict;
use Data::Dumper;

# you have to import names
our ($var1,@array,%hash);
require 'required.cfg';

print $var1;
print Dumper @array;
print Dumper %hash;
