#!/usr/bin/perl
use Data::Dumper;
# --------  Comprobacion de que el cliente existe --------------------------------------
my @Clientes = qw(CHI SDF GHR AAA IEI LFE LWC); 
####################################  grep hecho por mi
print "no existe \$ARGV[0]\n" if !$ARGV[0];
foreach (@Clientes) {
	if($_  ne $ARGV[0])	{
		#No se ha encontrado $ARG[0]
		#print "esto $_ no es igual a esto $ARGV[0]\n";
		next;	
	}
	else {
		print "encuentro esto $ARGV[0]\n";
		last;	
	}
}
###################################   Uso de grep como valor logico
#realmente grep devuelve un array con todas las coincidencias
#my @coincidencidas = grep /^$ARGV[0]$/, @Clientes; print Dumper(@coincidencias);
# pero es elegante hacerlo de esta forma
(grep /^$ARGV[0]$/, @Clientes) ? print "encontrado $ARGV[0]\n" : print "no encontrado\n";
