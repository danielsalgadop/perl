#!/usr/bin/perl
# Original copied from http://geek.jasonhancock.com/2009/08/07/perl-check-data-type/
#UNIVERSAL::isa expects a reference and "a guess"
print UNIVERSAL::isa(\$scalar,'SCALAR');

# Some changes to make it does work
use strict;
use warnings;

my $scalar;
my @array;
my %hash;

checktype(\$scalar);
checktype(\@array);
checktype(\%hash);

sub checktype
{
    my ($obj) = @_;

    if(UNIVERSAL::isa($obj, 'SCALAR'))
    {
        print "it's a scalar\n";
    }
    elsif(UNIVERSAL::isa($obj, 'HASH'))
    {
        print "it's a hash\n";
    }
    elsif(UNIVERSAL::isa($obj, 'ARRAY'))
    {
        print "it's an array\n";
    }
    else
    {
        print "unknown!!!n";
    }
}