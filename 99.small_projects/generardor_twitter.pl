#!/usr/bin/perl -w 
use diagnostics;
use Data::Dumper;
my $tamanio_mens = 140;
my $espacio="_";
my @bolsa=qw(a b c d e f g h i j k l m n o p q r s t u v w x y z);
push (@bolsa, $espacio);
my $num_letras = @bolsa;
my @vocales= qw(a e i o u );
my %vocales = (a=>"a",e=>"e", i=>"i", o=>"o", u=>"u");
my $contador_letras=0;
my $contador_consonantes=0;
my $anterior_letra="0";
my $mensaje;
my $parar=0;
while ($parar==0){
	my $mensaje = &generarMensaje;
	# evitar que acaben en una letra
	my $patron = "t|q|h|f";
	unless ($mensaje=~m/($patron)($espacio)/ && $mensaje=~/$patron$/){
		$parar=1;
	}
}
# evitar 4 vocales seguidas
sub generarMensaje{
	print "generar mens\n";
	for($i=0;$i<=$tamanio_mens;$i++){
		my $letra="0";
		$letra = &letra;
		# cuenta numero de consonantes, seguidas
		unless($vocales{$letra}){
			# si no es una vocal
			$contador_consonantes++;
		}
		# evita mas de 2 consonantes juntas
		if ($contador_consonantes > 1){
			$contador_consonantes=0;
			$letra = &vocal;
		}
		# evita palabras de mas de 12 letras
		if ($contador_letras > 12){
			$letra=$espacio;
			$contador_letras = 0;
		}
		$contador_letras++;
		#print "letra $letra anterior $anterior_letra\n";
		while ($letra eq $anterior_letra && $letra ne "r"){
			$letra = &letra;
			#print "\naqui entras\n";
		}
		$mensaje = $mensaje.$letra;
		$anterior_letra = $letra;
	}	
	return($mensaje);
}
print "$mensaje\n";
#sub print_letra

sub num_random ($){
	my $range = shift;
	my $num = int(rand($range));
	return ($num);
}
sub letra{
	my $random_number = &num_random($num_letras);
	my $letran = $bolsa[$random_number];
	return($letran);
}
sub vocal{
	my $random_number = &num_random(4);
	my $letran = $vocales[$random_number];
	return($letran);
}
