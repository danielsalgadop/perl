#!/usr/bin/perl
#para gestionar.global.rdsi necesitaba una matriz con los files ordenados por la fecha, y si la fecha coincidia primero las bajas.
use Data::Dumper;
use strict;
my @Files = qw(altardsi13122005.txt altardsi10012008.txt bajardsi13012008.txt altardsi11012008.txt bajardsi12012008.txt bajardsi10012008.txt altardsi12122005.txt);
my %invertir_orden;
my @nuevo_orden_Files;
foreach my $unfichero(@Files)
{	
	my $antiguo = $unfichero;
	$unfichero =~ s/\.txt//;
	my @unfichero = split(// , $unfichero);
	my @anio = splice(@unfichero , -4);
	my @mes = splice(@unfichero , -2);
	my @dia = splice(@unfichero , -2);
	my $anio = "@anio";
	my $mes = "@mes";
	my $dia = "@dia";
	my $resto = "@unfichero";
	$anio =~ s! !!g; 
	$mes =~ s! !!g; 
	$dia =~ s! !!g;
	$resto =~ s! !!g;
	my $nuevo_orden = "${anio}${mes}${dia}${resto}.txt";
	$invertir_orden{$antiguo}="$nuevo_orden";
	push (@nuevo_orden_Files , $nuevo_orden);
}
my @nuevo_orden_Files_sort = sort(@nuevo_orden_Files); # ordeno numericamente, lo malo es que (en misma fecha) primero estan las altas
my $fecha_comparar;
my $indice=0;
foreach my $unfichero (@nuevo_orden_Files_sort)
{
	my @unfichero = split(// , $unfichero);
	my @fecha = splice(@unfichero,0,8);
	my $fecha = "@fecha";
	$fecha =~ s! !!g;
	if ($fecha eq $fecha_comparar)    #dos files tienen misma fecha
	{
		#dar un cambio entre $indice y $indice -1, para cambiar el orden de alta y baja
		my	$derecha = $nuevo_orden_Files_sort[$indice];
		my $izquierda = $nuevo_orden_Files_sort[$indice -1];
		$nuevo_orden_Files_sort[$indice] = $izquierda;
		$nuevo_orden_Files_sort[$indice -1] = $derecha;
	}
	$fecha_comparar = $fecha;
	$indice++;
}
print Dumper(@nuevo_orden_Files_sort)."\n";
print Dumper(%invertir_orden);
