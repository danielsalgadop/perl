#!/usr/bin/perl -w

use strict;
use Test::More 'no_plan';
use validator_class;
my $validator = validator->new;

################# Validating esVacio
my @empty = (" ", "    ", "		","\t","\t\n\t","	
		");
foreach (@empty){
	ok(validator->esVacio($_), "valid vacio [".$_."]");
}
my @not_empty = ("1.2.3", "1.2.3.4.", "2 1,2,3,4", "siguiente vacio","\tn\t");
foreach (@not_empty){
	ok(!validator->esVacio($_), "valid vacio [".$_."]");
}

################# Validating Ips
my @valid_ips = qw(0.0.0.0 1.2.3.4 23.123.41.52 255.255.255.255);
foreach (@valid_ips){
	ok(validator->esIp($_), "valid ip ".$_);
}
my @not_valid_ips = qw(1 0.0.0.256 i.i.i.i 1.2.3.a 1.-3.4.4);
foreach (@not_valid_ips){
	ok(!validator->esIp($_), "not valid ip ".$_);
}
ok(validator->sonIps(@valid_ips),"valid sonIps");
ok(!validator->sonIps(@not_valid_ips),"not_valid sonIps");

################# Validating Emails
my @valid_emails = qw(bbbb@hotmail.com2.ss2.ssss lsdkjasdf@oooo.com);
foreach (@valid_emails){
	ok(validator->esEmail($_), "valid email ".$_);
}
my @not_valid_emails = qw(@hotmail.com2.ss2.ssss lsdkjasdf@oooo.com.2);
foreach (@not_valid_emails){
	ok(!validator->esEmail($_), "not_valid email ".$_);
}
ok(validator->sonEmails(@valid_emails),"valid sonEmails");
ok(!validator->sonEmails(@not_valid_emails),"not_valid sonEmails");

################# Validating Host
my @valid_hosts = qw(HOSTNAME VMAD04EFM1 host423Jdkd);
foreach (@valid_hosts) {
 	ok(validator->esHost($_), "valid host ".$_);
}
my @not_valid_hosts = qw(HO@STNAME VM%AD04EFM1 h&ost423Jdkd);
foreach (@not_valid_hosts) {
 	ok(!validator->esHost($_), "not_valid host ".$_);
}
ok(validator->sonHosts(@valid_hosts),"valid sonHosts");
ok(!validator->sonHosts(@not_valid_hosts),"not_valid sonHosts");