#!/usr/bin/perl -w
use strict;
package validator;

# Constructor
# Receives needed params
# Returns blessed object
sub new
{
	# receives the name of the object that it will create
	my $class = shift; # this is mandatory
	my $self = {}; # reference to a hash

	bless $self, $class;
	return $self;
}

##############################
# Method: esVacio
#	comprueba si el campo esta vacio o si esta compuesto entero por espacios en blanco
# incluidos tabuladores o saltos de linea
##############################
sub esVacio
{
	my $self 		= shift;
	my $estaVacio 	= shift;
	return $estaVacio =~/^\s*$/;
}

##############################
# Method: esIp
#	su valor no supera 255
#
# Returns
# 1- es IP correcta
# 0 - no  IP es correcta
##############################
sub esIp
{
	my $self = shift;
	my $esIP = shift;
	# sacado del package
	# http://search.cpan.org/~romm/Config-Hosts-0.01/lib/Config/Hosts.pm
  	return $esIP =~
	/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/ && ($1+0 | $2+0 | $3+0 | $4+0) < 0x100 ?
	1 : 0;
}

##############################
# Method: sonIps
# 	valida cada elemento con <esIp>
#
# Parameters: un array de emails
# Returns
##############################
sub sonIps(@)
{
	my $self = shift;
	foreach (@_){
		unless($self->esIp($_)){
			return 0;
		}
	}
	return 1;
}

##############################
# Method: esEmail
#
# Returns
# 1- es correcta
# 0 - no es correcta
##############################
sub esEmail
{
	my $self 	= shift;
	my $esEmail = shift;
	return 1 if $esEmail =~/^\w[\w\.\-]*\w\@\w[\w\.\-]*\w(\.[a-zA-Z]{2,4})$/;
	return 0;
}

##############################
# Method: sonEmails
#	valida cada elemento con <esEmail>
#
# Parameters: un array de emails
# Returns
##############################
sub sonEmails(@)
{
	my $self = shift;
	foreach (@_){
		unless($self->esEmail($_)){
			return 0;
		}
	}
	return 1;
}

##############################
# Method: esHost
# 	validat
##############################
sub esHost
{
	my $self = shift;
	my $host = shift;
	return 1 if $host =~/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/;
	return 0;
}

##############################
# Method: sonHosts
##############################
sub sonHosts
{
	my $self = shift;
	foreach (@_){
		unless($self->esHost($_)){
			return 0;
		}
	}
	return 1;
}

1;