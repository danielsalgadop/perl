#!/usr/bin/perl
use warnings;
use strict;
use Data::Dumper; 
sub HTML2String($){
	my $string =shift;
	my $out;
	my @aux = split (";",$string);
	foreach my $uncar(@aux){
		$uncar .= ";"; # le vuelvo a poner ";" al final de cada "letra"
		my $enhtml = HTML2Char($uncar);
		$out.=$enhtml;
	}
	return($out);
}
sub String2HTML($){
	my $string =shift;
	my $out;
	my @aux = split ("",$string);
	foreach my $uncar(@aux){
		my $enhtml = Char2HTML($uncar);
		$out.=$enhtml;
		#~ print "ANTES= ".$uncar." DESPUES= ".$enhtml."\n";
	}
	return($out);
}
sub HTML2Char($){
	my $in = shift;
	$in =~ s/&#//;
	$in =~ s/;//;
	my $carac = chr($in);
	return($carac);
}
sub Char2HTML($){
	my $in = shift;
	my $num_ascii = ord($in);
	return("&#".$num_ascii.";");
}
1
