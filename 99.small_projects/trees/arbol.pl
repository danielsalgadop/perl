#!/usr/bin/env perl
# codigo de joaquin ferrero

use strict;
use feature 'say';

use modelo_dato_arbol;
our %arbol;

use Data::Dumper;

# Primera fase:
# recorremos los niveles en orden inverso
# por cada nivel, creamos una referencia nueva ($json_new)
# por cada pareja hijo/padre, vemos si existe el hijo como padre en el árbol del nivel anterior
# si es así, el padre tendrá un hijo cuyo contenido apuntará a los nietos (hijos del hijo)
# y borramos a los nietos del árbol viejo
# Y si no, simplemente creamos una relación sencilla padre => hijo
# Crea una estructura hash de hashes

my $json_old = {};
my @descolgados;

# recorrer en orden de profundidad inversa
for my $nivel (reverse sort keys %arbol ) {
#    next if $nivel == 1;

    my $json_new;
    while (my ($hijo, $padre) = each %{ $arbol{$nivel} } ) {

	if (exists $json_old->{$hijo}) {
	    $json_new->{$padre}{$hijo} = $json_old->{$hijo};
	    delete $json_old->{$hijo};
	}
	else {
	    $json_new->{$padre}{$hijo} = {};
	}
    }

    push @descolgados, $json_old;

    $json_old = $json_new;
}
#say Dumper \@descolgados;	# comprobación de que no hay hijos descolgados. No debería haber ninguno
#say Dumper $json_old;		# > arbol.txt

# segunda fase:
# recorremos recursivamente la estructura,
# en cada nodo pasamos tres argumentos:
#	referencia al nodo en la estructura creada
#	referencia a la nueva estructura
#	nombre del nodo
my $json;
my $id = 0;

$json = recorre_recursivamente($json_old->{''}, $json, 0);	# empezar por el primer nodo

sub recorre_recursivamente {
    my($json_old, $json_ref, $clave) = @_;

    $json_ref->{'id'}   = $clave;				# creamos un nuevo nodo
    $json_ref->{'text'} = 'texto';
    $json_ref->{'item'} = [];

    for my $clave (keys %{ $json_old }) {			# creamos una estructura hash de array de hash de arrays...
    	push @{ $json_ref->{'item'} }, {};
    	recorre_recursivamente($json_old->{$clave}, $json_ref->{'item'}[-1], $clave);
    }
    
#    say Dumper $json_ref;
    return $json_ref;
}

#say Dumper $json;			# pre_json.txt

# tercera fase:
# creación del texto JSON
# recorremos recursivamente la estructura, con tres argumentos por nodo:
#	referencia al nodo a pintar
#	sangrado (espacios a poner en el lado izquierdo)
#	flag que indica si el nodo a pintar es el último de una serie (si no lo es, ponemos una coma más)
my $sangrado = 0;
construye_json($json, $sangrado, 1);				# pintar desde el primero nodo, y sin coma al final

# equivale a JSON::to_json
sub construye_json {
    my($json, $sangrado, $ultimo) = @_;

    if ('HASH' eq ref $json) {					# caso de que el nodo sea un hash
	print(" " x $sangrado, "{", "\n");					# {
	for my $clave (sort keys %{ $json }) {					# para todas las claves
	    if (ref $json->{$clave}) {						# si apunta a otra referencia
		construye_json($json->{$clave}, $sangrado + 2, 0);		# pintado recursivo, y con coma final
	    }
	    else {								# si es un nodo normal
	    	print(" " x ($sangrado + 2), qq("$clave":"$json->{$clave}"));	# lo sacamos
	    	if ($clave ne 'text') {						# como salen alfabéticamente, 'text' es el último (sí, es una ñapa, pero funciona)
	    	    print ',';							# mientras no sea 'text', sacamos una ',' extra
		}
		print "\n";
	    }
	}
	print(" " x $sangrado, '}');						# }
	if (!$ultimo) {								# si no es el último nodo
	    print(",");								# coma intermedia
	}
	print "\n";
    }
    elsif ('ARRAY' eq ref $json) {				# caso de que el nodo sea un array
	if (@{ $json }) {							# si el array tiene elementos
	    print(" " x $sangrado, '"item":', "\n");				# "item":
	    print(" " x $sangrado, "[", "\n");					# [
	    for my $elemento (@{ $json }) {					# para todos los elementos del array
		if (ref $elemento) {							# si son referencia a otra cosa
		    construye_json($elemento, $sangrado + 2, $elemento == $json->[-1]);	# pintado recursivo, poniendo una coma solo si es el último
		}
		else {									# si es elemento normal
		    print(" " x $sangrado, $elemento, "\n");				# lo sacamos tal cual
		}
	    }
	    print(" " x $sangrado, "],", "\n");					# ] (aquí hay una coma porque sabemos que luego vendrá 'text' (la ñapa sigue)
	}
    }
    elsif (ref $json) {
    	print("[", ref($json), "]\n");		# este caso no se debería dar nunca
    }
    else {
	print(" " x $sangrado, $json, "\n");	# este tampoco
    }
}

