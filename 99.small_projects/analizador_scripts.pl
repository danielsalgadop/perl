#!/usr/bin/perl -w
#Le das scripts que usan funciones,
# le das paths a las funciones,
# Detecta funciones usadas en scripts sin librerias origen
# detecta funciones origen sin uso en scripts

#A dia de hoy te dice si es necesario cargar las librerias que carga, es decir si hay un "use Algo" se aprende los sub Funcion y la busca dentro del script
use Getopt::Long;
use strict;
use Data::Dumper;

# use LeerCFGs;
if(!$ARGV[0]){
	print "DEBO RECIBIR EN ARGV[0], escribe el scritp que debo analizar\n";
	exit;
}
# Script que busca variables que no se usen, o librerias cargadas que no se usen sus funcionen
# estaria bene comprobar que las librerias esten cargadas (el md5sum sea igual dentro dl SAD y dentro de /perl5/)
# comprobar que tienen use strict
my %result;  # almaceno aqui lo que va a salir por pantalla
my %libfun; # relacion entre librerias y funciones incluidas
my %cfg = &LeerCfgSAD;
############# PARTE DE LIBRERIAS
my $pathlibreria = $cfg{'Path_web'}.$cfg{'Path_Librerias'};
opendir(my $dh, $pathlibreria) || die "can't opendir: $!";
    my @libs = grep { /pm$/ && -f "$pathlibreria/$_" } readdir($dh);
closedir $dh;
#print Dumper(@libs);
foreach my $unalib(@libs){
	open(UNALIB, $cfg{'Path_web'}.$cfg{'Path_Librerias'}."/".$unalib);
	my @unfich = <UNALIB>;
	close(UNALIB);
	$unalib =~ s/\.pm//; # le quito el .pm para poder compararlo luego con los use
	my @hay = grep (/use strict;/,@unfich);
	my @subs = grep (/^\s*sub\s+/,@unfich);
	# almaceno las librerias sin use strict
	if(!@hay){
		$result{$unalib} .= "1)NOTIENE use strict";
	}
	if(@subs){ # es raro que una lib no tenga sub,pero como vamos a hacer librerias que solo tengan uses
		my @limpio = &limpiarSubs(\@subs);
		foreach(@limpio){
			push(@{$libfun{$unalib}},$_);
		}
	}
}
############## PARTE DE SCRIPTS
my $pathscripts = $cfg{'Path_web'}.$cfg{'Path_Scripts'};
#print $pathscripts;
opendir(my $dhscripts, $pathscripts) || die "can't opendir: $!";
    my @scripts = grep { /pl$/ && -f "$pathscripts/$_" } readdir($dhscripts);
closedir $dhscripts;
foreach my $unscript(@scripts){
	print "ANALIZANDO ".$unscript."\n";
	open(UNSC, $cfg{'Path_web'}.$cfg{'Path_Scripts'}."/".$unscript);
	my @unfich = <UNSC>;
	close(UNSC);
	@unfich = grep(!/^\s*#/,@unfich); # quitar comentarios
	my @hay = grep (/use strict;/,@unfich);
	my @subs = grep (/^\s*sub\s+/,@unfich);
	my @uses = grep (/^\s*use/,@unfich); # ver que librerias usa
	#print Dumper(@uses)."xxxxxxxxxxxxxxxxxxxxxxxxxxx\n";
	# almaceno las librerias sin use strict
	if(!@hay){
		$result{$unscript} .= "1)NOTIENE use strict";
	}
	if(@subs){
		$result{$unscript}.="2) TIENE SUBRUTINAS, pensar en meterlos en libreria ";
		my @limpio = &limpiarSubs(\@subs);
		foreach (@limpio){
			$result{$unscript} .= "=".$_;
		}
	}
	if (@uses){ # comprobar que si usa una libreria hace uso de alguna funcion asociada
		my @limpio = &limpiarUses(\@uses);
		if(@limpio){
			foreach my $unuse(@limpio){
#				foreach (keys(%libfun)){print ">".$_."<\n";}
				my @funcionesAsociadas;
#				@funcionesAsociadas = @{$libfun{"miBajas"}};
#				print Dumper(@funcionesAsociadas);
#				exit;
				if($libfun{$unuse}){ # hay librerias que no estan el /Scripts
					print "SI que encuentro este unuse ".$unuse."\n";
#					@funcionesAsociadas = @{$libfun{$unuse}};
				}
				else{
					print "no encuentro este unuse como key de libfun >".$unuse."<<\n";
				}
				my $bien = 0; # esta bien si hay por lo menos una funcion de esta libreria que se usa
				foreach my $unafun(@funcionesAsociadas){
					my $hay = grep(/$unafun/,@unfich);
					if($hay){
						$bien = 1;
						last;
					}
				}
				if (!$bien){
					$result{$unscript}.="3) USA una libreria y NO USA sus funciones";
				}
				#print "para este use ".$unuse."\n";
				#print "hay estas funciones ";
				#print Dumper(@funcionesAsociadas);
				#exit;
			}
		}
	}
}
sub limpiarUses($){
#	return("Sdf","sdffwf","4fj","322tt2");
	my @return;
	my $ref_uses = shift;
	my @uses = @{$ref_uses};
#	print "esot esn dentro de limipiarUses\n".Dumper(@uses);
	foreach my $unuse(@uses){
		chomp($unuse);
		my @aux = split(/;/,$unuse);
		$unuse = $aux[0];
		$unuse=~s/^\s*use//;
		$unuse=~s/;//;
		while($unuse=~/\s/){ # quito los espacios en blanco
			$unuse=~s/\s//;
		}
		next if $unuse eq "strict" or $unuse eq "warnings" or $unuse eq "diagnostics"; # estos no son use al uso
		push(@return,$unuse);
	}
	return(@return);
}
sub limpiarSubs($){
	my @return;
	my $ref_subs = shift;
	my @subs = @{$ref_subs};
	foreach my $unsub(@subs){
		# quitar sub
		$unsub =~s/^\s*sub\s+//;
		if($unsub=~/\(/){ #si la definicion de funcion tiene los argumentos como ($$) hago split en (
			my @aux = split(/\(/,$unsub);
			$unsub = $aux[0];
		}
		elsif($unsub=~/\{/){ # la definicion de funcion no tiene argumentos
			my @aux = split(/\{/,$unsub);
			$unsub = $aux[0];
		}
		while($unsub=~/\s$/){ # quito los espacios en blanco
			$unsub=~s/\s//;
		}
		push(@return,$unsub);
	}
	return(@return);
}

#print Dumper(@scripts);
#exit;
#print Dumper(%libfun);
#exit;
#print Dumper(%result);
