#!/usr/bin/perl
# Generate (int) random number between a range of numbers

# Estaba intentando conseguir que randRange(10,50) == randRange(50,10), NO LO HE CONSEGUIDO
use strict;
use warnings;
use v5.18;
##################################
sub randRange {
  my ($min,$max) = @_;
  my $range = abs($max - $min);
  $range++;
  return int(rand($range)) + $min;
}
##################################


sub array_unique(@){   # php nomenclature
    my %seen;
    my @cleaned;
    foreach(@_){
        push (@cleaned,$_) unless($seen{$_});
        $seen{$_}++;
    }
    return(@cleaned);
}

my @arr;
for (1 .. 1000){
  push(@arr,randRange("10","50"));
}

my @arr2;
for (1 .. 1000){
  push(@arr2,randRange("50","10"));
}


@arr = array_unique(@arr);
@arr2 = array_unique(@arr2);
@arr = sort @arr;
@arr2 = sort @arr2;


if(@arr ~~ @arr2){
  say "son iguales";
}
else{
  say "NO lo son";
}


use Data::Dumper;
print Dumper @arr;