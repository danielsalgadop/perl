#!/usr/bin/perl -w
use strict;
use Data::Dumper;
use v5.22;
# necestito añadir dos elementos raiz a una ref a hash 
# $inicio es INPUT
# OUTPUT (mirar test)

my  $inicio = {
       "text" => "Despliegue Red",
       "id" => "0123",
       "item" => [
          {
             "text" => "International Services Alarms",
             "item" => [
                {
                   "text" => "VALOR",
                   "id" => "20060919092817"
                }
             ],
             "id" => "20060919092611"
          }
       ],
    };

#### coded developed
my %copia = %{$inicio};
$inicio->{id}   = 1;
$inicio->{item} = [ \%copia ];
$inicio->{text} = "NSIM";

my %copia2 = %{$inicio};
$inicio->{id}   = 0;
$inicio->{item} = [ \%copia2 ];
delete $inicio->{text};    # en nivel 0 no hay 'text'
#### END coded developed

print Dumper \%copia;
say "="x32;
print Dumper $inicio;
##### TEST
use Test::More tests => 1;
is_deeply($inicio, {
  'item' => [
    {
      'item' => [
        {
          'item' => [
            {
              'text' => 'International Services Alarms',
              'id' => '20060919092611',
              'item' => [
                {
                  'text' => 'VALOR',
                  'id' => '20060919092817'
                }
              ]
            }
          ],
          'id' => '0123',
          'text' => 'Despliegue Red'
        }
      ],
      'id' => 1,
      'text' => 'NSIM'
    }
  ],
  'id' => 0
}, 'test passed');
done_testing();