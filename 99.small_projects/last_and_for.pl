#!/usr/bin/perl -w
# queria saber si 'last' sale de un bucle 'for'
# RESPUESTA: sí

use strict;
use Data::Dumper;

my %hash = (
	1=>"value",
	2=>"value",
	3=>"value",
	4=>"value"
);

# recorrer en orden de profundidad inversa
for my $nivel (sort keys %hash) {
	print $nivel."\n";
	last if $nivel eq "2";    	# <<<<<<<<<<<<<<<< linea CLAVE: efectivamente
								# 'last' provoca salida de 'for'
}

# SALIDA
# 1
# 2