#!/usr/bin/perl
# aprendiendo a usar File::Find para que solo muestre un nivel de paths (que no lo haga recursivo)
use File::Find;
use Data::Dumper;
#%options = ("wanted",\&sub_wanted);
$path = "./";
# aqui veo como usar el hash de opciones
print ("\n SI RECURSIVO \n");
%options = ("wanted",\&sub_wanted);
# lo tipico seria find(\&sub_wanted, $path)  es decir una referencia a una subrutina

find(\%options, "$path");


print "\n NO RECURSIVO, no lo he conseguido!\n";
#find({ wanted => \&norecursivo, no_cdhdir }, '.');

%options_no_recursivo = {"wanted",\&norecursivo,"no_chdir", "1"};
find(\%options_no_recursivo , '.');



sub norecursivo {
	$todo = $File::Find::name;
	print $todo."\n";
}



sub sub_wanted { 
	# llamo todo al path+nombre_fichero

	$todo = $File::Find::name;
	# MODO CUTRE DE QUE NO SEA RECURSIVO
	# solo cumplira requisitos el que sea path definido en variable seguido del fichero, un subdirectorio no funcionara
	#if ($todo =~ m/^($path)($_)$/ && $todo !~ m/^($path)\./){
	#	unless (-d $todo) {
			print $todo."\n";
	#	}
	#}
}
print " -------------------------------- Sin usar File::Find\n";
#------------------ Sin usar File::Find ------------------
opendir(my $dh, $path) || die "can't opendir $path: $!";
# con el -f me aseguro que sea un archivo de texto
@dots = grep{ -f "$path/$_" } readdir($dh);
foreach my $unposible (@dots){
# aqui hago un filtrado mas fino
	if ($unposible !~ /^\./){
		print $unposible."\n";
	}
}
closedir $dh;
#print Dumper(@results);

