#!/usr/bin/perl
package RadVal;
use warnings;
use strict;
use Data::Dumper;
use Authen::Radius;

###############< Validate from a Radius Server >######################
######################################################################
## Function      : checkUserRadiusServer()			    ##
## Params        : $vsUser : User To Read                           ##
##		   $vsKey  : Key To Check	    		    ##
##		   $vsGroup : Group to Check. Future Use	    ##
## Desc          : Get User Info from some files in Radius Dir      ##
## Return        : Associative array. with statusCode and result.   ##
######################################################################    
sub checkUserRadiusServer($$$$)
{	
	my $vsUser 					= shift;
	my $vsKey 					= shift;
	my $radius_server 			= shift;
	my $radius_server_secret	= shift;

	my %return;
	my $hRadiusObject 		   = new Authen::Radius(Host => $radius_server, Secret => $radius_server_secret);
	if(!$hRadiusObject)
	{
		%return = ( "statusCode","ERROR","result","No Radius Server available. Radius library returns :". Authen::Radius::strerror);
	}
	else
	{
		#Authen::Radius->load_dictionary();
  		if($hRadiusObject->check_pwd($vsUser,$vsKey))
  		{
  			%return = ( "statusCode","OK","result","Valid user for Radius Server");
  		}
  		else
  		{
  			%return = ( "statusCode","ERROR","result","No valid user for Radius Server. Server returns :". Authen::Radius::strerror);
  		}
	}
	return %return;
}


# foreach (@{+RADIUSSERVERS})
# {
# 	print $_."\n";
# }
1;