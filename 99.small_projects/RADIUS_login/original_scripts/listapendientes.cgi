#!/usr/bin/perl -w
use strict 'vars';
use Data::Dumper;
use lib 'lib';
use asimov;
use cgi;
use URI::Escape;
my (%input);
&ReadParse(\%input);
my $ja = $input{'IP_Address'};
my @pdts = &TraerPendientes();
my $filtrado ="Todos"; # mensaje para indicar si estoy mostrando todos, clarify o manual
if($input{'filtrado'}){ # voy a quitar de @pdts los que sean dados de alta en web
	if($input{'filtrado'} eq "soloClarify"){ # voy a quitar de @pdts los que sean dados de alta en web
		@pdts = grep(/^\d{1,};/, @pdts); # quito los que tengan _ dentro de su Id Clarify 
		$filtrado="Casos de Clarify";
	}
	elsif($input{'filtrado'} eq "soloManual"){
		@pdts = grep(/^\d{1,}_/, @pdts); # mantengo los que tengan _ dentro de su Id Clarify 
		$filtrado="Manuales";
	}
}

my $hay_lock_ejecucion = 0;
if(-e $main::path_lock_ejecucion_actualizaNCM){
	$hay_lock_ejecucion =1;
}

#print "\nTOE\t\t".$main::path_lock_ejecucion_actualizaNCM."\n";
#exit;
print "Content-type: text/html\n\n";
print "
<html>
<head>
<style>
.tamanio{
	width:500px;
}
</style>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
<title>SolarWinds</title>
<link rel='stylesheet' type='text/css' href='asimov.css' />
<link rel='stylesheet' type='text/css' href='datatablemedia/css/demo_table.css' />
<link rel='stylesheet' type='text/css' media='all' href='css/jquery.modalbox.css' />
<script type='text/javascript' src='js/checkout.js'></script>
<script type='text/javascript' src='js/jquery-1.7.js'></script>
<script type='text/javascript' src='js/jquery.simplemodal-1.4.1.js'></script>
<script type='text/javascript' src='datatablemedia/js/jquery.dataTables.js'></script>
<script>
function FUNEnviar(){
// comprobar que por lo menos hay 1 seleccionado TODO
var s='no';
if (typeof document.formulario.editareste.length == 'undefined' && typeof document.formulario.editareste != 'undefined') {
	document.formulario.editareste.checked=true;
	s='si';
}
for ( var i = 0; i < document.formulario.editareste.length; i++ )
{
if ( document.formulario.editareste[i].checked ){
        s= 'si';
        break;
}
}
if ( s == 'no' ){
        alert('Debe seleccionar un registro a editar') ;
}
else{
	document.getElementById('idformulario').submit();
}
//document.formulario.submit();
}

//Para refrescar la pagina una sola vez tras borrar una linea.
var reloaded = false;
var loc=\"\"+document.location;
loc = loc.indexOf(\"?reloaded=\")!=-1?loc.substring(loc.indexOf(\"?reloaded=\")+10,loc.length):\"\";
loc = loc.indexOf(\"&\")!=-1?loc.substring(0,loc.indexOf(\"&\")):loc;
reloaded = loc!=\"\"?(loc==\"true\"):reloaded;

function reloadOnceOnly() {
    if (!reloaded) 
        window.location.replace(window.location+\"?reloaded=true\");
}

function Cliquea(valor){
texto = valor.split('-');
document.elimina.borrado.value=texto[0];
document.elimina.ipcpe.value=texto[1];
}
function Eliminar(){
document.elimina.submit();

}

validateAction = function(vsUser,vsPassword,viIndex,vipcpe)
{
if(vsUser != ''  && vsPassword != '' && viIndex!= '' && vipcpe!= '')
{			    	
	// POST data to send					       
	var sData    =  'identifier='+viIndex;
	sData	    += '&password='+vsPassword;
	sData	    += '&user='+vsUser;
	sData	    += '&ip='+vipcpe;
	document.body.style.cursor = 'wait';        
	\$.ajax({
		type: 'GET',
		url:  'contrasena.pl',
		data: sData,
		success: function(vsResponse,voXMLRequestObject)
		{
			document.body.style.cursor  		= 'auto';		                
			sReturn					= jQuery.trim(vsResponse);
			aReturn					= sReturn.split(';');
			if((aReturn[0]!='') && (aReturn[0]!='ERROR'))
			{
				\$.modal.close()
				alert('El campo se ha borrado correctamente')
				window.location.href='listapendientes.cgi';
			}
			else
			{
				\$.modal.close()
				alert('Hubo un error en el borrado del registro:\\n - Fallo de autenticación.\\n - El tipo de ERROR no permite su elminacióon');
			}
		},
		error: function(voXMLRequestObject,vsErrorType)
		{
		       \$.modal.close()
		       alert('Error','Error al enviar la peticion');              
		}
	 });
}
else
{	
	\$.modal.close()
	alert('Necesitas introducir usuario, contraseña y seleccionar un valor a borrar ');
}
		
};

openUserValidation = function()
{
\$('#validate').modal({onOpen: function (dialog) 
{
	dialog.overlay.fadeIn('fast', function () 
	{
		dialog.data.hide();
		dialog.container.fadeIn('fast', function () 
		{
			dialog.data.slideDown('fast');
		});
	});	
}});	

}
//Funcion que recoge los valores que se quieren modificar
//function Acceso(borra){
//   var1=borra;
//   pasarVariables('http://10.18.154.28/asimov/contrasena.pl', 'var1');
//}//Cierra function Acceso(borra)

//Funcion que pasa variables al popup para modificar los valores
//function pasarVariables(pagina, nombres){
//  pagina +=\"?\";
//  nomVec = nombres.split(\",\");
//  for (i=0; i<nomVec.length; i++)
//    pagina += nomVec[i] + \"=\" + escape(eval(nomVec[i]))+\"&\";
//  pagina = pagina.substring(0,pagina.length-1);
//  //location.href=pagina;
//  mipopup=window.open(pagina,'ventana','toolbar=0, resizable=0, scrollbars=0, menubar=0, status=0, location=0, width=500, height=300, top=150, left=400');
//  mipopup.document.comprueba.nuevo.value=\"hola\";
//}//Cierro function pasarVariables(pagina, nombres)

function Acceso(borra){
mipopup=window.open('http://10.18.154.28/asimov/contrasena.pl','ventana','toolbar=0, resizable=0, scrollbars=0, menubar=0, status=0, location=0, width=500, height=300, top=150, left=400');
//mipopup.document.comprueba.nuevo.value=borra;
mipopup.document.getElementById('nuevo2').innerHTML=\"hola\";
}

// pone las opciones de busqueda y pagina a la tabla
\$(document).ready(function() {
\$('#latabla').dataTable({
	\"sDom\": '<\"top tamanio\" ifpl>  <\"clear\"> rt<\"bottom tamanio\" flpi><\"clear\">'

});
} );
</script>
</head>

<body>
";
unless(@pdts){ # no hay archivo de pendientes
print "<h1>no hay datos pendientes</h1>"; # mejorar aspecto de esta mini web, pudiera usar un return parecido al que hace AlOuptput y entonces lo mostraria el index.cgi<br>";
}
else{
if($hay_lock_ejecucion){ # hay una ejecucion
		print "<div id='cabecera'>VISUALIZACION DE PENDIENTES</div><br><b>Existe una ejecuci&oacute;n </b>actualmente que puede modificar estos datos, por eso <b>no es posible editarlos</b> espera, por favor unos minutos y vuelve a la pagina principal<br><br><form name='formulxxxarioNOBORRAR'>"; # por una extrania razon, que desconozco si no hay form, la tabla sale descolocada
}
else{
	print "	<div id='cabecera'>¿CUAL QUIERES EDITAR?</div><br><form name='formulario' id='idformulario'  action='final.cgi' method='POST'>";
	print "<input type='hidden' name='nombre' value='anda'>";
}
#print Dumper(%input);
#system("echo \"ESPENDIENTE: ".Dumper(%input)."\" > /tmp/pruebapdtes");
#my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
#$mon++;
#$year+=1900;
my $fecha_actual=&GenerarFecha();
if(!defined($input{'espendiente'})){
	$input{'espendiente'}=0;
}
elsif($input{'espendiente'} == 1){ # input existe cuando llegas aqui desde final.cgi
#Solamente cuando vuelve de final por una modificacion en el fichero de pdtes
	my $ip_linea = $input{'IP_Address'};
	my $archivoviejo = '/home/op1/asimov/ncm_clarify/pdtes_NCM_Clarify.list';
	my $archivonuevo = '/home/op1/asimov/ncm_clarify/pdtes_NCM_Clarify.list.tmpx';
	open my $FH, q[<], $archivoviejo; # modo lectura
	open my $NFH, q[>], $archivonuevo; # modo escritura
	while (<$FH>) {                        
		if ( m/$ip_linea/ ){                    
	                #Pasamos de mayusculas a minusculas cuando es uno nuevo
                	$input{'Nombre_Organizacion'}=uc($input{'Nombre_Organizacion'});
                	$input{'Nombre_Equipo'}=uc($input{'Nombre_Equipo'});
			$input{'COD3'}=uc($input{'COD3'});
			#Armamos la nueva linea y la escribimos en el fichero nuevo
			my $linea_modificada=$input{'ID_Clarify'}.";"."ALTA_EDITAR".";".$input{'COD3'}.";".$input{'ID_Organizacion'}.";".$input{'Nombre_Organizacion'}.";".$input{'SITE_ID'}.";".$input{'IP_Address'}.";".$input{'Nombre_Equipo'}.";".$input{'Community'}.";".$input{'Modo_Autentificacion'}.";".$input{'Password_enable'}.";".$input{'Fabricante'}.";".$input{'Tipos_acceso'}.";".$input{'Version_SNMP'}.";".$input{'UserSNMPv3'}.";".$input{'PwdSNMPv3'}.";".$input{'TipoAuthSNMPv3'}.";".$input{'TipoEncripProtocolSNMPv3'}.";".$input{'AuthPassSNMPv3'}.";".$input{'PrivPassSNMPv3'}.";".$input{'NivelSegSNMPv3'}.";".$input{'contextSNMPv3'}.";".$input{'COD_Respuesta'}.";".$input{'Respuesta_Operacion'}.";".$input{'FECHA'};
			#print "$linea_modificada\n";
			print {$NFH} "$linea_modificada\n";
			next;                          
		}
		#print "$_\n";
		print {$NFH} $_;    # se imprime en el nuevo archivo
	}
	close $FH;
	close $NFH;
	#rename("$archivoviejo","$archivoviejo.bck_$year$mon$mday$hour$min$sec");
	rename("$archivoviejo","$archivoviejo.bck_$fecha_actual");
	rename("$archivonuevo","$archivoviejo");
	print "<script>reloadOnceOnly();</script>"; # Se refresca la pagina una vez tras modificar el fichero.

#	print Dumper(%input);
#	        my $output_mensaje = &AlOutput(\%input);
#	        my $error;
#	        $error = 1 if ($output_mensaje =~ /^ERROR/);
#	        print "<div id='mensaje' style='color:";
#	        if($error){
#	                print "red";
#	        }
#	        else{
#	                print "green";
#	        }
#	        print "' >".$output_mensaje."</div>";
}
elsif($input{'espendiente'} == 2){ # input existe cuando llegas aqui desde final.cgi
#Solamente cuando vuelve de final por uno nuevo
	my $yaexiste=&ComprobarExiste($input{'IP_Address'});
	if ($yaexiste == 0){
		if ($input{'COD_Operacion'}=~/ALTA/) {
		my $archivoviejo = '/home/op1/asimov/ncm_clarify/pdtes_NCM_Clarify.list';
		open my $FH, q[>>], $archivoviejo; # modo anadir
		#Armamos la nueva linea y la anadimos
		#my $fecha_actual="$year-$mon-$mday $hour:$min:$sec";
		#my $fecha_actual=&GenerarFecha();
		#Pasamos de mayusculas a minusculas cuando es uno nuevo
		$input{'Nombre_Organizacion'}=uc($input{'Nombre_Organizacion'});
		$input{'Nombre_Equipo'}=uc($input{'Nombre_Equipo'});
		$input{'COD3'}=uc($input{'COD3'});
		my $linea_alta=$input{'ID_Clarify'}.";"."ALTA_MANUAL".";".$input{'COD3'}.";".$input{'ID_Organizacion'}.";".$input{'Nombre_Organizacion'}.";".$input{'SITE_ID'}.";".$input{'IP_Address'}.";".$input{'Nombre_Equipo'}.";".$input{'Community'}.";".$input{'Modo_Autentificacion'}.";".$input{'Password_enable'}.";".$input{'Fabricante'}.";".$input{'Tipos_acceso'}.";".$input{'Version_SNMP'}.";".$input{'UserSNMPv3'}.";".$input{'PwdSNMPv3'}.";".$input{'TipoAuthSNMPv3'}.";".$input{'TipoEncripProtocolSNMPv3'}.";".$input{'AuthPassSNMPv3'}.";".$input{'PrivPassSNMPv3'}.";".$input{'NivelSegSNMPv3'}.";".$input{'contextSNMPv3'}.";".$input{'COD_Respuesta'}.";".$input{'Respuesta_Operacion'}.";".$fecha_actual;
		#print "$linea_modificada\n";
		print {$FH} "$linea_alta\n";
		close $FH;
		print "<script>reloadOnceOnly();</script>"; # Se refresca la pagina
		}
		elsif ($input{'COD_Operacion'}=~/BAJA/) {
			print "Este equipo no esta en Solarwinds, no es posible borrarlo<br><br>";
		}
	}
	elsif ($yaexiste == 1){
		if ($input{'COD_Operacion'}=~/ALTA/) {
			print "Este equipo ya esta en pendientes, si quieres modificarlo usa Editar<br><br>";
		}
		elsif ($input{'COD_Operacion'}=~/BAJA/) {
			print "Este equipo ya esta en pendientes, si quieres borrarlo usa Borrar<br><br>";
		}
	}
	elsif ($yaexiste == 2){
		if ($input{'COD_Operacion'}=~/ALTA/) {
			# Para altas
			print "Este equipo ya esta en Solarwinds, si quieres modificarlo usa modificar datos<br><br>";
		}
		elsif ($input{'COD_Operacion'}=~/BAJA/) {
			# Para bajas
			my $salida = qx[/bin/sh /var/www/html/asimov/listapendientes.sh $input{'IP_Address'}];
			if ($salida == 0){
				print "<script>alert('Baja de $input{'IP_Address'} realizada con exito');</script>";
			}
			else{
				print "<script>alert('Baja de $input{'IP_Address'} no realizada, el nodo no existe en Solarwinds');</script>";
			}
			print "<script>reloadOnceOnly();</script>"; # Se refresca la pagina
		}
	}
}

print "ORDENADOS SEGUN CRITERIO=".$filtrado."<br>";
print "<a href='?filtrado=soloClarify'>solo Clarify</a><br>";
print "<a href='?filtrado=soloManual'>solo Manuales</a><br>";
print "<a href='?filtrado=Todos'>Todos</a><br>";
print "<table id='latabla'><thead><th> &nbsp;</th>";
my @campos = &CamposOrdenListaPendientesCGI();
my %hashCampos = &HashCamposPendiente();
my $tam_campos = @campos;
foreach (@campos){
	print "<th>".$_."</th>\n";
}
print "	</thead><tbody>\n";
my $parimpar=0;
foreach my $unpendiente(@pdts){ # recorro los pendientes

	$parimpar++;
	my $esRun = &HayRun($unpendiente); # detecto si esta linea es RUN (No se si usar esta funcion ya que estoy haciendo dos split de los mismos datos casi seguidos)
	my @aux = split(/;/,$unpendiente);
	# la posicion de cada $aux[$i] 
	print "<tr ";
	if($esRun){  # si esta en Run, tiene clase esRun
		print "class='esRun'";
	}
	elsif($parimpar %2 == 0){
			print "class='alt'";
	}
	print ">\n";
	print "<td>";
	#~ print "uesRUN".$esRun."unpendiente".$unpendiente;
	if (!$esRun and (!$hay_lock_ejecucion)){ # solo es seleccionable si no esta en estado esRun
		print "<input type='radio' name='editareste' value='".$aux[0]."-".$aux[6]."' onclick='javascript:Cliquea(this.value)'>";  # creo que este /td debiera ir fuera de este if # COMPROBAR
		#print "<input type='radio' name='editareste' value='".$aux[0]."' onclick='javascript:Cliquea(this.value)'>";  # creo que este /td debiera ir fuera de este if # COMPROBAR
	}
	print "</td>\n";

	foreach my $uncampo(@campos){
		my $posicion_fichero = $hashCampos{$uncampo}[0] - 1; # Resto uno por que es un array y cuenta desde cero
		#~ my $posicion_fichero = ;
		print "<td>";
		#~ print "este campo".$uncampo." debe tener esta posicion ".$posicion_fichero;
		#~ if (defined($aux[$posicion_fichero])){# && ($aux[$posicion_fichero]) &&  ($aux[$posicion_fichero] ne "")){ # para quitar warning en apache
		#~ if (($aux[$posicion_fichero]) && ($aux[$posicion_fichero] ne "")){
		if ($aux[$posicion_fichero]){
			if($uncampo =~ /(Password)|(Pwd)/){
				print &CampoAsteriscos($aux[$posicion_fichero]);
			}
			else{
				print &String2HTML(&QuitarEspaciosExtremos($aux[$posicion_fichero])); # hashCampos tiene los datos de las posiciones $uncampo trae por orden los nombres de los campos.
			}
		}
		#~ }
		print "</td>\n";
	}
	print "</tr>\n";
} # cierro el recorrido de los pendientes my $unpendiente(@pdts)
print "</tbody></table></form>";

unless($hay_lock_ejecucion){ # solo cerrar form y mostrar el boton enviar si no hay lock
	print "<input type='button' name='Edita' value='Editar' onclick='javascript:FUNEnviar()'>";
	print "<input type='button' name='Borra' value='Borrar' onclick='javascript:openUserValidation();'>";
}
print "<div id='validate' name='validate' title='Validar Accion' style=\"display:none\"/>";
print "<h3 style='text-align:center;text-decoration:underline' id='div_title'>Valida la acción</h3>";
print "<table width='100%' align='center'>";
print "<tr>";
print "<td>";
print "<strong>Usuario:</strong>";
print "</td>";
print "<td>";
print "<input type='text' id='user' name='user' value='' onchange='javascript:document.getElementById(\"hUser\").value=this.value;'/>";
print "</td>";
print "</tr>";
print "<tr>";
print "<td>";
print "<strong>Clave:</strong>";
print "</td>";
print "<td>";
print "<input type='password' id='clave' name='clave' value='' onchange='javascript:document.getElementById(\"hPassword\").value=this.value;'>";
print "</td>";
print "</tr>";
print "<tr>";
print "<td colspan='2' style='text-align:center'>";
print "<input type='button' value='Enviar' onclick='javascript:validateAction(document.getElementById(\"user\").value,document.getElementById(\"clave\").value,document.getElementById(\"borrado\").value,document.getElementById(\"ipcpe\").value)'/>";
print "</td>";
print "</tr>";
print "</table>";
print "</div>";

# --- Para eliminar una linea del archivo de Pendientes. --- #
print "<form name='elimina' action='listapendientes.cgi'>";
print "<input type=hidden name=ipcpe id='ipcpe' value=''>";
print "<input type=hidden name=borrado id='borrado' value=''>";
print "<input type=hidden name='hUser' id='hUser' value=''>";
print "<input type=hidden name='hPassword' id='hPassword' value=''>";
print "<input type=hidden name=contrasena value=''>";
if($input{borrado} && $input{borrado} ne ""){
  if($input{contrasena} eq "" && $input{usuario} eq ""){
    print "<script>Acceso('$input{borrado}');</script>";
  }
  elsif($input{contrasena} ne "" && $input{usuario} ne ""){

    #print "HOOOOOOOOOOOOOOOOOLLLLLLLLLLLLLLLLLAAAAAAAAAAAAAAAAAAAAAAAAAAA";
            chomp($input{borrado});
            print "<br>Borrado: $input{borrado}<br>";
            chomp($input{contrasena});
            print "<br>Contrasena: $input{contrasena}<br>";
            chomp($input{usuario});
            print "<br>Usuario: $input{usuario}<br>";
	    
            #my @Busca=grep /$input{borrado};/,@pdts; # Busca el equipo a dar de baja en Pendientes.
 
            ## --- Si lo encuentra, se borra del archivo. --- #
            #if(@Busca){
            #  chomp($Busca[0]);
            #  my $ruta="/home/op1/asimov/ncm_clarify";
            #  open(ABRE,"$ruta/pdtes_NCM_Clarify.list");
            #    my @Abre=<ABRE>;
            #  close(ABRE);
#
#            open(CAMBIA,"> $ruta/pdtes_NCM_Clarify.list");
#            foreach my $cambia (@Abre){
#              chomp($cambia);
#
#              if($cambia!~/^$input{borrado};/){
#                print CAMBIA "$cambia\n";
#                #print "No se borra: $cambia<br>";
#              }
#              #else{
#              #  print "Se va a borrar: $cambia<br>";
#              #}
#            }#Cierra foreach my $cambia (@Abre)
#            close(CAMBIA);
#          }#Cierra if(@Busca)
          #print "<script>reloadOnceOnly();</script>"; # Se refresca la pagina una vez tras borrar una linea.
          }#Cierra if($input{borrado} ne "")
        }#Cierra if($input{borrado} && $input{borrado} ne "")

        print "</form>";

}#Cierra else
print "</body> </html> ";
