#!/usr/bin/perl

package NCM_fich;

use strict;
use warnings;
use NCM;
use POSIX;
use Data::Dumper;

# Constantes ficheros

our $NUM_CAMPOS_CAB=3; # Datos cabecera. Partiendo desde 0


### escribeLOG ###########################################################
sub escribeLOG($){
 
 my $mensaje=shift;
 my $fecha=NCM::fechaActual();
 chomp($fecha);
 chomp($mensaje);
 $mensaje=~s/\n//g;
 $mensaje=~s/\t//g;

 open FLOG, ">> $NCM::FICH_LOG";
 printf FLOG "$fecha - $mensaje\n";
 close FLOG;
}
### escribeLOG ###########################################################

### obtFichInClarify ######################################################
sub obtFichInClarify(){
	my %result;   # lugar donde almaceno el status y el fichero que busco
	$result{'status'} = "OK";    # es OK hasta que no se demuestre lo contrario
	my $fecha_actual =strftime('%Y%m%d',localtime()); # el archivo tiene aaaammdd pero hora y minuto distinto asi que busco fecha_actual aaaammdd
	opendir(INDIR,$NCM::DIR_CARGA_CLARIFY_IN); # abro el fichero de in de clarify
	my  @file = grep {$_!~/result$/ && $_=~/$fecha_actual/ && $_=~/^datos_NCM/ && -f "$NCM::DIR_CARGA_CLARIFY_IN/$_"  && -s "$NCM::DIR_CARGA_CLARIFY_IN/$_"} readdir(INDIR); # me quedo solo con los que NO acaben en result (ya estan tratados) matcheen el fecha_actual empiecen por datos_NCM y son texto plano y tiene NO zero size
	closedir INDIR;
	@file=sort(@file); # ordeno para quedarme con el mas moderno en caso de que haya mas de uno
	unless(@file){
	        $result{'status'}="no ha llegado nada de clarify o ya estaba tratado para este timestamp ".$fecha_actual;
	}
	if(scalar(@file) > 1){  # si hay mas de 2 ficheros
	        escribeLOG("\n hay mas de una entrada de clarify para este dia ".$fecha_actual." he usado la mas moderna;\n");
	}
	$result{'vvalor'}= pop(@file); # si no existe @file devolvera undef. no pasa nada.
	return(%result);
}
### obtFichInClarify ######################################################

### existeFich ############################################################
sub existeFich($){
# Comprobamos que existe y se puede leer. 
 my $nomF=shift;
 my $resultado=0;
 
 if (-r $nomF)
   { $resultado=1;};
 return($resultado);
}
### existeFich ############################################################

### resetCabFich ##########################################################
sub resetCabFich() {
  # Inicializa/devuelve el resgistro de la cabecera de un fichero.

  my %cabFich=('HOST' => '',            #Hostname equipo creo fichero
               'FECHA' => '',           #Fecha fich. YYYYMMDDHHMMSS
               'ALTAS' => -1,           #Numero altas
               'BAJAS' => -1);          #Numero bajas
  return(%cabFich);
}
### resetCabFich ##########################################################

### creaCabFich ###########################################################
sub creaCabFich() {
  # Crea el resgistro de la cabecera de un fichero.

  my %cabFich=('HOST' => $ENV{HOSTNAME},       	   #Hostname equipo creo fichero
               'FECHA' => NCM::fechaActualFich(),  #Fecha fich. YYYYMMDDHHMMSS
               'ALTAS' => 0,           	           #Numero altas
               'BAJAS' => 0);                      #Numero de bajas
  return(%cabFich);
}
### creaCabFich ###########################################################

### obtCabFich ###########################################################
sub obtCabFich($) {
  # Obtiene el registro de la cabecera de un fichero.
  my $nomF=shift;
  my %cabFich=resetCabFich();
  my $linea;
  my @aux;
  
  if (-r $nomF)
    {
       if (open (FDAT,"$nomF"))
         {
            $linea=<FDAT>;
            chomp($linea);
            close(FDAT);
            #L�mite -1 para que coja los campos vacios.
            @aux=split /\;/,$linea,-1;
            if ($#aux == $NUM_CAMPOS_CAB)
              { %cabFich=('HOST' => $aux[0],
                          'FECHA' => $aux[1],  #Fecha fich. YYYYMMDDHHMMSS
                          'ALTAS' => $aux[2], 'BAJAS' => $aux[3]);
              }
            else
              {escribeLOG("ERROR: Cabecera fichero $nomF erronea");};
         }
       else
         { escribeLOG("ERROR: No es posible acceder al fichero $nomF");};
    }
  else 
    { escribeLOG("ERROR: No es posible acceder al fichero $nomF");};           
  return(%cabFich);
}
### obtCabFich ###########################################################

### cargaFichDatos #######################################################
sub cargaFichDatos($) {
  # Carga los datos de un fichero en un array lo devuelve. 
  # @DATOS=<NULL> si existe alg�n problema al abrirlo.
  my $nomF=shift;
  my @DATOS;
  my $linea;
    
  if (-r $nomF)
    {
       if (open (FDAT,"$nomF"))
         {
            @DATOS=<FDAT>;
            close(FDAT);
         }
       else
         { escribeLOG("ERROR: No es posible acceder al fichero $nomF");};
    }
  else 
    { escribeLOG("ERROR: No es posible acceder al fichero $nomF");};           
  return(@DATOS);
}
### cargaFichDatos #######################################################

### grabaFichDatos #######################################################
sub grabaFichDatos($@) {
  # guarda los datos de un array en un fichero con su cabecera. 
  
  my $nomF=shift;
  my @DATOS=shift;
  my %cabF=creaCabFich();
  my @aux;
  my $linea;
  my $error=0; # Devuelve 0 si no existe error.
    
  push @DATOS,'###PROCESADO###';  
  # Comprobamos las altas y bajas para actualizar la cabecera.
  @aux=grep /\;ALTA_/,@DATOS;
  $cabF{ALTAS}=$#aux+1;
  @aux=grep /\;BAJA_/,@DATOS;
  $cabF{ALTAS}=$#aux+1;
  if (open (FDAT,">$nomF"))
    {
      print FDAT "$cabF{HOST};$cabF{FECHA};$cabF{ALTAS};$cabF{BAJAS}\n";
      while ($linea=<@DATOS>)
        {
			chomp($linea);
			$linea=~s/\n//g;
			$linea=~s/\t//g;
			print FDAT "$linea\n";
		};
      close(FDAT);
    }
  else
    { escribeLOG("ERROR: No es posible crear el fichero $nomF");
      $error=11;
    };
  return($error);
}
### grabaFichDatos #######################################################


1; #Devolvemos true al terminar el modulo
