#!/usr/bin/perl -w
use strict;
use Data::Dumper;
use miHTMLParseador;
#~ use utf-8;
my $id_clarify="";
my $path_pdts="/home/op1/asimov/ncm_clarify/pdtes_NCM_Clarify.list";
#~ my $path_pdts="/home/op1/asimov/ncm_clarify/pdtes_NCM_dani"; #SED
our $PATH_TOTAL_CPE="/home/clarimov/bbdd/TOTALDATOS.txt";
my $path_una_sola_linea="/tmp/una_linea_pdt".localtime; # esto no se usa por ahora, es para generar un archivo con una sola linea y que asi actualizaNCM pudiera ir a buscar solo los datos de este 
my $path_lock_editar_pdts="/tmp/path_lock_editar_pdts.lock"; # evita que dos ediciones de pdts editen a la vez
our $path_lock_ejecucion_actualizaNCM="/tmp/pdtes_NCM_Clarify.lock"; # evita que se edite pdtes mientras hay una ejecucion de actualizaNCM.pl 
my $iteracciones_lock_max = 3;
my $segundos_lock = 1;
my %campos = &HashCamposPendiente(); # %campos tiene el orden de la entrada y del orden de la web

use lib '/home/op1/asimov/ncm_clarify';
use NCM;
use NCM_BBDD;

sub HayRun($){
# recibe una linea de pdts, si es RUN devuelve 1
	my $linea = shift;
	my @aux = split(/;/,$linea);
	if ($aux[1] =~ /RUN$/){
		return(1);
	}
	return(0);
}

sub CamposPendiente(){  # se usa en listapendientes.cgi, el resto usan CamposOrdenFicheroPdts
	# devuelve un array con los campos que se usan en pendientes
	my @campos = qw(ID_Clarify COD_Operacion Fecha Cod_ERROR Desc_Error COD3 ID_Organizacion Nombre_Organizacion SITE_ID IP_address Nombre_Equipo Fabricante Modo_Autentificacion Tipo_acceso Version_SNMP Community_RW Password_enable  UserSNMPv3 PwdSNMPv3 TipoAuthSNMPv3 TipoEncripProtocolSNMPv3 AuthPassSNMPv3 PrivPassSNMPv3 NivelSegSNMPv3 contextSNMPv3);
	return(@campos);
}
 sub CamposOrdenFicheroPdts{ # devuelve un array con el nombre de los campos ordenados como estan en Fichero pendientes
	my %campos_input;
	foreach my $key(keys(%campos)){ # genero un hash temporal con Numero=>valor
		$campos_input{$campos{$key}[0]} = $key;  # tengo con los keys ordenados. IMP que no se repita ningun numero o el primer valor se perdera
	}
	my @campos_input; # array con los campos ordenados para Fichero
	for my $key ( sort {$a<=>$b} keys %campos_input) {
		push (@campos_input, $campos_input{$key}); # campos web tiene los campos ordenados
	}
	return(@campos_input);
}

sub CamposOrdenFinalCGI(){
	# ordeno los campos para la web (en funcion del segundo numero del array anonimo del hash)
	# devuelve un array con el orden que se veran los campos en la web final.cgi
	my %campos_web;
	foreach my $key(keys(%campos)){ # genero un hash temporal con Numero=>valor
		$campos_web{$campos{$key}[2]} = $key;  # tengo con los keys ordenados. IMP que no se repita ningun numero o el primer valor se perdera
	}
	my @campos_web; # array con los campos ordenados para web
	for my $key ( sort {$a<=>$b} keys %campos_web) {
		next if $key == 0;
		push (@campos_web, $campos_web{$key}); # campos web tiene los campos ordenados
	}
	return(@campos_web);
}

sub CamposOrdenListaPendientesCGI(){
	# ordeno los campos para la web (en funcion del segundo numero del array anonimo del hash)
	# devuelve un array con el orden que se veran los campos en la web final.cgi
	my %campos_web;
	foreach my $key(keys(%campos)){ # genero un hash temporal con Numero=>valor
		$campos_web{$campos{$key}[1]} = $key;  # tengo con los keys ordenados. IMP que no se repita ningun numero o el primer valor se perdera
	}
	my @campos_web; # array con los campos ordenados para web
	for my $key ( sort {$a<=>$b} keys %campos_web) {
		next if $key == 0;
		push (@campos_web, $campos_web{$key}); # campos web tiene los campos ordenados
	}
	return(@campos_web);
}


sub HashCamposPendiente() { # se usa en final.cgi
	# el key es el nombre que aparece en la web
	# el valor indica 1) posicion en fichero de entrada 2) posicion en listapendientes.cgi 3) posicion en la final.cgi, 4) determina si es text "t", si es select "s" 5) opciones del desplegable, 6) prepend antes del <input>, 7) dentro del <input> 8) apend despues del <input>
	
  my %campos=(
  		'ID_Clarify' => [1,1,1,"t",[""],"","readonly='readonly' class='miDisabled'",""],						# Cod Clarify y/o manual
  		'COD_Operacion' => [2,3,2,"d",["ALTA_BTCONF","BAJA_BTCONF"],"","",""],					# T.Operacion: ALTA_BTCONF, etc.
  		'COD3' => [3,4,3,"t",[""],"","",""],							#Codigo 3 de cliente
  		'ID_Organizacion' => [4,5,7,"t",[""],"","",""],				#Codigo Organizacion
		'Nombre_Organizacion' => [5,8,8,"t",[""],"","",""],			#Nombre Cliente
		'SITE_ID' => [6,9,9,"t",[""],"","",""],						#Site ID
		'IP_Address' => [7,10,10,"t",[""],"","",""],					#IP Gestion
  		'Nombre_Equipo' => [8,11,11,"t",[""],"","",""], 				#Hostname equipo
		'Community' => [9,12,15,"t",[""],"","",""],					# Community Escritura
		'Modo_Autentificacion' => [10,13,13,"d",["RADIUS","TACACS","BT"],"","",""],		# Modo Auth.: RADIUS, TACACS, BT,etc
		'Password_enable' => [11,17,16,"t",[""],"","",""],			# Clave acceso enable
		'Fabricante' => [12,14,12,"d",["Cisco","Teldat","Juniper"],"","",""],					# Fabricante
		'Tipos_acceso' => [13,15,14,"d",["telnet","ssh"],"","",""],				# Metodo descarga config.: TFTP,etc. 
		'Version_SNMP' => [14,16,17,"d",[1,2,3],"","",""],				# Version SNMP
		'UserSNMPv3' => [15,25,18,"t",[""],"<div id='snmpv3'>","",""],					# Usuario acceso al equipo
		'PwdSNMPv3' => [16,26,19,"t",[""],"","",""],					# Clave acceso
		'TipoAuthSNMPv3' => [17,18,20,"d",["MD5","SHA1"],"","",""],				# Tipo Auth SNMP V.3
		'TipoEncripProtocolSNMPv3' => [18,18,21,"d",["AES","DES"],"","",""],	# Tipo Encriptacion Protocolo  SNMP V.3
		'AuthPassSNMPv3' => [19,20,22,"t",[""],"","",""],				#   SNMP V.3
		'PrivPassSNMPv3' => [20,21,23,"t",[""],"","",""],				#   SNMP V.3
		'NivelSegSNMPv3' => [21,22,50,"d",["noAuthNoPriv","authNoPriv","authPriv"],"","",""],				#   SNMP V.3
		'contextSNMPv3' => [22,23, 51,"t",[""],"","","</div>"],	#   SNMP V.3
		'COD_Respuesta' => [23,6,123,"t",[""],"","readonly='readonly' class='miDisabled'",""],					# codigo error
		'Respuesta_Operacion' => [24,7,122,"t",[""],"<div id='caja_error'>","readonly='readonly' class='miDisabled'",""],				# Codigo respuesta operacion ## no se si este es el codigo error
		'FECHA' => [25,2,124,"t",[""],"","readonly='readonly' class='miDisabled'","</div>"],						# Fecha llegada registro
        );
  return(%campos);
  		#~ que hago con estee ????#~ 'ENABLELEVEL' => [25,24,30,"d",[1234,2345,3456],"","",""],					# Vacio para equipos no Cisco.
        #~ 'COMMRO' => [28,1],	# Community Lectura
        #~ 'MACHINETYPE' => [29,1],		# Tipo CPE. Igual que Vendor.
        #~ 'USRSNMPV3' => [16,14,56,"t",[""],"","","</div>"],					# Usr. SNMP V.3
};

sub Traer15DiasSinSnmp_Ficheros(){
	my %return;
	my $path = "/home/clarimov/bbdd"; 
	my $fichero = "LastInventory.txt"; # dentro de path habra distintas carpetas (una por maquina) donde pueda haber esto
	my @ls = `ls $path/*/$fichero`;
	foreach my $copia (@ls){
		chomp($copia);
		my @sp = split(/\//, $copia); #por ej /home/clarimov/bbdd/QMAD04NCM2/LastInventory.txt
		my $tam_sp = @sp;
		my $maquina = $sp[-2]; # es el penultimo valor del split
		open (FICH, "<".$copia) or die " no he podido abrir ".$copia;
		my @datos = <FICH>;
		close(FICH);
		$return{$maquina}=[@datos];
	}
	return(%return);
}

sub TraerPendientes(){
	&LogicaLock();
	my @pdts=();
	open(PDT,"< ".$path_pdts) or return(@pdts);# si no existe el fichero devuelvo @pdts vacio
	while(<PDT>){
		next if &EstaVacio($_); # ignoro lineas vacias
		push @pdts,$_;
	}
	close(PDT);
	return(@pdts);
	&BorrarLock();  # esto se esta ejecutando....
}
sub EsIP($){
	my $ip = shift;
	if($ip){ # quita warnings en apache
		if ($ip=~/^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$(?(?{$1 >= 256 || $2 >= 256 || $3 >= 256 || $4 >= 256})(?!))/){
			return(1);
		}
	}
	return(0);
}
sub EstaVacio($){
	#~ return(1);
	my $dato = shift;
	if($dato){ # si existe puede estar vacio o no... 
		if ($dato =~ /^\s*$/){ # esta vacio
			return(1);
		}
		else{ # no esta vacio
			return(0); 
		}
	}
	return(1); # si no existe es que esta vacio
}
sub GenerarID_Clarify(){
	if($id_clarify eq ""){ # si no esta generado lo genero (pero solo la primera vez)
		$id_clarify = time."_".$$; # segundo unix mas el numero de proceso
	}
	#2ble funcion sirve para las lineas de pdtes, y para identificar como propio o extranio el lock
	return($id_clarify);
}
sub CrearLock(){
	open (LOCK, "> ".$path_lock_editar_pdts);
	print LOCK $id_clarify."\n";
	close(LOCK);
}
sub BorrarLock(){
	unlink($path_lock_editar_pdts);
}
sub ExisteLock(){
	if (-e $path_lock_editar_pdts){
		open (LOCK,"< ".$path_lock_editar_pdts);
		my @lock = <LOCK>;
		close(LOCK);
		if ($lock[0] eq $id_clarify){  # miro la primera linea del lock, si coincide es que es tu propio lock, es decir para esta ejecucion no existe
			return(0);
		}
	}
	return(1);
}
sub LogicaLock(){
	&GenerarID_Clarify(); # esto es borrable, creo
	my $iteracciones_lock=0;
	while(&ExisteLock() and $iteracciones_lock < $iteracciones_lock_max){ # mientras exista lock y las iteracciones no hayan llegado al maximo....
		sleep($segundos_lock); # ... dormimos el tiempo definido por segundos_lock
		$iteracciones_lock++;
	}
	if(ExisteLock()){
		# si has llegado aqui es por que en while saliste por iteracciones 
		BorrarLock();
	}
	CrearLock();
}

####  POR HACER si estan dando de alta algo que ya esta en pendientes (misma ip mismo siteID) debe dar error, y poner "Este site id y esta ip ya estan en pendientes, si quieres modificarlo editalos";
####  PUES LO HACEMOS AQUI DE CUALQUIER MANERA, SI ESO QUE SE MODIFIQUE
sub ComprobarExiste($){
        my $ip = shift; # la ip del equipo a comprobar si existe
	my $siencontrado=0;
	#my @lista_pendiente=`grep -i -w $ip /home/op1/asimov/ncm_clarify/pdtes_NCM_Clarify.list`;
	my @lista_pendiente=`grep -i -w $ip $path_pdts`;
        if (@lista_pendiente){
		$siencontrado=1;
	}

#Esto lo hacemos para una busqueda mas sofisticada
my $servidores;
foreach $servidores (@NCM::SERVIDORES) {
my $host=`grep \"$ip \" /etc/hosts|awk -F \" \" '{print \$2}'`;
my $NodoNCM=NCM_BBDD::checkNodoNCM($servidores,$host,$ip);
#print "NODONCM:$NodoNCM\n";
if ($NodoNCM=~/^./)  {
	$siencontrado=2;
}
}

	#my @lista_total=`grep -i -w $ip /home/clarimov/bbdd/TOTALDATOS.txt`;
	#my @lista_total=`grep -i -w $ip $PATH_TOTAL_CPE`;
	#if (@lista_total){
	#	$siencontrado=2;
	#}
	return($siencontrado);
}

sub AlOutput($){ # escribie en output los datos nuevos o editados por pendientes. YA no comprueba si existe la misma entrada
	my $ref_input = shift;
	my %input = %{$ref_input};
	my @campos = &CamposOrdenFicheroPdts();
	my $aloutput;
	&CrearLock();  # creo el lock momentaneo para que no puedan editar a la vez dos ejecuciones
	foreach(@campos){ # construyo la línea de valores
		$input{$_} = '' unless($input{$_});
		if ($_ eq "FECHA"){
			unless($input{'espendiente'}){ # es nuevo
				$input{$_} = &GenerarFecha();	
			}	
		}
		$aloutput.= $input{$_}.";"; # aqui tengo la linea escrita
	}
	my $error_alta = 0; # guarda el mensjae que se mostrara en index.cgi al dar de alta algo con misma ip y hostname
	my @pdts = &TraerPendientes(); # @pdts son los pendientes leidos del fichero. Este tiene dentro LogicaLock
	if($input{'espendiente'}){
		open (OUT, "> ".$path_pdts);  # aqui se ha borrado pendientes
	}
	else {
		open (OUT, ">>".$path_pdts);
	}
	foreach (@pdts) {
		my @aux = split(/;/,$_);
		if($input{'espendiente'}){  # estan editando una entrada
			if($aux[0] eq $input{'ID_Clarify'}){ # esta es la linea que queremos editar
				my $linea = $_; # como vuelvo a usar $_ , copio valor original
				foreach(@campos){
					$aloutput.= $input{$_}.";";
				}
				# antes comprobaba si la linea es exacta a la que habia (en ese caso no hace nada, ya que han no han modificado nada de lo que habia)    ESTO NO SE HACE ROTO DEBIDO A LOS CARACTERES RAROS DE NOMBRE_ORGANIZACION. Ademas esta comprobacion no tiene sentido, puede ser que quieran ejecutar algo con los mismos datos por que lo que haya cambiado sea la red
				$aloutput=""; # borro lo que antes habia aqui
				foreach (@campos){
					unless($input{$_}){
						$input{$_}="";
					}
					$aloutput.= $input{$_}.";";
				}
				chop($aloutput); # le quito el ultimo ;
				&BorrarLock();
				my @aux = split(/;/,$aloutput);
				$aux[1]=$aux[1]."_RUN"; # le aniado la coletilla _RUN
				$aux[4] = &HTML2String($aux[4]); # le pongo formato original
				$aloutput = join(";",@aux);
				print OUT $aloutput."\n";
			}
			else{ # no es la linea que ha editado, no hago nada
				print OUT $_;
			}
		}
		else{ # es nuevo
			# comprobar si estan dando de alta algo que ya estuviera dado de alta, (mismo hostname misma ip) si no es asi concateno los datos nuevos
			if($_ =~ /$input{'IP_Address'};$input{'Nombre_Equipo'}/){
				$error_alta = "ERROR: ya estaba dado de alta la misma IP y el mismo HOSTNAME";
			}

		}
	}
	unless($input{'espendiente'}){ # es nuevo
		unless($error_alta){ # no tiene error
			chop($aloutput); # le quito el ultimo ;
			# generar la fecha
			print OUT $aloutput."\n"; # concateno
		}
		else{
			return($error_alta);
		}
	}
	close(OUT);
	&BorrarLock();
	return("LA PETICION SE HA PUESTO EN PENDIENTES CORRECTAMENTE");
}

sub QuitarEspaciosExtremos($){
        my $dato = shift;
        while($dato=~/(^\s+|\s+$)/){
                $dato=~s/^\s//;
                $dato=~s/\s+$//;
        }
        return($dato);
}

sub GenerarFecha(){ # genera la fecha de las entradas nuevas
	# ejemplo de fecha 2011-09-14 13:32:53
	my @time = my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	$year += 1900;
	$mon=$mon+1;
	return($year."-".&ForzarDosDigitos($mon)."-".&ForzarDosDigitos($mday)." ".&ForzarDosDigitos($hour).":".&ForzarDosDigitos($min).":".&ForzarDosDigitos($sec));
}

sub ForzarDosDigitos($){ # aniade un 0 al principio de un dato que sea un unico digito
	my $dato = shift;
	if($dato=~/^\d{1}$/){
		$dato="0".$dato;
	}
	return($dato);
}
sub GenerarText($$$$$){ # input tipo text
	my $campo = shift; # el nombre del campo
	my $valor = shift; # valor que llega desde clarify, deberia estar en los seleccionables
	my $prepend = shift;
	my $pend = shift;
	my $append = shift;
	print $prepend if $prepend;
		#~ print "<span  style='float:left ;right:3px;'>";

	$valor = "" unless $valor; # para quitar error en apache, al dar de alta un nuevo
	print $campo."<input type='text' name='".$campo."' value='".$valor."' ";
	print $pend if $pend; 
	print ">";
	    #~ print "</span>";

	print $append if $append;
}

sub GenerarDesplegable($$$$$@){ # input tipo desplegable
	my $campo = shift; # el nombre del campo
	my $valor = shift; # valor que llega desde clarify, deberia estar en los seleccionables
	my $prepend = shift;
	my $pend = shift;
	my $append = shift;
	$valor = "" unless $valor; # para quitar error en apache, al dar de alta un nuevo
	my @opciones = @_;
	print $prepend if $prepend;
	#~ print "<span  style='float:left;'>";
	my $haymatch = 0; # hay match entre las opciones del formulario y lo que me llega
	print $campo."<select name='".$campo."' "; # creo que valdria el valor $_ que esta fuera de esat subrutina, pero por si acaso lo declaro y lo mando
	print $pend if $pend;
	if ($campo eq "Version_SNMP"){ # el unico campo con este onchange
		print "onchange='VisibilidadSnmpV3();'";
	}
	print ">"; # cierro el select
	foreach(@opciones){
	        print "<option value='".$_."' ";
	        if($_ eq $valor){
		        print "selected";
		        $haymatch = 1;
	        }
	        print ">".$_."</option>"; 
	}
	unless($haymatch){ # la opcion que venia no estaba como opcion de desplegable, dejo en blanco el valor para que asi js lo pinte de rojo al ComprobarCampos
	        #~ print "<option value='".$valor."' selected>".$valor."</option>";
	        print "<option value='' selected>".$valor."</option>";
	}
    print "</select>";
    #~ print "</span>";
   	print $append if $append;
	# poner en rojo a posteriori, no me hacia falta por que ahora en onload hago un repaso de los datos. Ademas una vez puesto en asimov.pm me faltaba el array @datos_fichero_entrada
	#~ if(!$haymatch and @datos_fichero_entrada){ # para cambiar a posteriori el color si habia error, si NO es un nuevo y no hay match
		#~ print "<script type='text/javascript'>
			#~ document.formulario.".$campo.".style.color='#ff0000';
		#~ </script>";
	#~ }
	#~ print "<br>";
}

sub DatosParaUnIdDlarify($$){
	my $id_clarify = shift;
	my $ip = shift;
	my @pdts = &TraerPendientes();
	foreach (@pdts){
		my @datos_fichero_entrada = split/;/,$_;
		if(($datos_fichero_entrada[0] =~ $id_clarify) and ($datos_fichero_entrada[6] =~ $ip)){ # busco los datos del que ha seleccionado
			chomp(@datos_fichero_entrada); # si no pone un enter en el ultimo value de los inputs
			return(@datos_fichero_entrada);
		}
	}
}

sub next($){

  my $siguiente=shift;

  next if($siguiente=~/AgentIP/ || $siguiente=~/----/ || $siguiente=~/contexto/ || $siguiente=~/afectadas/ || $siguiente=~/context/ || $siguiente!~s/ //g);
}#Cierra sub next($)

sub ArrayORGSIDClienteGP(){
# devuelve un array con los ORGSID de los clientes que son GP
	my $path_csv = "/home/clarimov/listados_clientes/lista_clientes_gp.csv";
	open (CSV, $path_csv);
	my @orgids;
	while(<CSV>){
		my ($nombre,$orgid) = split(/;/,$_);
		push(@orgids,$orgid);
	}
	close (CSV);
	return(@orgids);
}

sub ConstruirTabla($$){
	# construye las tablas que aparecen en 15 dias sin backup
        my $gponogp = shift; ## determina si se trata de GP o noGP
        my $tiempo = shift;  ## determina si es 15 dias o "nunca han respondido
	my @orgidsGP = &ArrayORGSIDClienteGP();
	my (%ordenadorYdato) = &Traer15DiasSinSnmp_Ficheros();

        my $tam = (keys (%ordenadorYdato));
        $gponogp = uc($gponogp); ## acepta gp o Gp o gP o GP, como manera de determinar que es un GP
        $gponogp =~ s/\s+//g;   # fuera espacios

        my $tabla;

        my $parimpar=0;
	print "numero TOTAL=".$tam."<br>";
	print "TIPO CLIENTE=".$gponogp."<br>";
        #print "Encontradas estas maquinas con datos:<br>";
        $tabla .= "<table  id='latabla' border='1'>";
        $tabla .= "<thead><tr><th>Nombre del Cliente</th><th>ORGID</th><th>Router</th><th>IP</th><th>Timestamp ultimo inventario   </th><th> Instancia Solarwinds </th> </tr> </thead>";
	$tabla .= "<tbody>";
        foreach (keys (%ordenadorYdato)){
                my $ncm = $_;
                my @datos = @{$ordenadorYdato{$ncm}};
        #       $tabla .= "<a href='#' onclick='javascript:verdatos15dias(".$_.");'><br />".$_."</a><div id='".$_."' style='display:none;margin-left:2em;'>";

                foreach(@datos){
                        my ($nombre_cliente,$orgid,$router,$ip,$timestamp) = split(/;/,$_);
#                       $tabla.="<td>tiempo".$tiempo."XXX timestamp".$timestamp."</td>";
#                       next;
                        next if ($tiempo eq "15" and $timestamp =~/^1899/);   # quieren los de 15 dias 
                        next if ($tiempo ne "15" and $timestamp !~/^1899/);   # quieren los de NUNCA
			########  filtro los que sean (o no) GP
			########### PENSAR ESTO
			if($gponogp eq "GP"){ # quiere GP
				next unless  grep (/^$orgid$/,@orgidsGP);
			}
			elsif ($gponogp eq "NOGP") {
				next if  grep (/^$orgid$/,@orgidsGP);
			}
			# debo 
			#next unless ($esGP eq "GP" and grep(/^$orgid$/,@orgidsGP));
#			my @esgp = grep (/^$orgid$/,@orgidsGP);
#			next if(grep (/^$orgid$/,@orgidsGP)) and $esGP = "NOGP";
#			next unless(grep (/^$orgid$/,@orgidsGP)) and $esGP = "GP";
##				$orgid = "Es un GP";	
                        &next($_);
                        $tabla.="<tr>";
                        #print &String2HTML($_)."<br>";
                        $tabla.="<td>".$nombre_cliente."</td><td>".$orgid."</td><td>".$router."</td><td>".$ip."</td><td>".$timestamp."</td><td>".$ncm."</td>";
                        $tabla.="</tr>";
                }
                $parimpar++;

#                 my $linea=&String2HTML($_);

        }
	$tabla .= "</tbody>";
        $tabla .= "</table>";
        return($tabla);
}



1;
