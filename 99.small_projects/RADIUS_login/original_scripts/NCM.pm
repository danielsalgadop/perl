#!/usr/bin/perl
#  Este modulo contiene las variables gloables y definiciones

package NCM;

use strict;
use warnings;
use POSIX;
use Data::Dumper;
use Authen::Radius;
# use Switch;

#CONSTANTES
## Desc :        Controller returns OK if at any function everything went all right.          
use constant OK  			=> 0;
	
## Desc :        Controller returns ERROR if at any function something wrong happened          
use constant ERROR 			=> 1;
	
## Desc :        Controller returns NOUSERINFILES if any file did not have user information at all.          
use constant NOUSERINFILES  		=> 2;
	
## Desc :        Controller returns NORADIUSDIR if no Radius dir was found at configuration files.          
use constant NORADIUSDIR 		=> 3;
	
## Desc :        Controller returns NORADIUSFILES if no hermes files were found in Radius dir          
use constant NORADIUSFILES		=> 4;

## Desc :        Controller returns NOTENOUGHPARAMETERS if at any function some parameters was missed and their requirement is mandatory.          
use constant NOTENOUGHPARAMETERS 	=> 5;

## Desc :        Controller returns NOSELECTEDFILE if no selected file to open is found.          
use constant NOSELECTEDFILE		=> 6;
	
## Desc :        Controller returns NOFILETOWRITE if no selected file to open is found.          
use constant NOFILETOWRITE		=> 7;

## Desc :        Controller returns UNKNOWNOPERATIONTYPE if no selected file to open is found.          
use constant UNKNOWNOPERATIONTYPE 	=> 8;

## Desc :        Controller returns NORADIUSSERVER if no radius server is found          
use constant NORADIUSSERVER 		=> 9;

## Desc : 		 Flag to control how to manage RADIUSSERVER ( 0 NOT USE | 1 USE IT )
use constant USINGRADIUS 		=> 1;

# Where to get user and password whether radius is not acceptable for validating.
my $path_user			= "/home/op1/asimov/ficheros_configuracion/users";
# Radius Server IP 
my $radius_server		= "10.0.170.46";
#my $radius_server		= "10.0.191.252";
# Radius Server Secret
my $radius_server_secret	= "SA-IGNITE";

#our @SERVIDORES=('QMAD04NCM1','QMAD04NCM2','QMAD04NCM3');
our @SERVIDORES=('QMAD04NCM2','QMAD04NCM3');
our $NCM_MAX_CPE=10000;
our $NCM_WARN_CPE=9000;
our @T_OPERACION=('ALTA_BTCONF','BAJA_BTCONF','ALTA_MANUAL','BAJA_MANUAL');
our $MAX_CAMPOS_FICH=22;
our $MAX_CAMPOS_PDTES=25;

##Varibles respuesta.
our %resp_NCM2Clarify=('OK' => 'EJECUCION_CORRECTA',
                      'ERR01' => 'ERROR_NUMERO_CAMPOS_ERRONEO',
                      'ERR02' => 'ERROR_FABRICANTE_NO_SOPORTADO',
                      'ERR03' => 'ERROR_FABRICANTE_NO_COINCIDE',
                      'ERR04' => 'ERROR_NO_EXISTE_EN_FICHERO_HOSTS',
                      'ERR05' => 'ERROR_ALTA_EXISTE_IP_O_NOMBRE_EQUIPO',
                      'ERR06' => 'ERROR_AUTENTICACION_ACCESO_AL_ROUTER',
                      'ERR07' => 'ERROR_NO_EXISTE_CONECTIVIDAD',
                      'ERR08' => 'ERROR_NO_POSIBLE_DESCARGAR_CONFIGURACION',
                      'ERR09' => 'ERROR_BAJA_NO_EXISTE_EQUIPO',
                      'ERR10' => 'ERROR_CAMPO_IP_VACIO_O_ERRONEO',
                      'ERR11' => 'ERROR_CAMPO_MAL_FORMADO',
                      'ERR13' => 'ERROR_NO_HAY_ESPACIO_EN_LA_PLATAFORMA',
                      'ERR14' => 'ERROR_EQUIPO_YA_EXISTE',
                      'ERR15' => 'ERROR_CLARIFY',
                      'ERR16' => 'ERROR_FALLO_ALTA_BBDD_NCM',
                      'ERR99' => 'ERROR_DESCONOCIDO');
#i El error 15 se produce antes de llegar a nuestro desarrollo. Solo que incluye
# a modo de informacion.

#PATHs varios
our $DIR_T='/home/op1/asimov/ncm_clarify';
our $DIR_WEB='/var/www/html/asimov';
our $DIR_CARGA_CLARIFY_IN='/home/clarimov/in';
our $DIR_CARGA_CLARIFY_OUT='/home/clarimov/out';
our $DIR_CARGA_WEB='/home/op1/asimov/carga_web';
our $DIR_CARGA_MANUAL='/home/op1/asimov/carga_manual';

our $FICH_BLOQ_PDTE="$DIR_T/pdtes_NCM_Clarify.list";
our $FICH_BLOQ_PDTE_LOCK="/tmp/pdtes_NCM_Clarify.lock";
#Comprobada conectividad/acceso a falta de comprobar estado en Solarwind
our $FICH_ALTA_PDTE="$DIR_T/pdtes_ALTA_NCM.list";
our $FICH_BLOQ_ALTA_PDTE_LOCK="/tmp/pdtes_ALTA_NCM.lock";
our $FICH_LOG="$DIR_T/NCM_CLARIFY.log";
our $FICH_REG_ACCESO="$DIR_WEB/registro_borra_solarwinds.txt";

#Protocolos
our @PROTO_TRANS=('telnet','tftp','ssh');

#Fabricantes
our @FABRI_SOP=('Cisco','Teldat','Juniper');

#Autenticacion
our $RAD_CPE_USR='scriptscsc';
our $RAD_CPE_PWD='scripts482';
our $TACACS_USR='scriptsgsat';
our $TACACS_PWD='gTYV4R64';
our $BT_USR='bt';


#Conexion BBDD
our $SYS_USR='Administrador';
our $SYS_PWD='Lu55ddp';
our $BBDD_USR='SolarWindsCM';
our $BBDD_PWD='Lu55ddp';
our %connBBDD=(#'QMAD04NCM1' => 'SQLNCM1',
               'QMAD04NCM2' => 'SQLNCM2',
               'QMAD04NCM3' => 'SQLNCM3');

our %connALTABBDD=(#'QMAD04NCM1' => 'SQLALTANCM1',
                   'QMAD04NCM2' => 'SQLALTANCM2',
                   'QMAD04NCM3' => 'SQLALTANCM3');

###########################################################################
#### FUNCIONES ############################################################
###########################################################################

### resetRegNodo ##########################################################
sub resetRegNodo() {
  # Inicializa el resgistro de un nodo.
  my %Nodo=('NOMBRE' => '', 		#Hostname equipo
         'IP' => '',			#IP Gestion
         'CLIENTE' => '',		#Nombre Cliente
         'ORGID' => '',			#Codigo Organizacion
         'COD3C' => '',			#Codigo 3 de cliente
         'SITEID' => '',		#Site ID
         'CLARIFYID' => '',		# Cod Clarify y/o manual
         'OPERACION' => '',		# T.Operacion: ALTA_BTCONF, etc.
         'COMMRO' => 'cpecwr99',	# Community Lectura
         'COMMRW' => '',		# Community Escritura
         'SNMP' => 'SNMP V2',		# Version SNMP
         'USRSNMPV3' => '',		# Usr. SNMP V.3
         'PWDSNMPV3' => '',		# Clave SNMP V.3
         'TAUTHSNMPV3' => '',		# Tipo Auth SNMP V.3
         'TENCRIPPROTOSNMPV3' => '',	# Tipo Encriptacion Protocolo  SNMP V.3
         'AUTHPASSSNMPV3' => '',	#   SNMP V.3
         'PRIVPASSSNMPV3' => '',	#   SNMP V.3
         'NIVELSEGSNMPV3' => '',	#   SNMP V.3
         'CONTEXTSNMPV3' => '',		#   SNMP V.3
         'AUTHMODE' => '',		# Modo Auth.: RADIUS, TACACS, BT,etc
         'VENDOR' => '',		# Fabricante
         'MACHINETYPE' => '',		# Tipo CPE. Igual que Vendor.
         'TIPOACCESO' => '',		# Metodo descarga config.: TFTP,etc. 
         'USR' => '',			# Usuario acceso al equipo
         'PWD' => '',			# Clave acceso
         'ENABLEPWD' => '',		# Clave acceso enable
         'ENABLELEVEL' => '',		# Vacio para equipos no Cisco.
         'FECHA' => '',		        # Fecha llegada registro
         'CODRESPUESTA' => '',         # Codigo respuesta operacion
	 'Respuesta_Operacion' => '');
  return(%Nodo);
};
### resetRegNodo ##########################################################

### fechaActual ###########################################################
sub fechaActual() {
  # Devuelve la fecha actual. 
  my $fecha=strftime('%Y-%m-%d %H:%M:%S',localtime());
  chomp($fecha);
  return($fecha);
}
### fechaActual ###########################################################
### fechaActualFich #######################################################
sub fechaActualFich() {
  # Devuelve la fecha actual en el formato de las cabeceras.. 
  my $fecha=strftime('%Y%m%d%H%M%S',localtime());
  chomp($fecha);
  return($fecha);
}
### fechaActualFich #######################################################
### diaActual #############################################################
sub diaActual() {
  my $dia=strftime('%Y%m%d',localtime());
  chomp($dia);
  return($dia);
}
### diaActual #############################################################

### quitaEspaciosExtremos #################################################
sub quitaEspaciosExtremos($){
        my $dato = shift;
        while($dato=~/(^\s+|\s+$)/){
                $dato=~s/^\s//;
                $dato=~s/\s+$//;
        }
        return($dato);
}
### quitaEspaciosExtremos #################################################

### estaVacio #############################################################
sub EstaVacio($){

  my $dato = shift;
  my $resultado=0;
  
  if($dato)
    { if ($dato =~ /^\s*$/)
        { $resultado=1; };
    }
  else
    { $resultado=1; } ; # La variable est� indefinida=vacia
  return($resultado);  
}
### estaVacio #############################################################

### generaID ##############################################################
# Crea un identificador unico.
sub generaID_Clarify(){

  my $ID=time."_".$$; # segundo unix mas el numero de proceso;
  
  #2ble funcion sirve para las lineas de pdtes, y para identificar como propio o extranio el lock
  return($ID);
}
### generaID ##############################################################

### opPendiente ###########################################################
# Vale 1 si hay algun alta o baja para procesar.
sub opPendiente(%){

  my %cabF=@_;
  my $resultado=0;
  
  print "Dentro de opPendiente\n";
  print Dumper(%cabF);
  if(($cabF{ALTAS}>0)||($cabF{BAJAS}>0))
    { $resultado=1}; 
  
  return($resultado);
}
### opPendiente ###########################################################


### obtCod3 ############################################################### 
# --- Funcion para sacar las letras del codigo 3 del cliente. --- #
sub obtCod3($){

  my $codtres=shift;

  if($codtres=~/_/)
    { $codtres=substr($codtres,3,3); }
  else{ $codtres=substr($codtres,0,3);  }
  return($codtres);
}
### obtCod3 ###############################################################

###############< Read a text file >###################################
######################################################################
## Function      : readFile()					    ##
## Params        : arg_file : File to read                          ##
## 		   arg_file_type : Kind of file to read		    ##
##		   arg_matched_pair : Array with pair key=value	    ##
## Desc          : Read a file and return the information needed    ##
##                to parse					    ##
## Return        : Associative array. with statusCode and result.   ##
######################################################################    
sub readFile
{
	my ($vsChoosenFile,$vsFileType,@vaFieldsToRead) = @_;
	my @sFileLine;
	my %aReturn;	

  if ($vsChoosenFile eq "user")	
	{ 
    $vsChoosenFile = $path_user;
  }
	else
  {
     %aReturn = ("statusCode"=>NOSELECTEDFILE,"result"=>undef) and return %aReturn;
	}
	
	open(INPUT,"<$vsChoosenFile") or ( %aReturn = ("StatusCode"=>NOSELECTEDFILE,"result"=>"No selected file $vsChoosenFile") and return %aReturn );
	
	# TODO : Rewrite as a function...
  if ($vsFileType eq "CSV")
	{
		while(<INPUT>)
		{
		  chomp;
			@sFileLine = split(/;/,$_);
			$aReturn{$sFileLine[$vaFieldsToRead[0]]}	= $sFileLine[$vaFieldsToRead[1]];				
		}
  }
  elsif ($vsFileType eq "MCSV")
  {
    while(<INPUT>)
    {
      chomp;
      @sFileLine = split(/;/,$_);
      my @aFieldLine;
      foreach my $iField ( @vaFieldsToRead )
      {
        push(@aFieldLine,$sFileLine[$iField]);
      } 
      @{$aReturn{$sFileLine[$vaFieldsToRead[0]]}} = @aFieldLine;        
    }
  }
  elsif ($vsFileType eq "TXT")
	{							
		while(<INPUT>)
		{
			chomp;				
			$aReturn{$_}	= $_;				
		}			
		close(INPUT);						
  }		
	else
  {
    %aReturn = ("statusCode"=>NOTENOUGHPARAMETERS,"result"=>undef) and return %aReturn;
  }
	close(INPUT);
						
	my %aFinalReturn = ("statusCode"=>OK,"result"=> \%aReturn );			
	return(%aFinalReturn);
	
} 

###############< Validate from a Radius Server >######################
######################################################################
## Function      : checkUserRadiusServer()			    ##
## Params        : $vsUser : User To Read                           ##
##		   $vsKey  : Key To Check	    		    ##
##		   $vsGroup : Group to Check. Future Use	    ##
## Desc          : Get User Info from some files in Radius Dir      ##
## Return        : Associative array. with statusCode and result.   ##
######################################################################    
sub checkUserRadiusServer
{	
	my($vsUser,$vsKey,$vsGroup,@aStuff)= @_;
	my %aReturn;
	my $hRadiusObject 		   = new Authen::Radius(Host => $radius_server, Secret => $radius_server_secret, LocalAddr=>$ENV{'SERVER_NAME'});
	if(!$hRadiusObject)
	{
		%aReturn = ( "statusCode",NORADIUSSERVER,"result","No Radius Server available. Radius library returns :". Authen::Radius::strerror)
	}
	else
	{
		#Authen::Radius->load_dictionary();
  		if($hRadiusObject->check_pwd($vsUser,$vsKey))
  		{
  			%aReturn = ( "statusCode",OK,"result","Valid user for Radius Server")		
  		}
  		else
  		{
  			%aReturn = ( "statusCode",ERROR,"result","No valid user for Radius Server. Server returns :". Authen::Radius::strerror)  		
  		}	
		
	}	
	return %aReturn;		  	  	  	  
}

### sacaContrasena ########################################################
# --- Funcion que saca la contrasena enable del equipo dependiendo del fabricante. --- #
sub sacaContrasena($$){

  my $equipo=shift;
  my $fabricante=shift;
  my $contrasena="";
  $equipo=lc($equipo);

  my $complemento="";

  if($fabricante eq "cisco")
    { $complemento="rcis0";}
  elsif($fabricante eq "teldat")
    { $complemento="rtel0"; }
  elsif($fabricante eq "juniper")
    { $complemento="rjun0";};

  if ($contrasena ne "")
    { $contrasena=obtCod3($equipo) . $complemento; };

  return($contrasena);
}
### sacaContrasena ########################################################



### NoBorrar ####################################3
# Determina cuales errores no pueden ser borrados desde la web de listapendientes

sub ArrayErroresNoBorrables(){
	my @NoBorrar = ("ERR02","ERR14");
	return(@NoBorrar)
}



### NoBorrar ####################################3

1; #Devolvemos true al terminar el modulo
