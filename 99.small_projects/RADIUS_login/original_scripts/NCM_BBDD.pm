#!/usr/bin/perl

package NCM_BBDD;

use strict;
use NCM;
use NCM_fich;
use DBI;
use DBD::ODBC;
use Data::Dumper;

my $DBH_NCM;               #Handle conexion BBDD del NCM (ConfigMgmt)
my $DBH_ALTA_CPE;          #Handle de la conexion a la base de datos
my $STH_ALTA_CPE;          #ALTA_CPE statement handle
my $STH_BAJA_CPE;          #BAJA_CPE statement handle
my $STH_BAJA_ALTA_CPE;     #statement handle para borrar de la BBDD alta_c

sub conectarBBDD($$$) {

   my $DSN=shift;
   my $USER=shift;
   my $PASSWORD=shift;
   my $DBH;
   ##Abriendo la conexion con la base de datos alta_cpe
   #print "dbi:ODBC:$DSN, $USER, $PASSWORD\n";
   $DBH = DBI->connect("dbi:ODBC:$DSN", $USER, $PASSWORD,
    { PrintError => 0, RaiseError => 0 });
   if ($DBH) 
     { #Se comprueba que el HANDLE $DBH no sea undef para continuar
        NCM_fich::escribeLOG("Abiendo conexion con la base de datos($DSN).\n");
     }
   else
     { NCM_fich::escribeLOG("ERROR: No se puede conectar con la BBDD($DSN) \n");
       #print "DBIERR: $DBI::errstr\n";
     };
   return($DBH);
};

### numNodosBBDD ##########################################################
sub numNodosNCM ($) {
  # Devuelve el numero de Nodos en la BBDD del NCM. -1 si hubo algun error.
  my $servidor=shift;
  my $DBH;
  my $STH;
  my $numNodos=-1;
  my @DATOS;

#  print "\nFALLA AQUI numNodosNCM: -$servidor\n";
  print "ANTES DE CONECTAR AL SERVIDOR: $servidor\n";
  print "$NCM::connBBDD{$servidor},$NCM::BBDD_USR,$NCM::BBDD_PWD\n";
  #$DBH = DBI->connect("dbi:ODBC:$NCM::connBBDD{$servidor}","$NCM::BBDD_USR","$NCM::BBDD_PWD");
  $DBH=conectarBBDD("$NCM::connBBDD{$servidor}","$NCM::BBDD_USR","$NCM::BBDD_PWD");
  if (defined $DBH)
    {
      print "HEMOS CONECTADO AL SERVIDOR: $servidor\n";
      $STH = $DBH->prepare('SELECT COUNT(*) FROM dbo.Nodes;');
      $DBH->{RaiseError} = 1;
      $numNodos=$STH->execute();
      #while ( @DATOS=$STH->fetchrow_array())
      @DATOS=$STH->fetchrow_array();
      { print "DATOS: @DATOS\n";
          NCM_fich::escribeLOG("DATOS: @DATOS");
          print Dumper(@DATOS);
       };
      $STH->finish();
      $DBH->disconnect;
      $numNodos=$DATOS[0];
      print "N: $numNodos\n";
      print Dumper(@DATOS);
    }
  else
    { print "ERROR: NO HEMOS CONECTADO AL SERVIDOR: $servidor\n";
      NCM_fich::escribeLOG("ERROR: No se puede conectar con la BBDD del Servidor $servidor \n");
    };
  return($numNodos);
}
### numNodosBBDD ##########################################################

### checkNodoBBDD ##########################################################
sub checkNodoNCM ($$$) {
  # Devuelve el numero de Nodos en la BBDD del NCM. -1 si hubo algun error.
  my $servidor=shift;
  my $NodeCaption=shift;
  my $AgentIP=shift;
  my $DBH;
  my $STH;
  my $Nodo;
  my @DATOS;

  #print "\nFALLA AQUI checkNodoNCM: -$servidor-\n";
  #print "ANTES DE CONECTAR AL SERVIDOR: $servidor\n";
  #$DBH = DBI->connect("dbi:ODBC:$NCM::connBBDD{$servidor}","$NCM::BBDD_USR","$NCM::BBDD_PWD");
  $DBH=conectarBBDD("$NCM::connBBDD{$servidor}","$NCM::BBDD_USR","$NCM::BBDD_PWD");
  if (defined $DBH)
    {
      #print "HEMOS CONECTADO AL SERVIDOR: $servidor y buscamos el nodo:***$NodeCaption***\n";
      #my $query="\'SELECT NodeCaption,AgentIP FROM dbo.Nodes where NodeCaption like '$NodeCaption';\'";
      #$STH = $DBH->prepare("SELECT NodeCaption,AgentIP FROM dbo.Nodes where NodeCaption like '%AVSS54759901%';");
      $STH = $DBH->prepare("SELECT NodeCaption,AgentIP FROM dbo.Nodes where NodeCaption like '$NodeCaption' or AgentIP like '$AgentIP';");
      $DBH->{RaiseError} = 1;
      $Nodo=$STH->execute();
      @DATOS=$STH->fetchrow_array();
       #print "DATOS: @DATOS\n";
          #NCM_fich::escribeLOG("DATOS: @DATOS");
          #print Dumper(@DATOS);
      
      $STH->finish();
      $DBH->disconnect;
      $Nodo=$DATOS[0];
      #print "N: $Nodo\n";
      #print Dumper(@DATOS);
    }
  else
    { print "ERROR: NO HEMOS CONECTADO AL SERVIDOR: $servidor\n";
      NCM_fich::escribeLOG("ERROR: No se puede conectar con la BBDD del Servidor $servidor \n");
    };
  return($Nodo);
}
### checkNodoBBDD ##########################################################



1; #Devolvemos true al terminar el modulo
