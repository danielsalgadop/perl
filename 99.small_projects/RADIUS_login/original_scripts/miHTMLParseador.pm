#!/usr/bin/perl
use warnings;
use strict;
use utf8;
#use diagnostics;
use Data::Dumper; 
#my $input= ':"$%adsasdfasdf';
#if(@ARGV){
#	$input="$ARGV[0]";
#}
#print " esto es input ".$input."\n";
#my $htmli = &String2HTML($input);
#print " esto es en html =".$htmli."\n";
#my $input2 = &HTML2String($htmli);
#print " esto es de nuevo input2 =".$input2."\n";
# --- Recibe un String en HTMl (ej &#33;&#44;) y devuelve ascii (rt)
sub HTML2String($){
	my $string =shift;
	my $out="";
	my @ads = split("",$string); # ads = array de string
	my $tam_string = length($string);
#	my $i=0; # si lo defino aqui me da error "Useless use of private variable in void iext"
	for(my $i = 0;$i<$tam_string;$i++){
		my $aux = $ads[$i];	
		if($ads[$i] eq "&" && $i < $tam_string -3){ # comprobar que no estamos en los ultimos 3 caracteres del string
			$aux = $ads[$i];  # $ads[$i] es UN caracter del array del String 	
			$i++;
			if($ads[$i] eq "#"){ # ya se que hay estos dos caracteres &# 
				$aux .= $ads[$i]; # guardo #
				$i++;
				while($ads[$i] =~m/[0-9]/ and length($aux)<6 and $i < $tam_string){ 
					$aux .= $ads[$i];
					$i++;		
				}
				if($ads[$i] eq ";" and $ads[$i-1]=~m/[0-9]/){
					$aux = &HTML2Char($aux);
				}
			}
		}
		$out.=$aux;
	}
	return($out);
}
sub String2HTML($){
	my $string =shift;
	my $out;
	my @aux = split ("",$string);
	foreach my $uncar(@aux){
		my $enhtml;
		if($uncar =~m/[a-zA-Z0-9]/){
			$enhtml = $uncar;  # realmente este enhtml, no esta en html
		}else{
			$enhtml = &Char2HTML($uncar);
		}
		$out.=$enhtml;
		#~ print "ANTES= ".$uncar." DESPUES= ".$enhtml."\n";
	}
	#return("todo igualllll");
	return($out);
}
sub HTML2Char($){
	my $in = shift;
	$in =~ s/&#//;
	$in =~ s/;//;
	my $carac = chr($in);
	return($carac);
}
sub Char2HTML($){
	my $in = shift;
	my $num_ascii = ord($in);
	return("&#".$num_ascii.";");
}
sub CampoAsteriscos($){
	my $string =shift;
	my $out;
	if($string){
		my @aux = split ("",$string);
		foreach my $uncar(@aux){
			my $asterisco;
				$uncar=~s/$uncar/\*/;
				$asterisco=$uncar;
			$out.=$asterisco;
		}
	}
	return($out);
}
(1)
