#!/usr/bin/perl
use strict 'vars';
use Data::Dumper;
use CGI qw(:standard);
use lib 'lib';
use asimov;
use cgi;
use URI::Escape;
use lib '/home/op1/asimov/ncm_clarify';
use NCM;

my @pdts = &TraerPendientes();

my (%input);
&ReadParse(\%input);

#my @NoBorrar=("ERR02","ERR14");    # esto deberia estar en una libreria mas general como NCM
my @NoBorrar=NCM::ArrayErroresNoBorrables;

print "Content-type: text/html\n\n";

if($input{'identifier'} ne "" && $input{'password'} ne "" && $input{'user'} ne ""){
  open(ABRE,"valida_asimov.txt");
    my @Abre=<ABRE>;
  close(ABRE);

  my $tiempo=localtime();
  chomp($tiempo);

  # Check Radius access an validating. 
  my $bContinue 	= NCM::ERROR;
  my $sUserName		= $input{'user'};
  my $sUserPassword 	= $input{'password'};
  #############################################################		
  # Validating user. 					      #
  #############################################################
  if(NCM::USINGRADIUS)
  {
	# Radius validating
	my %aRadiusReturn = &NCM::checkUserRadiusServer($sUserName,$sUserPassword,"");
	$bContinue = $aRadiusReturn{"statusCode"};
        system("echo \"bContinue:$bContinue if\" >> /tmp/bcontinue.tmp");
  }
  else
  {
	# Radius checking is not active. We must check user text file instead.
	my %aTextUserReturn=&NCM::readFile("user","CSV",(0,1));
	if($aTextUserReturn{"statusCode"} eq NCM::OK)
	{			
    	  my %aUserInfo	= %{$aTextUserReturn{"result"}};    	  
    	  foreach my $sUserNameIndex(sort keys %aUserInfo)
     	  {
    		if($sUserNameIndex eq $sUserName && $aUserInfo{$sUserNameIndex} eq $sUserPassword)
    		{
    			# Continue with cgi as everything was all right.
			$bContinue 	= NCM::OK;
                        system("echo \"bContinue:$bContinue else\" >> /tmp/bcontinue.tmp");
    			last;
    		}    		
    	  } 				         
	}	
  }

  ##if(grep/$input{'user'};$input{'password'};/,@Abre)
  #$bContinue = NCM::OK;
  if($bContinue eq NCM::OK)
  {
    system("echo \"bContinue = OK\" >> /tmp/bcontinue.tmp");
    my @Busca=grep /$input{'identifier'};/,@pdts; # Busca el equipo a dar de baja en Pendientes.
 
    # --- Si lo encuentra, se borra del archivo. --- #
    if(@Busca){
      if($#Busca>0){
	@Busca=grep /$input{'ip'};/,@pdts; # Busca el equipo a dar de baja en Pendientes.
      }
      chomp($Busca[0]);
      print "BUSCA: @Busca\n";
      system("echo \"Busca = $Busca[0]\" >> /tmp/bcontinue.tmp");

      my $contador=0;

      while(($bContinue eq NCM::OK) && ($contador le $#NoBorrar)){
        if($Busca[0]=~/;$NoBorrar[$contador];/){
          system("echo \"Busca $NoBorrar[$contador] if NOBORRAR\" >> /tmp/bcontinue.tmp");
          $bContinue=NCM::ERROR;
          print "NOBORRAR;$input{'identifier'}";
        }
        $contador++;
      }

      if($bContinue eq NCM::OK){
      
        system("echo \"Busca else NOBORRAR\" >> /tmp/bcontinue.tmp");
        #my $ruta="/home/op1/asimov/ncm_clarify";
        my $ruta=$NCM::DIR_T;
        #open(ABRE,"$ruta/pdtes_NCM_Clarify.list");
        open(ABRE,"$NCM::FICH_BLOQ_PDTE");
          my @Abre=<ABRE>;
        close(ABRE);

        #open(CAMBIA,"> $ruta/pdtes_NCM_Clarify.list");
        open(CAMBIA,"> $NCM::FICH_BLOQ_PDTE");
          foreach my $cambia (@Abre){
            chomp($cambia);

            if($cambia!~/^$input{'identifier'};/){
              print CAMBIA "$cambia\n";
              #print "No se borra: $cambia<br>";
            }
            elsif($cambia!~/$input{'ip'};/){
		print CAMBIA "$cambia\n";
              	#print "No se borra: $cambia<br>";
	    }
	    else{
                #  print "Se va a borrar: $cambia<br>";
	        system("echo \"Lo que se borra: $cambia\" >> /tmp/bcontinue.tmp");
            }
          }#Cierra foreach my $cambia (@Abre)
        close(CAMBIA);

        #system("echo \"$input{'user'};$input{'password'};$tiempo\" >> registro_borra_solarwinds.txt");
        system("echo \"$input{'user'};$input{'password'};$tiempo\" >> $NCM::FICH_REG_ACCESO");
        print "OK;$input{'identifier'}";
      }
    }#Cierra if(@Busca)

  }
  else{
    print "ERROR;$input{'identifier'}";
  }
}#Cierra if($input{'identifier'} ne "" && $input{'password'} ne "" && $input{'user'} ne "")
