#!/usr/bin/perl -w
# going to imitate List::Utils qw(shuffler)
use strict;
use Data::Dumper;
use v5.20;
########### global variables
my @array = qw(uno dos tres cuatro cinco seis);

# uses the "randomness" inside 'values'
# IT IS NOT random it is determined by the way perl stores structures in memory
# TODO design test to determine how "random" this is.
# From http://perldoc.perl.org/functions/values.html "Hash entries are returned in an apparently random order"
sub simpleMiShuffler {
    my @arr = @_;
    my %hash;

    my $pos = 0;
    foreach my $element (@array) {
        $hash{$pos} = $element;
        $pos++;
    }
    return values %hash;    # se basa en el "random" intrinseco a este values
}

# recibe un rango (min,max) devuelve int elegido al azar entre el rango
sub randRange {
    my ( $min, $max ) = @_;
    my $range = abs( $max - $min );
    $range++;
    return int( rand($range) ) + $min;
}

# construye un hash intermedio, donde los indices son las posiciones iniciales
sub miShuffler {
    my @original = @_;
    my @shuffled;
    my %hash;
    my %hash_shuffled;

    # build a hash with the values eq to values of array
    # and keys are position of values in array
    my $pos = 0;
    foreach my $element (@original) {
        $hash{$pos} = $element;
        $pos++;
    }

    %hash_shuffled = %hash;

    # recorrer hash e ir construyendo %hash_shuffled
    for my $pos ( keys %hash ) {
        my $rand_pos = randRange( 0, ( "" . @original ) - 1 );

        # swap values
        my $actual_value = $hash_shuffled{$pos};
        my $new_value    = $hash_shuffled{$rand_pos};

        $hash_shuffled{$pos}      = $new_value;
        $hash_shuffled{$rand_pos} = $actual_value;
    }

    return values %hash_shuffled;
}

# @array = simpleMiShuffler(@array); # simple implementation. No tengo claro que sea 100% al azar
# print Dumper @array;

@array = miShuffler(@array);
print Dumper @array;
# TODO - testo to determine if @array has "uno,dos,tres,cuatro,cinco,seis"