#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;

########################################################################################
# Function: controlPermisos
# - tests for files or archives
# - All permisions (r|w|x) are tested by effective uid/gid
# Receives: 
#			- a hash with paths as keys and values are permissions
#           - permissions can be r|w|x  (case insensitive)
# Returns: hash in first error found
#          	- status  OK|ERROR
#          	- mensaje describing ERROR
#
# Errors detected:
#			- size of values must be {1,3}
#			- values must be (r|w|x)
#			- values can not duplicate permissions, rr or rwr are wrong
#			- files MUST exist
#			- paths with 'r' must be readable
#			- paths with 'w' must be writable
#			- paths with 'x' must be executable
# TODO:
# - test with real uid/gid
########################################################################################
sub controlPermisos{
	my %permisos = @_;
	# print Dumper(%permisos);
	my %return;

	# codigo muy mejorable
	# I've tried to use 1 uniq regex  !~m/^(r{1}|w{1}|x{1}){1,3}$/ (no lo he conseguido)

	##########################
	# Validate Params Received
	##########################
	foreach my $un_permiso_conjunto(values(%permisos)){   # $un_permiso_conjunto might be 'r' or 'rw' or 'rwx' etc...
		# print "un_permiso_conjunto ($un_permiso_conjunto)";
		# SIZE
		if ($un_permiso_conjunto !~ m/^\w{1,3}$/){
			$return{mensaje} = "size of values permissions must be up to 3 elements Reveived ($un_permiso_conjunto)";
		}
		
		# VALUES (r|w|x) and they are not repeated
		my @un_permiso_troceado = split("",$un_permiso_conjunto);
		my %repeated_permissions;
		foreach my $valor_un_permiso(@un_permiso_troceado){
			# $valor_un_permiso = lc($valor_un_permiso);
			if($valor_un_permiso!~m/(r|w|x)/i){
				$return{mensaje} = "Values of Permissions must be combinations of (r|w|x) Received (".join("",@un_permiso_troceado).")";
				last;
			}else{
				# detect repeated permissions
				unless($repeated_permissions{$valor_un_permiso}){
					$repeated_permissions{$valor_un_permiso} = 1;
				}
				else{
					# REPEATED value of permision
					$return{mensaje} = "Values of Permission must not be repeated Received (".join("",@un_permiso_troceado).")";
				}
			}
		}
	}

	# if mensaje exists, it is an ERROR
	unless($return{mensaje}){   # there are no previous errors
	# Launch each subroutine for every path
		foreach my $path (keys(%permisos)){
			unless(-e $path){
				$return{mensaje} = "File $path does not exists";
				last;
			}
			
			# All permisions (r|w|x) are tested by effective uid/gid
			# I tried to use the value as operator (no lo he conseguido). my $test = "-".$file_operator." ".$path; unless ($test) 
			foreach my $valor_un_permiso(split("",$permisos{$path})){
				$valor_un_permiso = lc($valor_un_permiso);
				if($valor_un_permiso eq "r"){
					unless(-r $path){
						$return{mensaje} = "File $path is not readable";
						last;
					}
				}
				if($valor_un_permiso eq "w"){
					unless(-w $path){
						$return{mensaje} = "File $path is not writable";
						last;
					}
				}			
				if($valor_un_permiso eq "x"){
					unless(-x $path){
						$return{mensaje} = "File $path is not executable";
						last;
					}
				}
			}
		}
	}
	$return{status} = ($return{mensaje})?"ERROR":"OK";
	return(%return);
}

###############################################################
# Function: controlPermisosAgregado
# very similar to controlPermisos but it returns hash with ALL results
# Returns
#		- status= 
#			- OK if all permissions are ok
#			- ERROR if one or more permissions are not ok
###############################################################
sub controlPermisosAgregado{
	my %permisos = @_;
	my %return;

	$return{status} = "OK";

	foreach my $path(keys(%permisos)){
		my %un_unico_query;
		$un_unico_query{$path}= $permisos{$path};
		# print $path."\n";
		# next;

		my %r_controlPermisos = controlPermisos(%un_unico_query);

		$return{results}{$path}{status} = $r_controlPermisos{status}; # copy of r_controlPermisos{status}
		$return{results}{$path}{permisos} = $permisos{$path};
		if($return{results}{$path}{status} ne "OK"){
			$return{status}= "ERROR";
			$return{results}{$path}{mensaje} = $r_controlPermisos{mensaje};
		}
	}
	
	return(%return);
}

1;