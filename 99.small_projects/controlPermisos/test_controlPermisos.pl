#!/usr/bin/perl
# Test for controlPermisos (control_permisos.pm)
# Test for controlPermisosAgregado (control_permisos.pm)
use strict;
use warnings;
use Data::Dumper;
use lib '.';
use controlPermisos;
# use Test::More 'no_plan';
use Test::More 'no_plan';

my $path1="/tmp/control_permisos_test1";
my $path2="/tmp/control_permisos_test2";
my $path3="/tmp/control_permisos_test3";
my $path4="/tmp/control_permisos_test4";

my $dir_path1="/tmp/dir1/";

my %permisos;    				# place to query permissions. This is what the user of this function will write
my %r_controlPermisos; 			# place to retreive function controlPermisos response
my %r_controlPermisosAgregado; 	# place to retreive function controlPermisosAgregado response

#############################
# incorrect size of values {1,3}
$permisos{"path/ficticio"}="";
%r_controlPermisos = controlPermisos(%permisos);
ok($r_controlPermisos{status} eq "ERROR", "incorrect size of values {1,3}");
ok($r_controlPermisos{mensaje} =~ /^size of values permissions/, "correct mensaje");

#############################
# incorrect values {1,3}
$permisos{"path/ficticio"}="rw1";
%r_controlPermisos = controlPermisos(%permisos);
ok($r_controlPermisos{status} eq "ERROR", "incorrect values (r|w|x)");
ok($r_controlPermisos{mensaje} =~ /^Values of Permi/, "correct mensaje");

#############################
# repetead values
$permisos{"path/ficticio"}="rr";
%r_controlPermisos = controlPermisos(%permisos);
ok($r_controlPermisos{status} eq "ERROR", "repeated values");
ok($r_controlPermisos{mensaje} =~ /^Values of Permission must not be repeated Received/, "correct mensaje");

#############################
# files dont exist
$permisos{"path/ficticio"}="r";
%r_controlPermisos = controlPermisos(%permisos);
ok($r_controlPermisos{status} eq "ERROR", "repeated values");
ok($r_controlPermisos{mensaje} =~ /does not exists$/, "correct mensaje");

#############################
# very simple test - one file readable and writable
`touch $path1`;     # by default read and write permissions are asigned
undef %permisos;
$permisos{$path1} = "rw";    # path1 must be readable and writable
# TESTS
%r_controlPermisos = controlPermisos(%permisos);
ok($r_controlPermisos{status} eq "OK","very simple test - one file readable and writable" );

#############################
# Multiple files read and write
`touch $path1 && touch $path2 && touch $path3 &&touch $path4`;
undef %permisos;
$permisos{$path1} = "rw";
$permisos{$path2} = "rw";
$permisos{$path3} = "rw";
$permisos{$path4} = "rw";
# TESTS
%r_controlPermisos = controlPermisos(%permisos);
ok($r_controlPermisos{status} eq "OK","Multiple files read and write");

#############################
# One file not executable
`touch $path1`;   # by default not executable
undef %permisos;
$permisos{$path1}= "x";
%r_controlPermisos = controlPermisos(%permisos);
ok($r_controlPermisos{status} eq "ERROR","One file not executable" );
ok($r_controlPermisos{mensaje} =~m/executable/,"detecting mensaje" );
`chmod u+x $path1`;
%r_controlPermisos = controlPermisos(%permisos);
ok($r_controlPermisos{status} eq "OK","Now $path1 IS executable");

############################# function controlPermisosAgregado
`touch $path1 && touch $path2`;
undef %permisos;
$permisos{$path1} = "r";
$permisos{$path2} = "r";
%r_controlPermisosAgregado = controlPermisosAgregado(%permisos);
ok($r_controlPermisosAgregado{status} eq "OK","All paths revised in controlPermisosAgregado are OK");

#############################
# One file OK and one not executable
`touch $path1 && touch $path2`;
undef %permisos;
$permisos{$path1} = "r";
$permisos{$path2} = "x";
%r_controlPermisosAgregado = controlPermisosAgregado(%permisos);
ok($r_controlPermisosAgregado{status} eq "ERROR","One file OK and one not executable");
# print Dumper(%r_controlPermisosAgregado);

#############################
# Testing dir readable|writable|executable
`mkdir $dir_path1`;
undef %permisos;
$permisos{$dir_path1} = "rwx";
%r_controlPermisosAgregado = controlPermisosAgregado(%permisos);
ok($r_controlPermisosAgregado{status} eq "OK","DIR is readable|writable|executable");
# print Dumper(%r_controlPermisosAgregado);

#############################
# DETECTING dir not readable
`chmod u-r $dir_path1`;
$permisos{$dir_path1} = "r";
%r_controlPermisos = controlPermisos(%permisos);
ok( ($r_controlPermisos{status} eq "ERROR" and $r_controlPermisos{mensaje} eq "File ".$dir_path1." is not readable"),"DIR detected NOT readable and correct mensaje");
# print Dumper(%r_controlPermisos);


&limpiarTests;

# removes all paths used in tests
sub limpiarTests{
	`rm -f $path1`;
	`rm -f $path2`;
	`rm -f $path3`;
	`rm -f $path4`;
	`rmdir $dir_path1`;
}
