#! /usr/bin/perl
# Este script es una prueba para ver como se pueden pasar filehandlers a subrutinas
# tarea muy facil quitando 'use strict'
use Data::Dumper;
use strict; # estamos usando 'use strict'
use warnings; # para probar print() on closed filehandle

# Definicion del filehandler
my $FH1 = ">FH1.txt"; # ejemplo que provoca ERROR my $FH1 = ">/root/FH1.txt";
my $FH2 = ">FH2.txt";
my $FH3 = ">FH3.txt";

# Abrir los file handlers
die "no he abierto_1 $FH1\n" unless open (FH1 , $FH1);
die "no he abierto_2 $FH2\n" unless open (FH2 , $FH2);
die "no he abierto_3 $FH3\n" unless open (FH3 , $FH3);

# Pasarlos como argumento
&concatenar(*FH1,*FH2,*FH3);

#
sub concatenar(){  # El Filehandler viene como argumento, usando use strict;
	foreach (@_){
		print $_ "my guitar gently sweeps";
		close $_;   # se puede cerrar el file handler como variable
	}
}
