#!/usr/bin/perl -w
use strict;
#  Package inside package. Artificial relation.Since all packages are global in scope, nesting of packages is not supported. However, you can have two packages, one called A and another called A::B , to give an illusion of nesting.

use package1;
print $package1::var1."\n";
print $package1::package2::var1."\n";  # <<< this is inside package1 (a son)
