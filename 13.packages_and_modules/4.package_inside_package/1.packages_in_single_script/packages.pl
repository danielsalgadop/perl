#!/usr/bin/perl -w
use strict;
#  Package inside package. Dont look much diferent one inside another.

use package1;
print $package1::var1."\n";
print $package2::var1."\n";  # <<< this is inside package1 (a son)
print $package3::var1."\n";	 # <<< this is next to package1 (a brother)