#!/usr/bin/perl -w
package package1;
{
	use strict;
	our $var1 = "value1";
	package package2;
	{
		our $var1 = "value2";
	}
}
package package3;
{
	our $var1 = "value3";
}
1;