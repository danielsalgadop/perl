#!/usr/bin/perl -w
use strict;

package Globals;
{    # <<<<<<<<<<<<<<<<<< WITHOUT this you could use 'my' as scope
	use strict;
	our $var = "value";
	our $app = "perlmvc"; # name
}

package Paths;
{
	use strict;
	our $var = "value2";
	print $Globals::var."\n";  # this variable is defined inside package.pm (the other .pm) you can see it because is 'use' in .pl
	my $path_app = "/usr/bin/myapp";
	our $app = $path_app."/".$Globals::app;   # using 2 variables name 'app' in diferenct name spaces
}

package StrangePackage;
# { # uncomment this to see how the scope changes
	my $this_is_main = "I can access this without ::";
# }

package main;
print $Globals::var."\n"; # this varible must be defined as 'our'
print $Paths::var."\n";

print "paths to app [".$Paths::app."]\n";
print "actual package [".__PACKAGE__."]\n";
print "StrangePackage [".$this_is_main."]\n";