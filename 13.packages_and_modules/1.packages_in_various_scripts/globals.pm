#!/usr/bin/perl -w
package Globals;
use strict;
our $var = "var_value in package [".__PACKAGE__."]";
our $app = "perlmvc"; # name
our @array = qw(array_value1 array_value2 array_value3 array_value4 );
our %hash = 
(
	key1=>"hash_value1",
	key2=>"hash_value2",
	key3=>"hash_value3",
	key4=>"hash_value4"
);
1;