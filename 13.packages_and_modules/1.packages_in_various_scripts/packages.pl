#!/usr/bin/perl -w
# Needed this developing perlmvc
use strict;
#  TODO: understand diference between use and require package;

use globals;
use paths; # inside paths will use variables from globals
print $Globals::var."\n"; # this varible must be defined as 'our'
print $Paths::var."\n";

print "actual package [".__PACKAGE__."]\n";
print "has used globals inside paths [".$Paths::app."]\n";
print "[".$Globals::on_the_fly."]\n";

print "ARRAY - ";
map{print "[".$_."]\t"}@Globals::array;
print "\n";

print "HASH - ";
map{print "[".$_." => ".$Globals::hash{$_}."]\t"}keys %Globals::hash;
print "\n";