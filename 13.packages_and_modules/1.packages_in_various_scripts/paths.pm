#!/usr/bin/perl -w
package Paths;
use strict;
our $var = "var_value in package [".__PACKAGE__."]";
my $path_app = "/usr/bin/myapp";
our $app = $path_app."/".$Globals::app;   # using 2 variables name 'app' in diferenct name spaces
$Globals::on_the_fly = "NOT defined in globals.pm (but has scope in it) defined in paths.pm"; # can not be defined with 'our'
1;