# Switches
## -c 
- check perls syntaxs
## -w
- equivalent to 'use warnings;'
## -e
- executes code in the terminal instead of a script

```perl
perl -e 'print "hello\n";'

```
Will output 'hello'

## -n
- implicityly loop through the file one line at a time (like _sed_ or _awk_)


## -ne
- Switches -n -e combined

```
perl -ne 'print;' input.txt
perl -ne 'print if /333/;' input.txt
```

## Pipes
- Beacuse perl is another program commands can be piped to perl
```
date | perl -ne 'chomp;print "the output is inside \$_[".$_."]\n";'
```

## < file input
```
perl -ne 'print;' < input
```
perl output to another file
```
perl -ne 'print;'  input > output
```