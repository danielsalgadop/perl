#!/usr/bin/perl -w
use strict;
use Data::Dumper;
print "dentroo de stdin.pm";
sub imprimirStdin(){
	my %return;
	$return{'status'} = "OK";
	# imprimir todo lo que llega de stdin
	my $num_lineas = 0;
	my %trap;  # donde almaceno {oid} = valor. Luego se guarda en %return{trap}
	while(<STDIN>){
		my ($oid, $valor) = /([^\s]+)\s+(.*)/;
		$trap{$oid} = $valor;
		$num_lineas ++;
	}
	print Dumper(%trap);
	if($num_lineas == 0){
		# ¿esta vacio el trap?
		$return{status} = "FAIL: Stdin esta vacio, no ha llegado trap alguno";
	}
	else{
		# no esta vacio
		$return{num_lineas} = $num_lineas;
		$return{htrap} = \%trap;
		# meto los valores del trap en key 'trap'
	}
	return(%return);
}
1;