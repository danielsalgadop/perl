#!/usr/bin/perl
use warnings;
use strict;
use Data::Dumper; 
# Yo can use sytem, exec or the ``
# More info 
# perldoc -f exec

# links 
# http://perldoc.perl.org/functions/system.html
# http://perlmaven.com/running-external-programs-from-perl

# system Does exactly the same thing as exec LIST , except that a fork is done first and the parent process waits for the child process to exit
my $response; # script colector
$response = `perl perl_with_response.pl`;
print "script respones is with \` \` [".$response."]\n";

$response = system("perl perl_with_response.pl");
print "<< response whent here, because SYSTEM doesnot colects scritp respones [".$response."]\n";

# Detecting error in a perl script
# link http://chimera.labs.oreilly.com/books/1234000001527/ch12.html

# I cant "grab" error Can't open perl script no_existe.pl: No existe el archivo o el directorio
eval(`perl no_existe.pl`);
if ($@){
	print "ERROR in script";
	exit;  # <<< nunca llego aqui
}

# I cant "grab" error syntax error at error_script.pl
`perl error_script.pl`;
if ($? == -1) {
	print "ERROR in script\n";
	exit;  # <<< nunca llego aqui
}

# Put a & if you dont want to wait
print "con \` \` WAITS AFTER\n";
`perl sleep.pl`;
print "\nscript  \` \` waits BEFORE\n"; 

print "con system AFTER\n";
system("perl sleep.pl");
print "\ncon system BEFORE (SE VE EL CONTENIDO DEL SCRIPT)\n"; 

print "con EXEC AFTER, there is no BEFORE\n";
exec("perl sleep.pl");
# If the scrip puts something in here (except for die or exit or warn) it says (Maybe you meant system() when you said exec()?) )