#!/usr/bin/perl -w
# Perl monguers 20151124


use Data::Dumper;
use strict;

# problema sacado de perlenespañol, para construir contraseñas
# http://perlenespanol.com/foro/post39088.html
# ENUNCIADO Tengo esta IP: 10.113.125.8

# 1- Quitar el primer octeto -> 113.125.8
# 2- Rellenar con 0 los octetos con menos de 3 dígitos -> 113.125.008
# 3- Restar de 9 cada dígito -> 999.999.999 - 113.125.008 = 886.874.991
# 4- Quitar los puntos y darle la vuelta -> 199478688

my $ip = "10.113.125.8";
my ($oc1,$oc2,$oc3,$oc4) = split (/\./,$ip);
print "oc1 [$oc1] oc2 [$oc2] oc3 [$oc3] oc4 [$oc4]\n";
foreach ($oc2,$oc3,$oc4)
{
	 $_ = sprintf('%03d',$_); # <<<<< aprendido hoy, cambias los valores que están DENTRO del array
}
print "oc1 [$oc1] oc2 [$oc2] oc3 [$oc3] oc4 [$oc4]\n";


# las variables lexicas (dentro de bucle) son referencias (alias) al elemento dsel interior del array
# join

use Try::Tiny; # simplifica la escritura del eval

use utf8::all;  # todo el sopoerte de Unicode en perl