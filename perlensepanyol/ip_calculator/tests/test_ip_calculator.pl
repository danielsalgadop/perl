#!/usr/bin/perl
use warnings;
use strict;
use Test::More 'no_plan';
use lib '../';
use ip_calculator;
ok(&ip_calculator("10.0.0.1","d") eq "-1","operation has NO numbers");
ok(&ip_calculator("10.0.0.1","d1") eq "-1","control of operation 1d");
ok(&ip_calculator("10.0.0.1","") eq "-1","control of operation empty");
# print &ip_calculator("10.0.0.1","1")."\n";
isnt(&ip_calculator("10.0.0.1","1"),"-1","1 vALID add operation");
isnt(&ip_calculator("10.0.0.1","+1"),"-1","+1 vALID add operation");
isnt(&ip_calculator("10.0.0.1","1"),"-1","-1 A vALID rest operation");
ok(&ip_calculator("10.0.0.1","+1") eq "10.0.0.2","sumarle 1 a 10.0.0.1 da 10.0.0.2");
ok(&ip_calculator("10.0.0.1","+100") eq "10.0.0.101","sumarle 1 a 10.0.0.1 da 10.0.0.2");
# print &ip_calculator("10.0.0.1","+1");
ok(&ip_calculator("10.0.0.1","-1") eq "10.0.0.0","Restarle 1 a 10.0.0.1 da 10.0.0.0");
ok(&ip_calculator("10.0.0.101","-100") eq "10.0.0.1","Restarle 100 a 10.0.0.101 da 10.0.0.1");


ok(&ip_calculator("10.0.0.255","1") eq "10.0.0.256","ESTO NO DEBEROA SER ASI");
