#!/usr/bin/perl
use warnings;
use strict;
use Data::Dumper;
# http://perlenespanol.com/foro/ayuda-modulo-para-direcciones-ip-t8804.html?utm_source=twitterfeed&utm_medium=twitter
#Hola, la verdad que soy nuevo programando en Perl, ahora estoy haciendo una pequeña aplicación para mi asignatura de programación en Perl que me configure un enrutador.
#Anduve buscando algún módulo que permita sumar a una IP valores ejemplo: 10.0.0.1 sumarle 1 y que quede 10.0.0.2 o restarle 1 y quede 10.0.0.0.
#Se los agradezco de antemano

sub ip_calculator($$){
	my $ip 			= shift;
	my $operation 	= shift;
	# control operation
	return -1 if $operation!~/\d/;
	return -1 if $operation=~/[a-zA-Z]/;
	return -1 if ($operation!~m/^\+\d+$/ and $operation!~m/^-\d+$/ and $operation!~m/\d+$/);
	# At this point operation is ok

	my $type_operation=()?"rest":"add";
	my @octetos = split(/\./,$ip);

	($operation=~m/^-/)?&rest($operation,@octetos):&add($operation,@octetos);

	# this can be done usng functional programing
	sub add($){
		my $value = shift;
		my @octetos = @_;
		$value =~ s/\+//;
		return($octetos[0].".".$octetos[1].".".$octetos[2].".".($octetos[3]+$value) );
	}
	sub rest($){
		my $value = shift;
		my @octetos = @_;
		$value =~ s/\-//;
		return($octetos[0].".".$octetos[1].".".$octetos[2].".".($octetos[3]-$value) );
	}
}
1;