#!/usr/bin/perl -w
# motivacion, hacer pruebas para muchas subrutinas
# hacer todas las posibles IPs (OJO tras 4 minutos he parado el proceso y ya habia calculado 988330 ips (la última ip 0.15.20.168) y el fichero ocupaba 33 megas)
use v5.22;
# copio todos las maneras en las que 'explorer' los ha resuelto. Todas menos el perl6

sub manera1($)
{

    #!/usr/bin/env perl
    use v5.18;
    my $IP = shift; # // '10.113.125.8';    # Leemos la IP desde la entrada estándar
    $IP =~ s/^\d+//;                        # 1- Quitar el primer octeto -> 113.125.8
    $IP =~ s/(\d+)/sprintf "%03d", $1/ge;   # 2- Rellenar con 0 los octetos con menos de 3 dígitos -> 113.125.008
    $IP =~ s/[.]//g;                        # 3- Quitar los puntos
    $IP = 999_999_999 - $IP;                # 4- Restar de 9 cada dígito -> 999.999.999 - 113.125.005 = 886.874.991
    $IP = reverse $IP;                      # 5- y darle la vuelta
    return $IP;                             # 199478688
}


sub manera2($)
{
	use v5.18;
	my $IP = shift; # // '10.113.125.8';    # leemos la IP desde la entrada
	my @IP = split /[.]/, $IP;              # partimos la IP
	@IP = map { 999 - $_ } @IP;             # restamos 999 - octeto   ### Esta manera puede ser re-escrita asi    $_ = 999 - $_ for @IP;
	shift @IP;                              # quitamos primer octeto
	$IP = join '', @IP;                     # unir
	$IP = reverse $IP;                      # dar la vuelta
	return $IP;
}

sub manera3($)
{
	return scalar reverse join '', map { 999 - $_ } (split /[.]/, shift)[1..3];
}

sub manera4($)
{
	# Hay que dejar de ser perl oneliner
	# perl -E 'say scalar reverse 999999999 - join "", map { substr "00$_", -3 } (split /[.]/, shift)[1..3]' 10.113.125.8

	# en este caso siempre se añade 2 ceros y posterior mente te quedas con los 3 primeros caracteres
	return scalar reverse 999999999 - join "", map { substr "00$_", -3 } (split /[.]/, shift)[1..3]; # 10.113.125.8
}

sub manera5()
{
	# Hay que dejar de ser perl oneliner
	# perl -E 'say scalar reverse "9"x9 - sprintf "%03d"x3, (split /[.]/, shift)[1..3]' 10.113.125.8
}



################ TESTS

use strict;
use Test::More tests => 1;

# my @coleccion_ips = ("0.0.0.0", "10.113.125.8", "2.23.4.235", "3.56.3.6", "255.255.255.255"); # 0.0.0.0
# foreach my $ip (@coleccion_ips)

for my $oct2 (0 .. 255)
{
	for my $oct3 (0 .. 255)
	{
		for my $oct4 (0 .. 255)
		{
			my $ip = "0.".$oct2.".".$oct3.".".$oct4;
			my $response1 = manera1($ip); # lo uso para poner nombre al test
			ok
				(
					( $response1 == manera2($ip) ) &&( $response1 == manera3($ip) ) && ($response1 == manera4($ip) )
					,$ip."=>".$response1
				);
		}
	}
}
