#!/usr/bin/perl
use warnings FATAL=>'all';
use strict;
use lib 'lib';
use HTML::Template;
use Data::Dumper;
# send the obligatory Content-Type and print the template output
print "Content-Type: text/html\n\n";

# Playing with <TMPL_INCLUDE>
# This tag includes a template directly into the current template at the point where the tag is found. The included template contents are used exactly as if its contents were physically included in the master template.

# The file specified can be an absolute path (beginning with a '/' under Unix, for example). If it isn't absolute, the path to the enclosing file is tried first. After that the path in the environment variable HTML_TEMPLATE_ROOT is tried, if it exists. Next, the "path" option is consulted, first as-is and then with HTML_TEMPLATE_ROOT prepended if available. As a final attempt, the filename is passed to open() directly. See below for more information on HTML_TEMPLATE_ROOT and the "path" option to new().

# As a protection against infinitly recursive includes, an arbitary limit of 10 levels deep is imposed. You can alter this limit with the "max_includes" option. See the entry for the "max_includes" option below for more details.

my $template; # class 
##################
# Example 1
# Basic example
# This has to be called in same path from this script
##################
$template = HTML::Template->new(filename => 'mytmpl/template_include.tmpl');
$template->param(VAR => "VALOR VAR");
print $template->output;

##################
# Example 2
# Using HTML_TEMPLATE_ROOT
# to really (executing in otrer path) try this, you must:
# 1) point correctly to path mytmpl
# 2) COMMENT Example 1 (or it will raise error)
##################
$template = HTML::Template->new
(
  path => ['/home/dan/perl/63.HTML_Template/mytmpl/'],
  filename => 'template_include.tmpl',
);
$template->param(VAR => "VALOR VAR");
print $template->output;

# our $HTML_TEMPLATE_ROOT = "/home/dan/perl/63.HTML_Template/mytmpl";