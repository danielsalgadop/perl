#!/usr/bin/perl
use warnings FATAL=>'all';
use strict;
use lib 'lib';
use HTML::Template;
use Data::Dumper;
# send the obligatory Content-Type and print the template output
print "Content-Type: text/html\n\n";

my $template; # class
# Complex use

##################
# Example 1
# Passing html inside the PARAM
##################
$template = HTML::Template->new(filename => 'mytmpl/template_complex.tmpl');
$template->param(HTML=>"<b>this html</b> inside the param <ul><li>list1</li><li>list2</li></ul>");
##################
# Example 2
# opitional tag 'NAME='
##################
$template->param(NO_TAG_NAME=>"In template REALLY the tag NAME= is optional, although for extensibility's sake the author recommends using it");
##################
# Example 3
# PARMS are case insentitive
##################
$template->param(CasE_InSeNsItIvE=>"key is case insentitive");
##################
# Example 4
# Strange html can be created and works
##################
$template->param(COLOR=>"red");
##################
# Example 5
# How to confort VALID HTML
# Puting it in comments <!-- TMPL_VAR NAME=PARAM1 -->
##################
$template->param(VALID_HTML=>"valid html (in html comment)");
print $template->output;