#!/usr/bin/perl
use warnings FATAL=>'all';
use strict;
use lib 'lib';
use HTML::Template;
use Data::Dumper;
# send the obligatory Content-Type and print the template output
print "Content-Type: text/html\n\n";
print "<html><body>";
# Playing with <TMPL_LOOP>

my $template; # class 
##################
# Example 1
##################
$template = HTML::Template->new(filename => 'mytmpl/template4.tmpl');
$template->param
(
	REF_ARRAY => 
		[ 
			{ name => 'Sam', job => 'programmer' },
			{ name => 'Steve', job => 'soda jerk' },
		]
);

##################
# Example 2
##################
# Creating a LOOP programmatically
# a couple of arrays of data to put in a loop:
my @words = qw(I Am Cool);
my @numbers = qw(1 2 3);

my @loop_data = ();  # initialize an array to hold your loop

while (@words and @numbers) {
 my %row_data;  # get a fresh hash for the row data

 # fill in this row
 $row_data{WORD} = shift @words;
 $row_data{NUMBER} = shift @numbers;

 # the crucial step - push a reference to this row into the loop!
 push(@loop_data, \%row_data);
}
# finally, assign the loop data to the loop param, again with a
# reference:
# print Dumper(@loop_data);
$template->param(THIS_LOOP => \@loop_data);

##################
# Example 3
##################
# LOOP inside another LOOP
$template->param
(
	LOOP => 
	[
    { 
    	name => 'Bobby',
      nicknames => 
      [
      	{ name => 'the big bad wolf' }, 
        { name => 'He-Man' },
      ],
    },
  ],
);
$template->param(THIS_LOOP => \@loop_data);
print $template->output;

# <TMPL_LOOP> introduces a new scope much like a perl subroutine call. If you want your variables to be global you can use 'global_vars' option to new() described below.

print "</body></html>";