#!/usr/bin/perl
use warnings FATAL=>'all';
use strict;
use lib 'lib';
use HTML::Template;
use Data::Dumper;
# send the obligatory Content-Type and print the template output
print "Content-Type: text/html\n\n";

my $template; # class
# Playing with <TMPL_IF>
# The <TMPL_IF> tag allows you to include or not include a block of the template based on the value of a given parameter name.

##################
# Example 1
##################
$template = HTML::Template->new(filename => 'mytmpl/template_if_else_unless.tmpl');
$template->param(BOOL_TRUE=>1);
$template->param(BOOL_FALSE=>0);  # BOOL_FALSE=>'' will work the same
print $template->output;

# <TMPL_IF> </TMPL_IF> blocks can include any valid HTML::Template construct - VARs and LOOPs and other IF/ELSE blocks. BEAWARE that intersecting a <TMPL_IF> and a <TMPL_LOOP> is invalid.
   # Not going to work:
   # <TMPL_IF BOOL>
   #    <TMPL_LOOP SOME_LOOP>
   # </TMPL_IF>
   #    </TMPL_LOOP>

# WARNING: Much of the benefit of HTML::Template is in decoupling your Perl and HTML. If you introduce numerous cases where you have TMPL_IFs and matching Perl if()s, you will create a maintenance problem in keeping the two synchronized. I suggest you adopt the practice of only using TMPL_IF if you can do so without requiring a matching if() in your Perl code.