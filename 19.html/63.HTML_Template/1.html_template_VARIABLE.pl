#!/usr/bin/perl
use warnings FATAL=>'all';
use strict;
use lib 'lib';
use HTML::Template;
use Data::Dumper;
# send the obligatory Content-Type and print the template output
print "Content-Type: text/html\n\n";

# HTML::Template extends simple HTML with this tags <TMPL_VAR>, <TMPL_LOOP>, <TMPL_INCLUDE>, <TMPL_IF>, <TMPL_ELSE> and <TMPL_UNLESS>

my $template; # class 
##################
# Example 1
##################
# open the html template
$template = HTML::Template->new(filename => 'mytmpl/template1.tmpl');
# fill in some parameters
$template->param(HOME => $ENV{HOME});
$template->param(PATH => $ENV{PATH});
# $template->param(INEXISTENTPARAM => "eoeoeo");  # will die !!!
print $template->output;

# You MUST be precise SENDING params
# Sending param not defined in template will "die_on_bad_params => 1"
# But you can be lazy 
# Not sending a param defined in param is OK

print "====================\n<br>";
##################
# Example 2
# Not sending ALL parameters defined in template is OK
##################
$template = HTML::Template->new(filename => 'mytmpl/template2.tmpl');
$template->param(EXISTS => "value_EXISTS");
print $template->output;

##################
# Example 3
# Escaping Characters
##################
$template = HTML::Template->new(filename => 'mytmpl/template3.tmpl');
$template->param(PARAM1 => "uncomfortable Characters  < / > ! · $ % & / ( ) =");
print $template->output;