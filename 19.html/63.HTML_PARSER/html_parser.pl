#!/usr/bin/perl
# he necesitado parsear html al hacer una interfaz curl para manejar SAD
# al dar de alta/baja equipos man.equipos.cgi devuelve un html

# enlaces de interes
# http://perlmeme.org/tutorials/html_parser.html
# http://www.foo.be/docs/tpj/issues/vol5_1/tpj0501-0003.html
# Para que funcione tengo que hacer uso de packages, hacer subclass el modulo
# y sobreescribir metodos

use warnings;
use strict;

# defines la subclase
package MyParser;
use base qw(HTML::Parser);
use Data::Dumper;

my $flag_text_interesante = 0;    # determina si estamos en texto interesnate

# This parser only looks at opening tags
sub start {
    my ( $self, $tagname, $attr, $attrseq, $origtext ) = @_;
    if ( $tagname eq 'span' ) {
        if ( $attr->{class} =~ /^fondo/ ) {
            $self->{flag_text_interesante} = 1;
        }
    }
}

sub text {
    my ( $self, $text ) = @_;
    if ($self->{flag_text_interesante}) {
        print $text;
        $self->{flag_text_interesante} = 0;
    }
}

package main;
my $parser = MyParser->new;
$parser->parse_file('foo.html');
