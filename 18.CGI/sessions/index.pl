#!/usr/bin/perl
use warnings;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use CGI qw(:standard);
use lib '/home/dan/perl5/lib/perl5/';  # installed via CPAN
use CGI::Session;
use Data::Dumper;

my $q = CGI->new;
# print $q->header();  # cualquier print (aunque sea header imposibilita el redirect)
unless($q->cookie("logeado")){   # la cookie NO existe
        print $q->redirect('login.pl');
}
else{
    # el valor de la cookie debe existir en el id de alguna session
    my $session = new CGI::Session("driver:File", $q->cookie("logeado"), {Directory=>"/tmp/pruebas_session"}) or die CGI::Session->errstr;
    if($session->id() ne $q->cookie("logeado")){   # la cookie era antigua (o generada malintencionadamente)
        print $q->redirect('login.pl');
    }
}
print $q->header();  # cualquier print (aunque sea header imposibilita el redirect)
print $q->h1("INDEX");