#!/usr/bin/perl
use warnings;
use strict;
use CGI::Carp qw(fatalsToBrowser warningsToBrowser);
use CGI qw(:standard);
use CGI::Session;

my $q = CGI->new;

my $html_colector_pre_header = "";
	# # mirar si tiene la cookie logeado
	$html_colector_pre_header .= "cookie logeado ";
	unless ($q->cookie("logeado")){
		# $sid = undef;
		$html_colector_pre_header .= "NO EXISTIA";
	}
	else{
		# $sid = $q->cookie("logeado");
		$html_colector_pre_header .= "SI EXISTIA";
	}
	$html_colector_pre_header .= "<br>";
#
my $sid = $q->cookie("logeado") || undef;    # $sid valdra: o el contenido de cookie (que es el valor del session_id) o 'undef'
my $session = new CGI::Session("driver:File", $sid, {Directory=>"/tmp/pruebas_session"}) or die CGI::Session->errstr;
	if($session->id() eq $q->cookie("logeado")){   # valor coola cookie era antigua (o generada malintencionadamente)
	    print $q->redirect('index.pl');
	}


my $cookie = $q->cookie(-name=>"logeado",-value=>$session->id());
print $q->header(-cookie=>$cookie);
print $q->h1("LOGIN");

# my $esta_logeado = $session->param(-name=>'logeado');
	$html_colector_pre_header .= "session id [".$session->id()."]<br>";
	$html_colector_pre_header .= "valor cookie logeado[".$q->cookie("logeado")."]<br>";

print $html_colector_pre_header;
