#!/usr/bin/perl
use warnings;
use strict;
use Data::Dumper;
use lib '.';
use miSession;
our $q;

my $debug;

my %r_estaLogeado = estaLogeado();
if ($r_estaLogeado{status} eq "ERROR"){
	miSyslog((split('/',$0))[-1]." recibe r_estaLogeado [".$r_estaLogeado{status}."]");
	print $q->redirect('login.pl');
}


print $q->header();
print $q->h1("YA ESTAS LOGEADO INDEX");
print $q->a({-href=>"index2.pl"},"otra pagina");