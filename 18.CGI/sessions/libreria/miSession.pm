#!/usr/bin/perl
# Study this options
# http://cgi-app.org/index.cgi?LoginLogoutExampleApp
# http://www.perlmonks.org/?node_id=698693
#
use warnings;
use strict;
use Data::Dumper;
use CGI::Carp qw(fatalsToBrowser);
use CGI qw(:standard);
use CGI::Session;
use lib '.';
use miSyslog;
our $q;
$q = CGI->new;

our $path_session_directory = "/tmp/pruebas_session2";
our $name_cookie_that_stores_session_id  = "logeado";

# Function: estaLogeado
# estaLogeado significa que el contenido del cookie es == al valor de la session
# looks for cookie 'logeado'
# compares value of cookie 'logeado' and compares it to session->id()
# returns{status} == OK if so
# returns{stauts} == ERROR if not,
sub estaLogeado() {
	my $existia_cookie = $q->cookie($name_cookie_that_stores_session_id);
	if (&cookieAndSessionEqual($existia_cookie)){

		return(status=>"OK");
	}
	return(status=>"ERROR");

	# Compares values
	# if they are not equal removes new created session
	sub cookieAndSessionEqual($){
		my $value_of_cookie = shift;
		return(0) unless $value_of_cookie;
		my $session = new CGI::Session("driver:File", $value_of_cookie, {Directory=>$path_session_directory}) or die CGI::Session->errstr;
		return 1 if $value_of_cookie eq $session->id();  # everything looks ok
		$session->delete();
    	$session->flush(); # Recommended practice says use flush() after delete().
    	return(0);
	}
}
1;
