#!/usr/bin/perl
use warnings;
use strict;
use lib '.';
use miSession;
our $q;

my $debug;
my %params = $q->Vars;



if( &MOCKcredencialesOK()){
    # miSyslog((split('/',$0))[-1]." recibe r_estaLogeado [".$r_estaLogeado{status}."]");
    our $path_session_directory;
    our $name_cookie_that_stores_session_id;
    my $session = new CGI::Session("driver:File", undef, {Directory=>$path_session_directory}) or die CGI::Session->errstr;
    &miSyslog("name cookie [".$name_cookie_that_stores_session_id."] session [".$session->id()."]");
    my $cookie = $q->cookie(-name=>$name_cookie_that_stores_session_id,-value=>$session->id());
        print $q->redirect(-uri=>"index.pl",-cookie=>$cookie);
}
# BAD credenciales
print $q->header();  # cualquier print (aunque sea header imposibilita el redirect)
print $q->h1("LOGIN");

my %r_estaLogeado = estaLogeado();
if ( $r_estaLogeado{status} eq "OK" ) {
    print $q->div("hola XX TODO boton desconectar");
}


(  $params{usuario} or $params{passw} ) ? &loginForm(1):&loginForm();

sub loginForm() {
    my $error = shift;
    print "PROBLEMAS USUARIO/CONTRASENYA" if $error;
    # Forms
    print $q->start_form(
        -name     => 'main_form',
        -method   => 'GET',
        -enctype  => &CGI::URL_ENCODED,
        -onsubmit => 'return javascript:validation_function()',
        -action   => $q->self_url,                               # Defaults to
             # the current program
    );
    print "usuario";
    print $q->textfield(
        -name      => 'usuario',
        -value     => '1',
        -size      => 20,
        -maxlength => 30,
    );
    print $q->br . "contraseña";
    print $q->password_field(
        -name      => 'passw',
        -value     => '1',
        -size      => 20,
        -maxlength => 30,
    );
    print $q->br . $q->submit( -value => 'BOTON enviar form' );
    print $q->end_form;

    # miSyslog( Dumper(%params) );
}
sub MOCKcredencialesOK(){

    # cant have credentials without $params
    return 0 unless $params{usuario};
    return 0 unless $params{passw};
    # miSyslog("dentro de MOCKcredencialesOK usuario [".$usuario."] password [".$password."]");

    # logic for credentials
    return 1 if ($params{usuario} eq "1" and $params{passw} eq "1" );
    return 0;
}
# print "PARAMS<br>".Dumper( \%params );