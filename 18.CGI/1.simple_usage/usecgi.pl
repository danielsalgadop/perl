#!/usr/bin/perl
use warnings;
use strict;
use CGI;

my $q = CGI->new;
print $q->header( -charset => 'utf-8' );
print $q->start_html(
    -title   => "titulo",
    -charset => 'utf-8',
    -bgcolor => 'white',
    -style   => [ { -src => 'css1.css' }, { -src => 'css2.css' } ],
    -script  => [
        {   -type => 'text/javascript',
            -src =>
                'http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js',
        },
        {   -type => 'text/javascript',
            -src  => 'js.js',
        }
    ]
);
#### HEADING
print $q->h1("h1");
print $q->h2("h2");
print $q->h3("h3");

#### LINKS
print $q->a( { href => "http://google.com" }, "Un link a google" );
print $q->a(
    {   href    => "http://google.com",
        class   => "mi_clase",
        onclick => "miAlert('valor')"
    },
    "Link with a css class and a function for ONCLICK"
);
print $q->div(
    { id => "resultado_calcular_isid", class => "comandos_en_nodo" }, "" );

print "<br>";

# PARAGRAPHS
print $q->p("un PARAGRAPH");
print $q->p({style=>"color:pink"},"un PARAGRAPH con INLINE css");
print $q->p({class=>"mi_clase"},"un PARAGRAPH con CLASE css");
# crear DIVS
print $q->div("esto es un div");
# crear Tablas
# con array para li

# Forms
# TODO mirar CGI::QuickForm http://search.cpan.org/~summer/CGI-QuickForm-1.93/QuickForm.pm
print $q->start_form(
    -name     => 'main_form',
    -method   => 'GET',
    -enctype  => &CGI::URL_ENCODED,
    -onsubmit => 'return javascript:validation_function()',
    -action   => '/where/your/form/gets/sent',                # Defaults to
         # the current program
);
print $q->textfield(
    -name      => 'text1',
    -value     => 'default value',
    -size      => 20,
    -maxlength => 30,
);
print $q->textarea(
    -name  => 'textarea1',
    -value => 'default value',
    -cols  => 60,
    -rows  => 3,
);
print $q->password_field(
    -name      => 'passw',
    -value     => 'passw',
    -size      => 20,
    -maxlength => 30,
);
print $q->hidden(
    -name    => 'hidden1',
    -default => 'some value',
);

### POPUP MENU
my @values = ( 'value1', 'value2', 'value3' );
print $q->popup_menu(
    -name    => 'popup1',
    -values  => \@values,
    -default => 'value2'
);
### MORE COMPLEX POPUP MENU
#    my @values = ('value1', 'value2', 'value3');
my %labels = (
    'value1' => 'Choice 1',
    'value2' => 'Choice 2',
    'value3' => 'Choice 3',
);

print $q->popup_menu(
    -name       => 'popup1',
    -values     => \@values,
    -default    => 'value2',
    -labels     => \%labels,
    -attributes => { 'value1' => { 'style' => 'color: red' } },
);
print $q->submit(-name=>'button_name',-value=>'BOTON enviar form');
print $q->end_form;

# TODO checkboxes
# he aprendido a hacer group de checkboxes que comparatan name. El "problema" es que al compartir name, al ser codificado en peticion post (o get) los valores se "colapsan". Para separarlos hay que usar  "\0" (null) character.
# @separados = split ("\0",$params{el_name_del_grupo_checkboses}); 





## IMAGES
print $q->img(
    {   src =>
            'http://icons.iconarchive.com/icons/yellowicon/game-stars/256/Mario-icon.png'
    }
);
print &returnedHtml();
print $q->end_html;

# This subroutine DOES NOT WRITE ANYTHING
sub returnedHtml(){
    return($q->p("returnde PARAGRAPH"));
}