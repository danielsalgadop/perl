#!/usr/bin/perl
# http://perldoc.perl.org/functions/getpeername.html
# Returns the packed sockaddr address of the other end of the SOCKET connection.
use warnings;
use strict;
use Socket;

# NO FUNCIONA: Name "main::SOCK" used only once: possible typo at use_Socket.pl line 9.getpeername() on unopened socket SOCK at use_Socket.pl line 9.
my $hersockaddr = getpeername(SOCK);
print $hersockaddr;
my ($port, $iaddr) = sockaddr_in($hersockaddr);
my $herhostname = gethostbyaddr($iaddr, AF_INET);
my $herstraddr = inet_ntoa($iaddr);

print "port [".$port."] iaddr [".$iaddr."] herhostname [".$herhostname."] hersockaddr\n";