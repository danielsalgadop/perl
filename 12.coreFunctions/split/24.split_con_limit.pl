#!/usr/bin/perl

use warnings;
use strict;
use Data::Dumper; 

my @datos = qw(notegonada tengo_uno ahora_tengo_dos el_tres_es_elsiguiente 1_2_3_4_sasdfas);
print "DATOS ORIGEN\n-------------\n";
print Dumper(@datos);
print " SIM LIMIT\n-----------\n";
foreach my $undato(@datos){
	my @spliteados = split /_/,$undato;
	print Dumper(@spliteados);
}
# el limit en positivo marca el numero de campos, es por eso que poner limit 1 es absurdo, por que simplemente no corta nada
print " LIMIT 2\n-----------\n";
foreach my $undato(@datos){
	my @spliteados = split /_/,$undato,2;
	print Dumper(@spliteados);
}



# simular una url y splitear por un string
# problema encontrado al hacer enrutador de peticiones. Ya que los links agregan la nueva url

my $url="/nombreapp/index.pl/ruta1/valor1/index.pl/ruta2/valor2";
my @url_spliteada = split ("index.pl",$url);
print Dumper(@url_spliteada);