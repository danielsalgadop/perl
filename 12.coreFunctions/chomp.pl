#! /usr/bin/perl
use strict;
use warnings;
use Data::Dumper;
# in auto-auto-config.pl we had a problem with chomps ended in '\r\n' (edited in windows)

#### can you chomp all elements in an array?
my @array1 = (
	"uno_array1\n",
	"dos_array1\n",
	"tres_array1\n"
);
my @array2 = (
	"uno_array2\n",
	"dos_array2\n",
	"tres_array2\n"
);

print "array1 and array2 PRE chomp\n";
print Dumper(@array1,@array2);

chomp(@array1,@array2); # <<<<<<<<<<<<<<<<<<<<<<<<<<<

print "array1 and array2 POST chomp\n";
print Dumper(@array1,@array2);

# todo: chomp that works equal in windows or unix
my universalChomp(){
	
}