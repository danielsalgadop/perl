#!/usr/bin/perl -w
use strict;
use Data::Dumper;
# --------  Comprobacion de que el cliente existe --------------------------------------
my @Clientes = qw(CHI SDF GHR AAA IEI LFE LWC); 
print Dumper($ARGV[0]);
my @buscar = grep(/$ARGV[0]/, @Clientes);
unless (@buscar)
{
	print "no lo he encontrado\n";		
}
print Dumper(@buscar);
# lo malo es que grep encuentra una subcadena como encontrada, es decir ABC es positivo en XXXABCXXXX.  

# -------------------------    2a cosa, probar un grep en un if(grep)

if (grep(/$ARGV[0]/, @Clientes))
{

	print "\nEsto ha funcionado\n";
}
foreach my $uncliente(@Clientes) {
	if ($uncliente=~ /$ARGV[0]/) {
		print "encontrado mediante =~ \n";
	}
}


#### detectar con grep cuando hay arrays solo con numeros o con letras
print "\n--detectar con grep cuando hay arrays solo con numeros o con letras---\n";

my @solo_numeros = (1,2,3,4,5,6);
my @solo_numeros_con_comillas = (1,2,3,"4");
my @alfanumericos = ("alfa1","alfa2");

print "solo_numeros=";
&detectarNumerosEnArray(\@solo_numeros);

print "solo_numeros_con_comillas=";
&detectarNumerosEnArray(\@solo_numeros_con_comillas);
print "alfanumericos=";
&detectarNumerosEnArray(\@alfanumericos);



sub detectarNumerosEnArray($){
	my $ref_array = shift;
	if(grep(/\D/,@$ref_array)){ # matcheo Non Digit
		print "encontrado NO numericos";
	}
	else{
		print "Solo hay numeros";
	}
	print "\n";
}