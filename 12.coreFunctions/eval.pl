#!/usr/bin/perl -w
# eval EXPR
# In the EXPR form, often referred to as a "string eval", the return value of EXPR is parsed and executed as if it were a little Perl program. The value of the expression (which is itself determined within scalar context) is first parsed, and if there were no errors, executed as a block within the lexical context of the current Perl program. This means, that in particular, any outer lexical variables are visible to it, and any package variable settings or subroutine and format definitions remain afterwards.

# eval BLOCK
# In the BLOCK form, the code within the BLOCK is parsed only once--at the same time the code surrounding the eval itself was parsed--and executed within the context of the current Perl program. This form is typically used to trap exceptions more efficiently than the first (see below), while also providing the benefit of checking the code within BLOCK at compile time.


# In both forms, the value returned is the value of the last expression evaluated inside the mini-program; a return statement may be also used, just as with subroutines.

use strict;
my $a = 0;
my $b = 0;
my $answer="initial_value";
# make divide-by-zero nonfatal
eval { $answer = $a / $b; }; warn $@ if $@;
# same thing, but less efficient
eval '$answer = $a / $b'; warn $@ if $@;

# a compile-time error
# eval { my $answer = }; # WRONG

# a run-time error
eval '$answer ='; warn "run-time error ".$@ if $@; # sets $@

# changing value inside eval
eval {$answer = "value changed in BLOCK";}; 
print "answer [".$answer."]\n";
eval '$answer="value changed in EXPR"';
print "answer [".$answer."]\n";

# Returning
my $return = eval { return "value returned";};
print "return [".$return."]\n";
$return = eval{my $explicitly = "value explicitly returned"};
print "return [".$return."]\n";