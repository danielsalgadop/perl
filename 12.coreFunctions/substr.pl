#!/usr/bin/perl
use strict;
use warnings;
# pruebas para substr



# voy a recortar un string para que tenga 80 caracteres
my $string_cortable_pequenio = "123";
my $string_cortable_justo_80 = "12345678901234567890123456789012345678901234567890123456789012345678901234567890";
my $string_cortable_grande = "12345678901234567890123456789012345678901234567890123456789012345678901234567890estoSobra";


my @strings = ($string_cortable_pequenio,$string_cortable_justo_80,$string_cortable_grande);
foreach (@strings){
	print "este string ".$_."\t";
	print "numero de caracteres [".length($_)."]\n";
	my $string_con_substr = substr($_,0,80);
	print "string_con_substr[".$string_con_substr."]\n";
	print "-------\n";
}